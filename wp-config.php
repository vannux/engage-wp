<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_t1');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '3ceK+^;>ID8%CW!Oz-Ea1q=-cy%/1bVnb7vp(O8-x#H(m7[vEBZ?5Y}+>r.;89jh');
define('SECURE_AUTH_KEY',  'sil;uiX*;dNZ{=kf3Ppp k9hz2p)~?NKvK#D|-Q`l?oznc+vd2T-nu56=gYXKiTk');
define('LOGGED_IN_KEY',    '/!^)I4haX;Cw2PtK82LlR35Bk;8OP_IN E*3|c1qD+AXldpsDbn7E>LZl~_0d3y+');
define('NONCE_KEY',        '5Mm>mNt%bH8t$Po+V-u~>HtYyQ(+cvWQf-UXIf~z+z@?$EDqq_ot%vsR:8*N7U|H');
define('AUTH_SALT',        '#)??j74dOV0apJ8cf!_a2@X.:zoh04.ZAaaptV- |rzZ2L<Oua$uxAo!A]R_CO@j');
define('SECURE_AUTH_SALT', ' Uo9+D5=S}mBN1^#?)#Gem-.=d;3_s/a)[bWocj|EOY5pUcB/ng{esc&9&)PNCyg');
define('LOGGED_IN_SALT',   ',c$5VFts!_pSz+TLl amn:~ToC+.M1;}w=>X9P_.Td*7B--`EKZBx._+2jiD)X e');
define('NONCE_SALT',       '3-;xUH++o?@@XB+Tg*F|*ISc}|*+d3M[6n{+4zS:<>S7]ald2(M~iYi*:E)Y|+x$');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'esci_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);

/* Multisite */
define( 'WP_ALLOW_MULTISITE', true );
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', 'localhost');
define('PATH_CURRENT_SITE', '/wp_t1/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
