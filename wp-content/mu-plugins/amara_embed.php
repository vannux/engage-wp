<?php
/*
Plugin Name: Amara
Description: Use [amara width='620px' height='370px' url='http://www.youtube.com/watch?v=An22NL4Gj00'] to insert a trip advisor badge into your post or page.
Author: Mauro Vannucci
Version: 0.1
*/

add_shortcode( 'amara', 'amara_add_div' );
add_shortcode( 'amarascript', 'amara_add_script' );
function amara_add_div( $atts ) {
	$default = array(
		'height' => '370px',
		'width' => '620px',
		'url' => ''
	);
	$args = wp_parse_args($atts, $defaults);
	if ($args['url'] == '') {
		return  "Amara video url not specified.";
	}
	return  "<div class='amara-embed' data-height='{$args['height']}' data-width='{$args['width']}' data-url='{$args['url']}'></div>";
}
function amara_add_script( $atts ) {
	return  "<script type='text/javascript' src='http://amara.org/embedder-iframe'></script>";
}
