<!-- Engage Link Template: Default Template -->
<div class="download">
<div class="download-group">
    <div class="download-icon">
    [icon]
    </div> 
    <div class="download-content">
    <h4><a href="[download_url]">[title]</a></h4>
    <div class="download-details">
    [file_size] | [download_count] downloads
    </div>
    </div>
</div>
</div>