<?php
function render_table() {
		global $wpdb, $current_user;

	wp_enqueue_style( 'els-dm-mod.css', plugins_url( '/mod.css', __FILE__ ) );

	wp_enqueue_script('jquery-1.11.3', plugins_url( '/jq/jquery-1.11.3.min.js', __FILE__ ), null);

	wp_enqueue_script('jquery-ui', plugins_url( '/jq/jqui/jquery-ui.min.js', __FILE__ ), array('jquery-1.11.3'));

	wp_enqueue_style( 'jquery-ui.min.css', plugins_url( '/jq/jqui/jquery-ui.min.css', __FILE__ ) );
	wp_enqueue_style( 'jquery-ui.structure.min.css', plugins_url( '/jq/jqui/jquery-ui.structure.min.css', __FILE__ ) );
	wp_enqueue_style( 'jquery-ui.theme.min.css', plugins_url( '/jq/jqui/jquery-ui.theme.min.css', __FILE__ ) );

	wp_enqueue_script('els-dm-mod', plugins_url( '/mod.js', __FILE__ ), array('jquery-ui'));

	$qfilter = '';
	$filter = isset($_GET['role']) ? $_GET['role'] : 0;
	if (isset($_POST['role'])) $filter = $_POST['role'];
	if ($filter > 0) {
		if ($filter == '1') $qfilter = ' and pm1.meta_value like "%guest%" ';
		else if ($filter == '2') $qfilter = ' and not(pm1.meta_value like "%adapter%") ';
		else if ($filter == '3') $qfilter = ' and pm1.meta_value like "%adapter%" ';
	}

	if (isset($_POST['confirm_roles'])) {
		//var_dump($_POST['add_role']);
		//var_dump($_POST['id']);
		$new_roles = serialize($_POST['add_role']);
		foreach($_POST['id'] as $pid) {
			$wpdb->update( $wpdb->prefix . 'postmeta', array('meta_value' => $new_roles), array('post_id'=>$pid, 'meta_key'=>'__wpdm_access') );
		}
		unset($_POST['id']);
	}
	if (isset($_POST['cancel_roles'])) {
		unset($_POST['id']);
	}
	
	$itemsPerPage = 10;
	$currentPage = isset($_GET['paged']) ? $_GET['paged'] : 1;
	$start = ($currentPage - 1) * $itemsPerPage;
	echo '<h2>' . __('ELS Download Manager', 'els_download_manager') . '</h2>';
	//echo '<pre>' ; var_dump($currentPage); echo '</pre>' ;
	
	$count = $wpdb->get_var("SELECT count(p.id) from {$wpdb->prefix}posts p
		left join {$wpdb->prefix}postmeta pm1 on p.id = pm1.post_id and pm1.meta_key='__wpdm_access'
		where p.post_type = 'wpdmpro' $qfilter");
		
	$lastPage = floor ($count / $itemsPerPage) + 1;
	if ($currentPage > $lastPage) {
		$currentPage = $lastPage;
		$start = ($currentPage - 1) * $itemsPerPage;
	}
		
	$res = $wpdb->get_results("
		SELECT p.id, p.post_title, pm1.meta_value access FROM {$wpdb->prefix}posts p 
		left join {$wpdb->prefix}postmeta pm1 on p.id = pm1.post_id and pm1.meta_key='__wpdm_access'
		where p.post_type = 'wpdmpro' $qfilter limit $start, $itemsPerPage");		
	?>
	
	
<div id="dialog-confirm" title="Add/Remove Roles" style="display: none; max-width:800px">
  <?php 
  global $wp_roles;
  foreach ($wp_roles->roles as $role_name => $role_info) {
	if(isset($role_info['capabilities']['read'])) echo "<button class='button role-btn' id='$role_name' style='margin: 1px 2px 3px 2px'>$role_name</button>";
  }
  ?>
  </div>

	<form method="post" action="" enctype="multipart/form-data">
		<input type="hidden" name="post_type" value="wpdmpro">
		<input type="hidden" name="page" value="els-download-manager">
		<input type="hidden" name="role" value="<?php echo $filter;?>">
		
		

	<?php 
	if (isset($_POST['id'])) {
		foreach($_POST['id'] as $id) {
			echo "<input type='hidden' name='id[]' value='$id'>";
		}
	
		if (isset($_POST['edit_selected']) and count($_POST['id'] > 0)) { 
			$ids = $_POST['id'];
			$roles = array();
			$posts = array();
			foreach($res as $r) {
				if (in_array ($r->id, $ids)) {
					array_push($posts, $r);
					$arr = unserialize($r->access);
					$roles = array_unique (array_merge ($roles, $arr));
				}
			}
			?>
			<table cellspacing="0" class="widefat fixed">
			<thead>
				<tr>
					<th><b>Packages</b></th>
					<th><b>Access</b> &nbsp;
						<input type="button" id="add_role" name="add_role" class="button action" value="Edit roles">
						<input type="submit" id="confirm_roles" name="confirm_roles" class="button action" value="Confirm Roles">
						<input type="submit" id="cancel_roles" name="cancel_roles" class="button action" value="Cancel">
					</th>
				</tr>
			</thead>
			<tbody>
			<tr>
				<td>
				<?php foreach($posts as $r) {
					echo $r->post_title . '<br/>';
				}?>
				</td>
				<td id="access-material">
				<?php foreach($roles as $r) {
					echo "<input class='col-xs-6 col-sm-3 mat-access' name='add_role[]' value='$r' />" ;
				}?>
				</td>
			</tr>
			</tbody>
			</table>
			<?php
		} 
	} ?>
	
	
	<div class="tablenav top">
		<div class="alignleft actions bulkactions">
			<input type="submit" name="edit_selected" class="button action" value="Edit Selected">
		</div>
		
		<div class="alignleft actions">
			<select name="role" id="filter-by-date">
				<option <?php if (!isset($filter) || $filter=='0') echo 'selected="selected"';?> value="0">All Packages</option>
				<option <?php if ($filter=='1') echo 'selected="selected"';?> value="1">Guest</option>
				<option <?php if ($filter=='2') echo 'selected="selected"';?> value="2">Not downloadable for Adapter</option>
				<option <?php if ($filter=='3') echo 'selected="selected"';?> value="3">Downloadable for Adapter</option>
			</select>
			<input type="submit" name="filter_action" id="post-query-submit" class="button" value="Filter">
		</div>
		
		<div class="tablenav-pages">
			<span class="displaying-num"><?php echo $count ?> items</span>
			<span class="pagination-links">
			<?php $link = "post_type=wpdmpro&page=els-download-manager&role=$filter"; ?>
				<a class="first-page <?php if ($currentPage == 1) echo 'disabled" style="pointer-events:none;"'; ?>" title="Go to the first page" value = "<<" href="<?php echo admin_url("edit.php?$link&paged=1");?>">«</a>
				<a class="prev-page  <?php if ($currentPage == 1) echo 'disabled" style="pointer-events:none;"'; ?>" title="Go to the previous page" href="<?php echo admin_url("edit.php?$link&paged=". ($currentPage - 1)); ?>">‹</a>
				<span class="paging-input">
					<label for="current-page-selector" class="screen-reader-text">Select Page</label>
					<input class="current-page" id="current-page-selector" title="Current page" type="text" name="paged" value="<?php echo $currentPage; ?>" size="1"> of <span class="total-pages"><?php echo $lastPage; ?></span>
				</span>
				<a class="next-page <?php if ($currentPage == $lastPage) echo 'disabled" style="pointer-events:none;"'; ?>" title="Go to the next page" href="<?php echo admin_url("edit.php?$link&paged=".($currentPage + 1)); ?>">›</a>
				<a class="last-page <?php if ($currentPage == $lastPage) echo 'disabled" style="pointer-events:none;"'; ?>" title="Go to the last page" href="<?php echo admin_url("edit.php?$link&paged=$lastPage"); ?>">»</a>
			</span>
		</div>
		<input type="hidden" name="mode" value="list">

		<br class="clear">
	</div>

	<table cellspacing="0" class="widefat fixed">
		<thead>
		<tr>
		<th class="manage-column column-cb check-column" id="cb" scope="col"><input type="checkbox"></th>
		<th style="width:50px" style="" class="manage-column column-id sortable <?php echo $_POST['sorder']=='asc'?'asc':'desc'; ?>"  scope="col"><a href='admin.php?page=file-manager<?php echo $_POST['cat']!=''?"&cat={$_POST[cat]}":""; ?>&sfield=id&sorder=<?php echo $_POST['sorder']=='asc'?'desc':'asc'; ?><?php echo $qr; ?>&paged=<?php echo $_POST['paged']?$_POST['paged']:1;?>'><span><?php echo __('ID','wpdmpro'); ?></span><span class="sorting-indicator"></span></a></th>
		<th style="width:35%" class="manage-column column-media sortable <?php echo $_POST['sorder']=='asc'?'asc':'desc'; ?>" id="media" scope="col"><a href='admin.php?page=file-manager<?php echo $_POST['cat']!=''?"&cat={$_POST[cat]}":""; ?>&sfield=title&sorder=<?php echo $_POST['sorder']=='asc'?'desc':'asc'; ?><?php echo $qr; ?>&paged=<?php echo $_POST['paged']?$_POST['paged']:1;?>'><span><?php echo __('Package Title','wpdmpro'); ?></span><span class="sorting-indicator"></span></a></th>
		<th style="" class="manage-column column-access" id="parent" scope="col"><?php echo __('Access','wpdmpro'); ?></th>    
		</tr>
		</thead>
		<tbody class="list:post" id="the-list">
		
		<?php foreach($res as $post) { ?>
		
		<tr valign="top" class="alternate author-self status-inherit" id="post-<?php echo $post->id; ?>">
			<th class="check-column" scope="row"><input type="checkbox" value="<?php echo $post->id ?>" name="id[]"></th>
			<th class="check-column" scope="row"><?php echo $post->id; ?></th>
			<td class="media column-media">
				<a title="Edit" href="<?php echo admin_url("post.php?action=edit&post={$post->id}"); ?>"><?php echo $post->post_title;?></a>
			</td>
			<td class="media column-media">
			<?php 
			$arr = unserialize($post->access);
			foreach($arr as $a) { 
				echo "<span class='col-xs-6 col-sm-3 mat-access'>$a</span>";
			 } ?>
			</td>
		</tr>
		<?php } ?>
		</tbody>
	</table>
	</form>
<?php }