<?php
/*
Plugin Name: ELS download manager
Description: Add functionalities to the "Download Manager" plugin.
Version: 1.0.0
Author: Mauro Vannucci
Author URI: 
Copyright 2014  Mauro Vannucci
Permission to add/modify/copy are subject to the author.
*/

class ELS_Download_Manager {

	public function __construct() {
		add_action('admin_menu', array($this, 'register_submenu_pages'));
		add_action('wp_enqueue_scripts', array($this, 'enqueue_style'));
		add_shortcode('wpdm_els_adapt', array($this, 'wpdm_els_adapt'));
	}
	
	function wpdm_els_adapt($params) {
        $id = isset($params['id'])?$params['id']:false;
        if(!$id) return "";
		
        $style = isset($params['style'])?$params['style']:'';
        $align = "left";
        if(strpos($style,"entered"))  $align = 'center';
        if(strpos($style,"ight"))  $align = 'right';
		
		
		if (isset($params['page_title'])) {
			$form_page = get_page_by_title($params['page_title']);
			if ($form_page)
				$form_link = get_permalink($form_page->ID);
		}
		
		$arr = get_post_meta($id, '__wpdm_access', true);
		$fileIsSubscriber = in_array('subscriber', $arr);
		$fileIsAdapter = in_array('adapter', $arr);
		$user_roles = wp_get_current_user()->roles;
		$userIsSubscriber = in_array('subscriber', $user_roles);
		$canUserDownload = count(array_intersect($user_roles, $arr)) > 0;
		
		if (!$canUserDownload && !is_user_logged_in()) {
			$footer = '<div class="download-footer"><img class="wpdm_icon" src="'.get_template_directory_uri().'/images/alert.png">';
			$footer .= sprintf (__('Please login to access downloadables.','els_download_manager'), $form_link);
			$footer .= '</div>';
		}
		else if (!$canUserDownload && $userIsSubscriber && $fileIsAdapter) {
			$footer = '<div class="download-footer"><img class="wpdm_icon" src="'.get_template_directory_uri().'/images/alert.png">';
			if ($form_link)
				$footer .= sprintf (__('This material is for Advanced Users only. You can promote yourself to Avdanced User via <a href="%s">this form.</a>','els_download_manager'), $form_link);
			else	
				$footer .= __('This material is for Advanced Users only.','els_download_manager');
			$footer .= '</div>';
		}
		
        $package = get_post($id, ARRAY_A);
        $package = wpdm_setup_package_data($package);
        //$link_label = get_post_meta($id, '__wpdm_link_label', true);
		
		if ($canUserDownload) {
			$title = '<h4><a href="[download_url]">[title]</a></h4>';
		}
		else {
			$title = '<h4>[title]</h4>';
		}
		
        $html = '
<div class="w3eden">
	<div class="download-adapt">
		<div class="download">
			<div class="download-group">
				<div class="download-icon">[icon]</div> 
				<div class="download-content">
					'.$title.'
					<div class="download-details">
						Size: [file_size]
					</div>
				</div>
			</div>
		</div>
		'.$footer.'
	</div>
</div>';
		/*
		<script>
		jQuery(function(){
		jQuery('.link-btn a.wpdm-download-link img').after('{$link_label}');
		jQuery('.link-btn a.wpdm-download-link img').remove();
		});
		</script>
		*/
        $html = str_replace(array("\r","\n"), "", $html);
        return FetchTemplate($html, $package, 'link');

    }

    function enqueue_style(){
        wp_enqueue_style("wpdm-button-templates",plugins_url("wpdm-button-templates/buttons.css"));
    }
	
	public function register_submenu_pages() {
		$access_level = 'manage_options';
		add_submenu_page( 'edit.php?post_type=wpdmpro', __('ELS Download Manager &lsaquo; Download Manager','els_download_manager'), __('ELS Download Manager','els_download_manager'), $access_level, 'els-download-manager', array(__CLASS__, 'download_manager'));
	}
	
	public function download_manager() {
		//require('download-table.php');
		include('download-table.php');
		render_table();
	}
}
new ELS_Download_Manager();
