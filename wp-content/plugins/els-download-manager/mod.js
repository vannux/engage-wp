(function () {
	var $ = jQuery;
	
	$('#add_role').click(function() {
		$( "#dialog-confirm" ).dialog({
		  resizable: true,
		  height: "auto",
		  modal: true,
		  buttons: {
			"Close": function() {
			  $( this ).dialog( "close" );
			}
		  }
		});
		$(".role-btn").click(function(ev) {
			console.log("ev.target.id = " + ev.target.id);
			var b = false;
			$("#access-material").children().each(function() {
				if (!b && ev.target.id == this.value) {
					b = true;
					this.remove();
				}
			});
			
			if (!b) $("#access-material").append("<input class='col-xs-6 col-sm-3 mat-access' name='add_role[]' value='"+ev.target.id+"'/>");
		});
		
	});
	
}());