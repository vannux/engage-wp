<?php
/*
Plugin Name: ELS Ninja Form Add-On
Description: Add functionalities to the "Ninja Form" plugin.
Version: 1.0.0
Author: Mauro Vannucci
Author URI: 
Copyright 2014  Mauro Vannucci
Permission to add/modify/copy are subject to the author.
*/

class ELS_Ninja_Form {

	public function __construct() {
		add_action( 'ninja_forms_post_process', array($this, 'ninja_forms_submit') );
	}
		
	public function ninja_forms_submit() {
		global $ninja_forms_processing;
		$all_fields = $ninja_forms_processing->get_all_fields();
		if( is_array( $all_fields ) && $ninja_forms_processing->get_form_setting('form_title') == 'Form_adapter') {
			$user = wp_get_current_user();
			if (!in_array('adapter', $user->roles)) {
				$user->add_role( 'adapter' );
				$_SESSION["is_now_adapter"] = true;
			}
		}
	}

}
new ELS_Ninja_Form();

if (isset($_SESSION["is_now_adapter"])) {
	add_action("wp_footer", els_show_dialog_advanced);
}
function els_show_dialog_advanced() {
	unset($_SESSION['is_now_adapter']);
	//$page = get_page_by_title( 'Welcome Adapter Popup' );
	$pages = get_posts( array( 'name' => 'welcome-adapter-popup', 'post_type' => 'page' ) );
	if (count($pages) == 0) return;
	$page = $pages[0];
	
	wp_enqueue_script( 'bpopup', plugins_url( 'js/bpopup.js', __FILE__ ), ['jquery'], null, true );
	wp_enqueue_script( 'popup_login', plugins_url( 'js/login_popup.js', __FILE__ ), ['bpopup'], null, true );
	?>
	<style>
	#login_popup {
		display:none;
		width: 661px;
		height: 941px;
		background-image: url("<?php echo plugins_url( 'dialogue_box_b.png', __FILE__ ); ?>");
		background-repeat: no-repeat;
		}
		#login_popup_content {
			position:relative;

			top: 173px;
			width: 453px;
			height: 380px;
			margin: 0 86px 0 106px;
			overflow: auto;
		}
		#login_popup_links {
				position: absolute;
				left: 117px;
				top: 564px;
				width: 453px;
				height: 48px;
				text-align: right;
				overflow: hidden;
				font-weight: bold;
		}
				@media only screen and (max-width: 768px){
			#login_popup {
				padding:30px;
				display:none;
				max-width: 300px;
				height: auto;
				background: #fff;
				
				-webkit-border-radius: 30px;
				-moz-border-radius: 30px;
				border-radius: 30px;
				
				box-shadow: 1px 1px 1px #ccc;
			}
			#login_popup_content {
				position:relative;
				left: 0px;
				top: 0px;
				max-width: 280px;
				height: auto;
				overflow: auto;
			}
			#login_popup_links {
				position:relative;
				left: 0px;
				top: 0px;
				width: 250px;
				height: auto;
				text-align: right;
				overflow: hidden;
				font-weight: bold;
			}		
		}
			</style>
	<div id="login_popup" hidden="hidden">
		<div id="login_popup_content">
			<?php
			 $content = get_post_field('post_content', $page->ID);
			 $content = apply_filters('the_content', $content);
			 echo $content;
			?>
		</div>
		<!-- div id="login_popup_links">
			< ? php echo '<a href="'.admin_url( 'profile.php' ).'">'. __('Admin area', '') . '</a>'; ? >
		</div-->
	</div>
<?php
}

