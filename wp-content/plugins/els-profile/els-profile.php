<?php
/*
Plugin Name: ELS Profile
Plugin URI:
Description: ELS Profile.
	[els-profile]
Version: 0.01.0
Author: Mauro Vannucci
Author URI:
Copyright 2014  Mauro Vannucci (email : vannux@gmail.com)
Permission to add/modify/copy are subject to the author.
*/

add_shortcode('els-profile',  'els_profile');
function els_profile($atts, $content = null) {
	$user = wp_get_current_user();
	$user_id = $user->ID;
	if(!$user_id) {
		echo '<h2>'.__('You are not logged.','engage').'</h2>';
		return;
	}
	$user_roles = $user->roles;
	?>
	<style>
	.els-col-6 {
		width:49%;
		float:left;
	}
	.els-img-profile {
		width:15em;
		margin-right:1em;
		float:left;
	}
	.els-about-myself {
		border: 1px solid black;
		background: #ddd;
		height:6em;
		overflow: auto;
		padding: 8px;
	}
	</style>
	<img class='els-img-profile' src="<?php echo get_cimyFieldValue($user->ID, 'PHOTO'); ?>"/>
	<h2><?php echo $user->get("display_name"); ?></h2>
	Role:
	<?php
	foreach($user_roles as $r) {
		echo $r;
	}
	echo '<h3>'.__('Personal information', 'engage').'</h3>';
	//
	?>
	<div class="clear"></div>
	<div class="row">
		<div class="els-col-6">
			<?php _e('Job:', 'engage'); ?> 		<b><?php echo get_cimyFieldValue($user_id, 'JOB'); ?></b><br/>
			<?php _e('City:', 'engage'); ?> 	<b><?php echo get_cimyFieldValue($user_id, 'CITY'); ?></b><br/>
			<?php _e('Country:', 'engage'); ?>	<b><?php echo get_cimyFieldValue($user_id, 'COUNTRY'); ?></b><br/>
			<?php _e('Email:', 'engage'); ?> 	<b><?php echo $user->user_email; ?></b><br/>
			<?php _e('Website:', 'engage'); ?>	<b><?php echo get_cimyFieldValue($user_id, 'CITY'); ?></b><br/>
			<?php _e('School:', 'engage'); ?>	<b><?php echo get_cimyFieldValue($user_id, 'SCHOOL/INSTITUTION'); ?></b><br/>
		</div>
		<div class="els-col-6 last">
			<?php _e('About myself:', 'engage'); ?><br/>
			<div class="els-about-myself">
			<?php echo sanitize_text_field(get_cimyFieldValue($user_id, 'ABOUT-MY-SELF')); ?>
			</div>
		</div>
	</div>
	<div class="clear"></div>
	<hr style="margin-top:1em"/>
	<div class="row">
		<div class="els-col-6">
			<h3><?php _e('My teaching', 'engage'); ?></h3>
			<?php _e('Downloads:', 'engage'); ?>       <br/>
			<?php _e('Materials liked:', 'engage'); ?> <br/>
			<?php _e('Comments:', 'engage'); ?>        <br/>
			<?php _e('Blog replies:', 'engage'); ?>    <br/>
			<?php _e('Material used:', 'engage'); ?>   <br/>
			<b><?php _e('I use ENGAGE for improving student\'s:', 'engage'); ?>   <br/>
			<b><?php _e('ENGAGE Courses Registered:', 'engage'); ?>   <br/>
			<b><?php _e('ENGAGE Courses Completef:', 'engage'); ?>   <br/>
			<b><?php _e('Tools mastered:', 'engage'); ?>   <br/>
			<h3><?php _e('My Reviews', 'engage'); ?></h3>
		</div>
		<div class="els-col-6">
			<h3><?php _e('My curriculum', 'engage'); ?></h3>
			<b><?php _e('Lesson used:', 'engage'); ?></b><br/>
			<b><?php _e('Topic addressed:', 'engage'); ?></b><br/>
			<b><?php _e('Inquiry Skills addressed:', 'engage'); ?></b><br/>
		</div>
	</div>
	<?php
}
