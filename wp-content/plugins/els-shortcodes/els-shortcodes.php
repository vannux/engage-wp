<?php
/*
Plugin Name: ELS Short codes
Plugin URI:
Description: Add Functions to the page.
	Usage: 1) [elsutils currentuser="username"]
	2) [els-restrict in_role="role1, role2" not_in_role="role3, role4"] ... [/els-restrict]
Version: 0.01.0
Author: Mauro Vannucci
Author URI:
Copyright 2014  Mauro Vannucci (email : vannux@gmail.com)
Permission to add/modify/copy are subject to the author.
*/

add_shortcode('elsutils',  'els_parse');
function els_parse($atts) {
	if (array_key_exists('currentuser', $atts)) {
		$user = wp_get_current_user();
		if ($atts['currentuser'] == 'username') {
			return $user->get("display_name");
		}
		else if ($atts['currentuser'] == 'display_name') {
			return $user->get("display_name");
		}
		return "elsutils currentuser value unrecognised";
	}
	return "elsutils tag unrecognised";
}

add_shortcode('els-user-nr-activities',  'els_user_nr_activities');
function els_user_nr_activities($atts, $content = null) {
	return get_nr_downloads_user();
}

add_shortcode('els-user-last-activity',  'els_user_last_activity');
function els_user_last_activity($atts, $content = null) {
	extract( shortcode_atts( array(
		'key' => 'none',
		), $atts ) 	);
	$post = get_last_activity();
	$post_link = get_permalink($post);
	if ($key=='href')
		return $post_link;
	else
		return "<a href='$post_link'>{$post->post_title}</a>";
}

add_shortcode('els-user-if-last-activity',  'els_user_if_last_activity');
function els_user_if_last_activity($atts, $content = null) {
	$post = get_last_activity();
	if (isset($post) && $post != null) return do_shortcode($content);
	return '';
}

add_shortcode('els-user-if-not-last-activity',  'els_user_if_not_last_activity');
function els_user_if_not_last_activity($atts, $content = null) {
	$post = get_last_activity();
	if ($post == null) return do_shortcode($content);
	return '';
}

add_shortcode('els-user-nr-reviews',  'els_user_nr_reviews');
function els_user_nr_reviews($atts, $content = null) {
	return get_nr_comments();
}

add_shortcode('els-restrict',  'els_restrict');
function els_restrict($atts, $content = null) {
	extract( shortcode_atts( array(
		'in_role' => 'none',
		'not_in_role' => null,
		), $atts ) 	);

	$arr_in_role = array_map('trim', explode(',', $in_role));
	$arr_not_in_role = array_map('trim', explode(',', $not_in_role));

	$content = trim ($content);
	$user_roles = wp_get_current_user()->roles;

	/*
	echo('<br/>atts in_role ');
	print_r($atts);
	echo('<br/>in_role ');
	print_r($arr_in_role);
	echo('<br/>not_in_role ');
	print_r($arr_not_in_role);

	echo('<br/>user roles ');
	print_r($user_roles);

	echo('<br/><br/>');
	//*/

	$res = true;
	if (array_key_exists('in_role', $atts)) {
		$res = false;
		foreach($arr_in_role as $r) {
			if (in_array($r, $user_roles)) {
				$res = true;
				break;
			}
		}
	}

	$notinrole = true;
	if (array_key_exists('not_in_role', $atts)) {
		foreach($arr_not_in_role as $r) {
			if (in_array($r, $user_roles)) {
				$notinrole = false;
				break;
			}
		}
	}

	if ($res && $notinrole)
		return do_shortcode($content);
}
