<?php 

add_action('wp_ajax_els_useful_for_rate', 'els_useful_for_rate_ajax');
add_action('wp_ajax_nopriv_els_useful_for_rate', 'els_useful_for_rate_ajax');

function els_useful_for_rate_ajax() {
	require_once dirname(__FILE__).'/lib.php';	
	check_ajax_referer( 'els_useful_for_nonce', 'nonce' );
	$user_id = get_current_user_id();
	$post_id = $_POST['post_id'];
	//$action = $_POST['action']; els_useful_for_rate
	$tag = $_POST['tag'];
	$tagval = $_POST['tagval'];
	$r = ELS_Useful_For_Lib::set_user_tag($post_id, $user_id, $tag, $tagval);
	if ($r) {
		$avg = ELS_Useful_For_Lib::get_avg_tags($post_id, $tag);
		if ($avg) {
			$resp = array('error'=>0, 
				'$tag' => $tag, 
				'$tagval' => $tagval, 
				'avg' => $avg[0]->average_rating,
				'nrVotes'=>	$avg[0]->tot,
				'strVotes' => $avg[0]->tot == 1 ? __('vote', 'engage') : __('votes', 'engage')
			);
		}
	}
	else {
		$resp = array('error'=>1, '$tag' => $tag, '$tagval' => $tagval);
	}
	
	wp_send_json( $resp );
	exit;
}
