<?php
/*
Plugin Name: ELS Useful For
Description: ELS Useful For.
Version: 1.0.0
Author: Mauro Vannucci
Author URI:
Copyright 2014  Mauro Vannucci
Permission to add/modify/copy are subject to the author.
*/

//Do not change this value
define( 'ELS_USEFUL_FOR_DB_VERSION', '1.2' );
load_plugin_textdomain('els-useful-for', false, basename( dirname( __FILE__ ) ) . '/languages' );

//include 'widgets/useful-for.php';
//include 'widgets/provide-feedback.php';
include 'useful_for_average.php';


class ELS_Useful_For {

	public function __construct() {
		add_action('admin_menu', array($this, 'register_submenu_pages'));
		//var_dump($this);
	}

	public function register_submenu_pages() {
		$access_level = 'manage_options';
		add_submenu_page( 'edit.php',
			__('ELS Useful for settings','els-useful-for'), __('ELS Useful for settings','els-useful-for'),
			$access_level, 'els-useful-settings',
			array(__CLASS__, 'menu_settings'));
	}

	public function menu_settings() {
		require_once('lib.php');
		wp_enqueue_script('els_useful_admin', plugins_url('js/els_useful_admin.js', __FILE__), ['jquery']);

		echo '<br/>';
		
		if ( isset($_POST['els-useful-del']) && wp_verify_nonce($_POST['els-useful-del'], 'wpnonce-els-useful') ) {
			$tagid = $_POST['tagname'];
			if ($_POST['action'] == 'del' && isset($_POST['tagname'])) {
				ELS_Useful_For_Lib::delete_all_tag_ref($tagid);
			}
			else if ($_POST['action'] == 'update' && isset($_POST['tagid'])) {
				$old_tag_name = $_POST['tagid'];
				$tagname = $_POST['tagname'];
				$tagdescr = $_POST['tagdescr'];
				$r = ELS_Useful_For_Lib::update_tag_name($old_tag_name, $tagname, $tagdescr);
				if (!$r) {
					$error_save = 'Error saving the tag.';
				}
			}
			else if ($_POST['action'] == 'save' && isset($_POST['tagid'])) {
				if (isset($_POST['tagvalue']) && trim($_POST['tagvalue']) != "") {
					$tagval = $_POST['tagvalue'];
					$r = ELS_Useful_For_Lib::update_tag_id($tagid, $tagval);
				}
				else {
					$error_save = 'Invalid tag value';
				}
			}
			
		}
		else if ( isset($_POST['wpnonce-els-useful']) && wp_verify_nonce($_POST['wpnonce-els-useful'], 'els-useful-add') ) {
			$new_tag = $_POST['new-tag'];
			if (trim($new_tag) == '') {
				$error_new = 'Tag empty.';
			}
			else if (ELS_Useful_For_Lib::is_default_tag_present($new_tag)) {
				$error_new = 'Tag already present.';
			}
			else {
				ELS_Useful_For_Lib::add_default_tag($new_tag);
			}
		}
		
		echo '<h2>' . __('ELS Useful for settings', 'els-useful-for') . '</h2>';

		echo '<h3>' . __('Common tags for all posts', 'els-useful-for') . '</h3>';
		
		$tags = ELS_Useful_For_Lib::get_default_tags();

		?>
		<form id="save-del-tags" method="post">
		<?php wp_nonce_field( 'wpnonce-els-useful', 'els-useful-del' ); ?>
		<input type="hidden" id="action" name="action" value=""/>
		<input type="hidden" id="tagid" name="tagid" value=""/>
		<input type="hidden" id="tagname" name="tagname" value=""/>
		<input type="hidden" id="tagvalue" name="tagvalue" value=""/>
		<input type="hidden" id="tagdescr" name="tagdescr" value=""/>
		</form>
		
			<table class="form-table">
				<tr class="form-field">
					<th scope="row">
						<label for"els_add">Default tags:</label>
					</th>
					<td>
					<?php 
					if (isset($error_save)) {
						echo '<div style="border: 1px solid #faa; border-radius: 4px; height: 2em; background-color: #FFEAEB; padding: 8px 0px 0 8px;">' . $error_save . '</div>';
					}
					foreach($tags as $f) {
						echo "<table style='width:90%;padding:0'>
							<tr>
								<th style='padding:0'>Name</th><td style='padding:0;width:90%'><input type='text' id='tagname-{$f->id}' name='tag[{$f->id}]' value='$f->tag' style='width:100%'></td>
								<td rowspan='2' style='width:1%;vertical-align: top;padding: 5px;'>
									<button data-tagid='{$f->id}' data-tagname='{$f->tag}' class='remove-field button button-primary els-update-btn' style='margin-bottom:0.5em'>Update</button>
									<button data-tagname='{$f->tag}' class='remove-field button button-primary els-del-btn' style='margin-bottom:0.5em'>Del</button>
								</td>
							</tr>
							<tr><th>Description</th><td style='padding:0;width:90%'><input type='text' id='tagdesc-{$f->id}' name='tagdescr[{$f->id}]' value='$f->tag_descr' style='width:100%'></td></tr>
							</table>
							<br/><br/>
							";
					}
					?>
						<br/>
					</td>
				</tr>
			</table>

		<form method="post" action="" enctype="multipart/form-data">
		<?php wp_nonce_field( 'els-useful-add', 'wpnonce-els-useful' ); ?>
		<table class="form-table">
			<tr class="form-field">
				<th scope="row">
					<label for"els_add">New default tag:</label>
				</th>
				<td>
				<?php if (isset($error_new)) { ?>
					<div style="border: 1px solid #faa; border-radius: 4px; height: 2em; background-color: #FFEAEB; padding: 8px 0px 0 8px;"><?php echo $error; ?></div>
				<?php } ?>
					<input id="new-tag" name="new-tag" type="text" value="<?php echo $_POST['new-tag']; ?>" style='width:80%'>
					<input type="submit" class="button-primary" value="<?php _e( 'Add', 'els-useful-for' ); ?>" />
				</td>
			</tr>
		</table>
		<p class="submit">
		</p>
	</form>
	
	<?php /*
	echo '<h3>' . __('Delete tag per post', 'els-useful-for') . '</h3>';
		
	if ( isset($_POST['wpnonce-els-select-post']) && wp_verify_nonce($_POST['wpnonce-els-select-post'], 'els-select-post') ) {
		$post_id = $_POST['post_id'];
		
		if ($_POST['post-tag-action'] == 'del' && isset($_POST['post-tag-tagname'])) {
			ELS_Useful_For_Lib::del_post_tag($post_id, $_POST['post-tag-tagname']);
		}
	}
	?>
	<form method="post" id="post-tag-save-del-tags">
		<?php wp_nonce_field( 'els-select-post', 'wpnonce-els-select-post' ); ?>
		<input type="hidden" id="post-tag-post_id" name="post_id" value="<?php echo $post_id; ?>"/>
		<input type="hidden" id="post-tag-action" name="post-tag-action" value=""/>
		<input type="hidden" id="post-tag-tagname" name="post-tag-tagname" value=""/>
		<input type="hidden" id="post-tag-tagdescr" name="post-tag-tagdescr" value=""/>
	</form>
	<table class="form-table">
		<tr class="form-field">
			<th scope="row">
				<label for"els_add">Select Material:</label>
			</th>
			<td>
				<select id="post-tag-select" name="post_id">
				<?php 
				$pages = get_posts( array( 'category' => 'resources', 'post_type' => 'post', 'posts_per_page' => 1000, 'orderby' => 'post_title', 'order' => 'ASC') );
				foreach($pages as $p) {
					echo "<option value='{$p->ID}'" . ( $post_id == $p->ID ? 'selected' : '' ) . ">{$p->post_title}</option>";
				} ?>
				</select>
				<input id="post-tag-select-post" type="button" class="button-primary" value="<?php _e( 'Select', 'els-useful-for' ); ?>" />
			</td>
		</tr>
		<?php if (isset($post_id)) {
			global $wpdb;
			$tags = ELS_Useful_For_Lib::get_all_tags_names($post_id, false);
			echo '<tr><th></th><td>';
			foreach($tags as $f) {
				echo "
					<input type='text' id='$f->id' name='tag[{$f->id}]' value='$f->tag ($f->tot)' style='width:80%' readonly>
					<button data-tagname='{$f->tag}' class='remove-field button button-primary els-del-post-tag-btn' style='margin-bottom:0.5em'>Del</button>
				";
			}
			echo '</td></tr>';
		} ?>
	</table>
	
	<?php */
		//echo '<p>Add:<br/><input id="new-tag" name="new-tag" type="text" value=""> 
		//	<span class="remove-field button button-primary button-large els_useful_def_add_btn" style="float:right">Add</span></p>';
	}

	public static function install() {
		require_once ABSPATH . 'wp-admin/includes/upgrade.php';
		$original_blog_id = get_current_blog_id();
		$sites = wp_get_sites();
		$vers = get_option(els_useful_for_dbVersion);
		$force = $vers != ELS_USEFUL_FOR_DB_VERSION;
		foreach($sites as $site) {
			switch_to_blog($site['blog_id']);
			ELS_Useful_For::create_table($force);
		}
		switch_to_blog($original_blog_id);
		add_option( 'els_useful_for_dbVersion', ELS_USEFUL_FOR_DB_VERSION );
	}

	public static function on_new_blog($blog_id, $user_id, $domain, $path, $site_id, $meta) {
		$original_blog_id = get_current_blog_id();
		switch_to_blog($blog_id);
		ELS_Useful_For::create_table();
		switch_to_blog($original_blog_id);
	}

	private static function create_table($force) {
		global $wpdb;
		$table_name = $wpdb->prefix . "els_useful_for";

		if ( $force || $wpdb->get_var( "show tables like '$table_name'" ) != $table_name ) {
			$sql = "CREATE TABLE $table_name (
					`id` bigint(11) NOT NULL AUTO_INCREMENT,
					`post_id` int(11) NOT NULL,
					`date_time` datetime NOT NULL,
					`user_id` int(11) NOT NULL,
					`tag` varchar(255) NOT NULL,
					`tag_descr` varchar(512) NOT NULL,
					`used` int(11) NOT NULL,
					PRIMARY KEY (`id`)
				);";

			dbDelta( $sql );
		}
	}
}

register_activation_hook( __FILE__, array( 'ELS_Useful_For', 'install' ) );
add_action( 'wpmu_new_blog', array( 'ELS_Useful_For', 'on_new_blog' ));

new ELS_Useful_For();
