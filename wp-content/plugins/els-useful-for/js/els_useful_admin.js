jQuery(document).ready(function($) 
{
	$('.els-del-btn').click(function(e) {
        e.preventDefault();
		//var id = e.target.getAttribute("data-id");
		var tagname = e.target.getAttribute("data-tagname");
		if (tagname != null) {
			if (confirm("Are you sure?")) {
				document.getElementById("action").value="del";
				document.getElementById("tagname").value=tagname;
				document.getElementById("save-del-tags").submit();
			}
		}
	});
	
	$('.els-update-btn').click(function(e) {
        e.preventDefault();
		var id = e.target.getAttribute("data-tagid");
		var tagname = e.target.getAttribute("data-tagname");
		if (tagname != null) {
			var iName = $('input[id=tagname-'+id+']');
			var iDesc = $('input[id=tagdesc-'+id+']');
			if (iName.val() != "" && iName.val() != undefined) {
				if (confirm("Are you sure?")) {
					document.getElementById("action").value="update";
					document.getElementById("tagid").value=tagname;
					document.getElementById("tagname").value=iName.val();
					document.getElementById("tagdescr").value=iDesc.val();
					document.getElementById("save-del-tags").submit();
				}
			}
			else {
				alert("tagname cannot be empty.");
			}
		}
	});

	$('.els-save-btn').click(function(e) {
        e.preventDefault();
		var id = e.target.getAttribute("data-id");
		if (id != null && Number.parseInt(id) > 0) {
			if (confirm("Are you sure?")) {
				var input = $('input[id='+id+']');
				if (input.val() != "") {
					document.getElementById("action").value="save";
					document.getElementById("tagid").value=id;
					document.getElementById("tagvalue").value=input.val();
					document.getElementById("save-del-tags").submit();
				}
			}
		}
	});
	
	$('#post-tag-select-post').click(function(e) {
        e.preventDefault();
		document.getElementById("post-tag-action").value="select";
		document.getElementById("post-tag-post_id").value=$("#post-tag-select").val();
		document.getElementById("post-tag-save-del-tags").submit();
	});
	
	$('.els-del-post-tag-btn').click(function(e) {
        e.preventDefault();
		var tagname = e.target.getAttribute("data-tagname");
		if (tagname != null) {
			if (confirm("Are you sure?")) {
				document.getElementById("post-tag-action").value="del";
				document.getElementById("post-tag-tagname").value=tagname;
				document.getElementById("post-tag-save-del-tags").submit();
			}
		}
	});

});
