jQuery(document).ready(function($) 
{
	$(".point_selectable").click( function(e) {
		e.preventDefault();
		var tagtag=e.target.getAttribute("data-tag");
		var tagval=e.target.getAttribute("data-tagval");
		document.getElementById("tagtag").value=tagtag;
		document.getElementById("tagval").value=tagval;
		document.getElementById("rate_mat").submit();
	});
	$(".click_to_add_tag").click(function(e){
		var t = document.getElementById("add_tag").value;
		if (t.length < 3) {
			alert("Tag name too short.");
			return;
		}
		document.getElementById("tagtag").value=t;
		document.getElementById("tagval").value=0;
		document.getElementById("rate_mat").submit();
	});
});