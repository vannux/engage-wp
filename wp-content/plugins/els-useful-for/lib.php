<?php
if (defined('ELS_USEFUL_FOR_POST_MARKER')) return;

define( 'ELS_USEFUL_FOR_POST_MARKER', '__POST__' );

class ELS_Useful_For_Lib {

	/**
	 * common tags for all posts
	 * tag : string
	 */
	 public static function get_default_tags() {
		global $wpdb;
		$table_name = $wpdb->prefix . "els_useful_for";
		return $wpdb->get_results( "select * from $table_name where post_id=-1 order by tag DESC");
	}

	/**
	 * common tags for all posts
	 * tag : string
	 */
	public static function add_default_tag($tag) {
		global $wpdb;
		$table_name = $wpdb->prefix . "els_useful_for";

		$up_tag = strtoupper($tag);
		$row = $wpdb->get_row( "select * from $table_name where post_id=-1 and UPPER(tag)='$up_tag'");
		if (!$row->id) {
			$sqldate = date("Y-m-d H:i:s");
			$r = $wpdb->insert($table_name, array('post_id'=>-1, 'date_time'=>$sqldate, 'user_id'=>-1, 'tag'=>$tag));
			return $r;
		}
		return false;
	}
	
	public static function delete_tag_id($tagid) {
		global $wpdb;
		$table_name = $wpdb->prefix . "els_useful_for";
		$r = $wpdb->delete($table_name, array('id'=>$tagid));
		return $r;
	}
	
	public static function delete_all_tag_ref($tagname) {
		global $wpdb;
		$table_name = $wpdb->prefix . "els_useful_for";
		$tag_u = strtoupper($tagname);
		return $wpdb->get_results("delete from $table_name where UPPER(tag)='{$tag_u}'");
	}
	
	public static function del_post_tag($post_id, $tagname) {
		global $wpdb;
		$table_name = $wpdb->prefix . "els_useful_for";
		$tag_u = strtoupper($tagname);
		return $wpdb->get_results("delete from $table_name where post_id=$post_id and UPPER(tag)='{$tag_u}'");
	}
	
	public static function update_tag_id($tagid, $tagval, $tagdescr="") {
		global $wpdb;
		$table_name = $wpdb->prefix . "els_useful_for";
		$r = $wpdb->update($table_name, array('tag'=>$tagval, 'tag_descr'=>$tagdescr), array('id'=>$tagid) );
		if (!$r) return $wpdb->last_error;
		return true;
	}
	
	public static function update_tag_name($prev_tagname, $new_tagname, $tagdescr="") {
		global $wpdb;
		$table_name = $wpdb->prefix . "els_useful_for";
		$r = $wpdb->update($table_name, array('tag'=>$new_tagname, 'tag_descr'=>$tagdescr), array('tag'=>$prev_tagname) );
		if (!$r) return $wpdb->last_error;
		return true;
	}
	
	
	

	/**
	 * Check case insensitive
	 */
	public static function is_default_tag_present($tag) {
		global $wpdb;
		$table_name = $wpdb->prefix . "els_useful_for";
		$tag_u = strtoupper($tag);
		$r = $wpdb->get_results( "select * from $table_name where post_id=-1 and UPPER(tag)='{$tag_u}' order by tag DESC");
		return $r;
	}
	
	/**
	 * tag : string
	 * average_rating: 0..5
	 * tot : int
	 */
	public static function get_avg_tags($post_id, $tag = null) {
		global $wpdb;
		$table_name = $wpdb->prefix . "els_useful_for";
		$w = $tag == null ? '' : 'AND UPPER(tag)="'. strtoupper($tag).'"';
		$r1 = $wpdb->get_results("select tag, tag_descr, count(id) tot, avg(used) average_rating from $table_name where post_id=$post_id $w group by tag order by tag");
		$r2 = $wpdb->get_results("select tag, tag_descr from $table_name where tag not in (select tag FROM $table_name where post_id=$post_id) AND post_id=-1 $w order by tag");
		return array_merge($r1, $r2);
	}

	/**
	 * post tags + default tags
	 * tag : string
	 * average_rating: 0..5
	 * tot : int
	 */
	public static function get_all_tags_names($post_id, $dafaults = true) {
		global $wpdb;
		$table_name = $wpdb->prefix . "els_useful_for";
		$q = $dafaults ? 
			"select id, tag, tag_descr, count(id) tot from $table_name where (post_id=$post_id OR post_id=-1) group by tag order by tag" :
			"select id, tag, tag_descr, count(id) tot from $table_name where (post_id=$post_id) and tag not in (select tag FROM $table_name where post_id=-1) group by tag order by tag" ;
		return $wpdb->get_results( $q );
	}

	/**
	 * tag,
	 * user_rating
	*/
	public static function get_user_tags($post_id, $user_id) {
		global $wpdb;
		$table_name = $wpdb->prefix . "els_useful_for";
		return $wpdb->get_results( "select id, tag, tag_descr, used from $table_name where post_id=$post_id and user_id=$user_id group by tag order by tag");
	}

	public static function set_user_tag($post_id, $user_id, $tag, $tagval) {
		global $wpdb;
		$table_name = $wpdb->prefix . "els_useful_for";
		$sqldate = date("Y-m-d H:i:s");
		$tag_u = strtoupper($tag);
		
		$tagid = $wpdb->get_var("select id from $table_name where user_id=$user_id and post_id=$post_id and upper(tag)='$tag_u'");
		$r = true;
		if ($tagid && $tagval == 0) {
			$r = $wpdb->delete($table_name, array('id'=>$tagid));
		}
		else if ($tagid) {
			$r = $wpdb->update($table_name, array('date_time'=>$sqldate, 'used'=>$tagval), array('id'=>$tagid));
		}
		else if ($tagval > 0) {
			$ttag = $wpdb->get_var("select tag from $table_name where (post_id=-1 or post_id=$post_id) and upper(tag)='$tag_u'");
			if (!$ttag) $ttag = $tag;
			$r = $wpdb->insert($table_name, array('date_time'=>$sqldate, 'post_id'=>$post_id, 'user_id'=>$user_id, 'tag'=>$ttag, 'used'=>$tagval));
		}
		return $r;
	}
		
	/*
	public static function get_tags($post_id) {
		global $wpdb;
		$table_name = $wpdb->prefix . "els_useful_for";
		return $wpdb->get_results( "select tag, count(id) tot from $table_name where post_id=$post_id and tag!='".ELS_USEFUL_FOR_POST_MARKER."' group by tag order by tot DESC");
	}

	public static function set_used($post_id, $user_id, $used=1) {
		global $wpdb;
		$table_name = $wpdb->prefix . "els_useful_for";

		$row = $wpdb->get_row( "select * from $table_name where user_id=$user_id and post_id=$post_id and tag='".ELS_USEFUL_FOR_POST_MARKER."'");
		$sqldate = date("Y-m-d H:i:s");
		if ($row->id) {
			$r = $wpdb->update($table_name, array('date_time'=>$sqldate, 'used'=>$used), array('id'=>$row->id));
		}
		else {
			$r = $wpdb->insert($table_name, array('post_id'=>$post_id, 'date_time'=>$sqldate, 'user_id'=>$user_id, 'tag'=>ELS_USEFUL_FOR_POST_MARKER, 'used'=>$used));
		}
		return $r;
	}

	public static function get_used($post_id, $user_id) {
		global $wpdb;
		$table_name = $wpdb->prefix . "els_useful_for";

		$row = $wpdb->get_row( "select * from $table_name where user_id=$user_id and post_id=$post_id and tag='".ELS_USEFUL_FOR_POST_MARKER."'");
		if ($row->id) {
			return $row->used;
		}
		return 0;
	}

	public static function get_user_tags($post_id, $user_id) {
		global $wpdb;
		$table_name = $wpdb->prefix . "els_useful_for";
		return $wpdb->get_results( "select tag, used, count(id) tot from $table_name where post_id=$post_id and user_id=$user_id and tag!='".ELS_USEFUL_FOR_POST_MARKER."' group by tag order by tot DESC");
	}

	public static function set_useful($post_id, $user_id, $tag, $useful) {
		global $wpdb;
		$table_name = $wpdb->prefix . "els_useful_for";
		$up_tag = strtoupper($tag);
		$row = $wpdb->get_row( "select * from $table_name where user_id=$user_id and post_id=$post_id and UPPER(tag)='$up_tag'");
		$sqldate = date("Y-m-d H:i:s");
		if ($row->id) {
			//if ($useful==0) {
			//	$r = $wpdb->delete($table_name, array('id'=>$row->id));
			//}
			//else {
				$r = $wpdb->update($table_name, array('date_time'=>$sqldate, 'used'=>$useful), array('id'=>$row->id));
			//}
		}
		else {
			$r = $wpdb->insert($table_name, array('post_id'=>$post_id, 'date_time'=>$sqldate, 'user_id'=>$user_id, 'tag'=>$tag, 'used'=>$useful));
		}
		return $r;
	}
	*/
}
