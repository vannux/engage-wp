<?php

require_once dirname(__FILE__).'/lib.php';
require_once dirname(__FILE__).'/ajax-frontend.php';

wp_enqueue_style( 'els-useful-for.css',  plugins_url('stylesheet.css', __FILE__) );
	
add_action( 'comment_list_before', 'useful_for_review' );

function getTempl($tag, $tag_descr, $avg){
	return '
<table class="els-uf-rating-rate">
<tr>
	<td>'.$tag.' <img title="'.$tag_descr.'" class="els-rating-rate-item-info" src="'. get_template_directory_uri().'/images/info_small.jpg"></img></td>
</tr>
<tr>
	<td class="els-uf-rating-rate-2nd">
			<div class="els-rating-rate-item-point els-rating-rate-item-point_'.($avg > 0 ? 'e' : 'd').' point_selectable" data-tagval=1 data-tag="'.$tag.'"></div>
			<div class="els-rating-rate-item-point els-rating-rate-item-point_'.($avg > 1 ? 'e' : 'd').' point_selectable" data-tagval=2 data-tag="'.$tag.'"></div>
			<div class="els-rating-rate-item-point els-rating-rate-item-point_'.($avg > 2 ? 'e' : 'd').' point_selectable" data-tagval=3 data-tag="'.$tag.'"></div>
			<div class="els-rating-rate-item-point els-rating-rate-item-point_'.($avg > 3 ? 'e' : 'd').' point_selectable" data-tagval=4 data-tag="'.$tag.'"></div>
			<div class="els-rating-rate-item-point els-rating-rate-item-point_'.($avg > 4 ? 'e' : 'd').' point_selectable" data-tagval=5 data-tag="'.$tag.'"></div>
			<div class="click_to_rate_arrow"></div>
			<div class="click_to_rate">' . __('Click to rate', 'els-useful-for') . '</div>
	</td>
</tr>
<tr>
	<td class="els-uf-rating-rate-3rd"><span style="display:'.($avg>0?'inline':'none').'" class="withdraw-my-vote point_selectable" data-tag="'.$tag.'" data-tagval=0>'.__('Withdraw my vote', 'els-useful-for').'</span></td>
</tr>
</table>
';
}

		function useful_for_review() {
	$post = get_post();
	$post_id = $post->ID;
	$user_id = get_current_user_id();
	if ( $user_id == 0 ) return;
	
  $user_tags = ELS_Useful_For_Lib::get_user_tags($post_id, $user_id);
  $all_tags = ELS_Useful_For_Lib::get_all_tags_names($post_id);

  //wp_enqueue_script('els_useful_client', plugins_url('js/els_useful_client.js', __FILE__), ['jquery']);

  foreach($user_tags as $ut) {
	  unset($key);
	  foreach($all_tags as $k=>$v) {
			if (strtolower($ut->tag) == strtolower($v->tag)) {
				$key = $k;
				break;
			}
	  }
	  if (isset($key)) unset($all_tags[$key]);
  }
	?>
  <hr/>	
  <span class="post_reviews"><?php _e('Please rate and provide a comment', 'els-useful-for');?></span>
	<div class="els-rating-rate">
    <?php 
		
	foreach ($user_tags as $value) {
		$avg = $value->used;
		$tagid = $value->id;
		$tag = $value->tag;
		echo getTempl($tag, $value->tag_descr, $avg);
	} 
	foreach ($all_tags as $value) {
    $tagid = -1;
	  $tag = $value->tag;
		echo getTempl($tag, $value->tag_descr, -1);
  } 
	?>
    <div class="clear"></div>
	
	<script>
jQuery(document).ready(function($) {
	function els_useful_def_add(e) {
		e.preventDefault();
		var $b=$(e.target);
		var tagtag=e.target.getAttribute("data-tag");
		var tagval=e.target.getAttribute("data-tagval");
		
		var data = {
			action: 'els_useful_for_rate',
			tag: tagtag,
			tagval: tagval,
			post_id: <?php echo $post_id; ?>,
			nonce: '<?php echo wp_create_nonce( "els_useful_for_nonce" );?>'
		};
		
		$.post( '<?php echo admin_url( 'admin-ajax.php' ); ?>', data, function( resp ) {
			if (resp.error == 0) {
				//var c = $b.parent().find(".els-rating-rate-item-point");
				var table = $b.parent();
				while(!table.hasClass("els-uf-rating-rate")) table=table.parent();
				c = table.find(".els-rating-rate-item-point");
				for(var i=0,l=c.length; i<l; i++) {
					var p = $(c[i]);
					if (p.attr("data-tagval") <= tagval) {
						p.removeClass("els-rating-rate-item-point_d");
						p.addClass("els-rating-rate-item-point_e");
					}
					else {
						p.removeClass("els-rating-rate-item-point_e");
						p.addClass("els-rating-rate-item-point_d");
					}
				}
				if (tagval==0) {
					table.find(".els-uf-rating-rate-3rd > span").css("display", "none");
				}
				else {
					table.find(".els-uf-rating-rate-3rd > span").css("display", "inline");
				}
				
				var items = $(".els-rating >  .els-rating-rate > .els-rating-rate-item");
				var avg = resp.avg ? resp.avg : 0;
				for(var j=0,l=items.length; j<l; j++) {
					var $t = $(items[j]);
					if ($t.find(".els-rating-rate-item-title").attr('data-tag')==tagtag) {
						var c = $t.find('.els-rating-rate-item-point');
						for(var i=0,l=c.length; i<l; i++) {
							var p = $(c[i]);
							if (p.attr("data-tagval") <= avg) {
								p.removeClass("els-rating-rate-item-point_d");
								p.addClass("els-rating-rate-item-point_e");
							}
							else {
								p.removeClass("els-rating-rate-item-point_e");
								p.addClass("els-rating-rate-item-point_d");
							}
						}
						var nrVotes = resp.nrVotes || 0;
						$t.find('.els-rating-votes')[0].innerHTML = nrVotes + " " + resp.strVotes;
					}
				}
				
			}
			else {
				console.log("ERROR");
			}
		});
	}
	$('.point_selectable').click(els_useful_def_add);
});
</script>

  </div>
  <?php
}
