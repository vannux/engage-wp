<?php 

add_action('wp_ajax_els_useful_def_add', 'els_useful_def_add_ajax');
add_action('wp_ajax_nopriv_els_useful_def_add', 'els_useful_def_add_ajax');

add_action('wp_ajax_els_useful_def_del', 'els_useful_def_del_ajax');
add_action('wp_ajax_nopriv_els_useful_def_del', 'els_useful_def_del_ajax');


function els_useful_def_add_ajax() {
	require_once dirname(__FILE__).'/../lib.php';
	check_ajax_referer( 'els_useful_for_nonce', 'nonce' );
	//$user_id = get_current_user_id();
	$tag = $_POST['tag'];
	$r = ELS_Useful_For_Lib::add_def_tag($tag);
	$resp = array('r'=>$r);
	wp_send_json( $resp );
	exit;
}

function els_useful_def_del_ajax() {
	require_once dirname(__FILE__).'/../lib.php';
	check_ajax_referer( 'els_useful_for_add_nonce', 'nonce' );
	$user_id = get_current_user_id();
	$tag = strip_tags($_POST['tag']);
	$r = ELS_Useful_For_Lib::del_def_tag($tag);
	$resp = array('r'=>$r, 'tag'=>$tag);
	wp_send_json( $resp );
	exit;
}
