<?php 

add_action('wp_ajax_els_useful_for', 'els_useful_for_ajax');
add_action('wp_ajax_nopriv_els_useful_for', 'els_useful_for_ajax');

add_action('wp_ajax_els_useful_for_add', 'els_useful_for_add_ajax');
add_action('wp_ajax_nopriv_els_useful_for_add', 'els_useful_for_add_ajax');

function els_useful_for_ajax() {
	require_once dirname(__FILE__).'/../lib.php';
	
	check_ajax_referer( 'els_useful_for_nonce', 'nonce' );
	$user_id = get_current_user_id();
	$post_id = $_POST['post_id'];
	$action = $_POST['action'];
	$useful = $_POST['useful'];
	$tag = $_POST['tag'];
	if ($useful == 0) {
		$u = 1;
	}
	else {
		$u = 0;
	}
	$r = ELS_Useful_For_Lib::set_useful($post_id, $user_id, $tag, $u);
	if (!$r) $u=$used;
	$resp = array('useful'=>$u, 'r'=>$r);
	wp_send_json( $resp );
	exit;
}

function els_useful_for_add_ajax() {
	require_once dirname(__FILE__).'/../lib.php';
	
	check_ajax_referer( 'els_useful_for_add_nonce', 'nonce' );

	$user_id = get_current_user_id();
	$post_id = $_POST['post_id'];
	$action = $_POST['action'];
	$tag = strip_tags($_POST['tag']);
	$r = ELS_Useful_For_Lib::set_useful($post_id, $user_id, $tag, 1);
	if ($r) {
		$u=1;
	}
	else {
		$u=0;
	}		
	$resp = array('useful'=>$u, 'r'=>$r);
	wp_send_json( $resp );
	exit;
}
