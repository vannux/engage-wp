<?php 

class ELS_Provide_Feedback_Widget extends WP_Widget
{
	function ELS_Provide_Feedback_Widget() {
		parent::WP_Widget(false, $name = __('ELS Provide Feedback', 'els-useful-for') );
	}

	function form($instance) {
		if( $instance) {
			$title = esc_attr($instance['title']);
		} else {
			$title = '';
		}
		?>
		<p>
<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Widget Title', 'wp_widget_plugin'); ?></label>
<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
</p>

		<?php
	}
	
	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		return $instance;
	}
		
	function widget($args, $instance) {
		extract( $args );
		require_once dirname(__FILE__).'/../lib.php';
		
		$post = get_post();
		$post_id = $post->ID;
		$user_id = get_current_user_id();

		if ($user_id) {
		?>
		<script>
jQuery(document).ready(function($) 
{
	function els_used_count_btn_click(e)
    {
        e.preventDefault();
		var b = $(e.target);
		b.unbind('click');
		var used=b.data("used");
        var data = {
            action: 'els_used_plus',
			used: used,
			post_id: '<?php echo $post_id; ?>',
            nonce:  '<?php echo wp_create_nonce( "els_used_plus_nonce" );?>'
        };

        $.post( '<?php echo admin_url( 'admin-ajax.php' ); ?>', data, function( response ) 
        {
			b.bind('click', els_used_count_btn_click);
			b.data('used', response.used);
			if (response.used==1) {
				b.html('<?php _e('Yes','els-useful-for');?>');
			}
			else {
				b.html('+1');
			}
        });
	}
	$('.els-used-count-btn').click(els_used_count_btn_click);
});
</script>
<?php
		} // end if $user_id
		
		// these are the widget options
		$title = apply_filters('widget_title', $instance['title']);
		
		echo $before_widget;

		echo '<div class="widget-text wp_widget_plugin_box">';

		// Check if title is set
		if ( $title ) {
			echo $before_title . $title . $after_title;
		}
		else {
			echo $before_title . 'Provide your feedback:' . $after_title;
		}

		//if (has_post_thumbnail($post->ID)) {
		//	echo get_the_post_thumbnail($single->ID, null, array('class' => "fullwidth" ));
		//}
		//else {
		//	$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
		//	$first_img = $matches[1][0];
		//	if (empty($first_img)) {
		//			$first_img = get_template_directory_uri().'/images/news.png';
		//	}
		//	echo "<img src='$first_img' class = 'fullwidth'>";
		//}
		
		
		if ( comments_open() ) { 
			echo '<span class="comments-link" style="margin-left:0em"><a href="'. get_comments_link() . '">'. __( 'Comments') . ' (' . $post->comment_count . ')</a></span><br/>';
		}
		if(function_exists('wp_ulike')) {
			echo '<div style="float:left;margin-right:10px">';
			wp_ulike('get');
			echo '</div>';
		}
		if ($user_id) {
			$used = ELS_Useful_For_Lib::get_used($post_id, $user_id);
			$str_used = $used == 0 ? '+1' : __('Yes','els-useful-for');
			echo '<div class="els-used-count">'. __('Used:', 'els-useful-for'). '<button class="els-used-count-btn" data-used="'.$used.'">'. $str_used .'</button></div>';
		}
		echo '</div>'; // widget-box
		echo $after_widget;
	}

}

add_action('widgets_init', create_function('', 'return register_widget("ELS_Provide_Feedback_Widget");'));
add_action('wp_ajax_els_used_plus', 'els_used_plus_ajax');
add_action('wp_ajax_nopriv_els_used_plus', 'els_used_plus_ajax');
		
function els_used_plus_ajax() {
	require_once dirname(__FILE__).'/../lib.php';
	
	check_ajax_referer( 'els_used_plus_nonce', 'nonce' );
	$user_id = get_current_user_id();
	if (!isset($user_id)) {
		echo 'User not logged';
		die();
	}
	
	$action = $_POST['action'];
	$used = $_POST['used'];
	$post_id = intval($_POST['post_id']);

	if ($used == 0) {
		$u = 1;
	}
	else {
		$u = 0;
	}
	
	$r = ELS_Useful_For_Lib::set_used((int)$post_id, (int)$user_id, $u);
	if (!$r) $u=$used;
	$resp = array('used'=>$u, 'r'=>$r);
	wp_send_json( $resp );
	exit;
}

