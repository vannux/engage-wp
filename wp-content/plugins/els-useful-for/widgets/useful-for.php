<?php 

class ELS_Useful_For_Widget extends WP_Widget
{
	function ELS_Useful_For_Widget() {
		parent::WP_Widget(false, $name = __('ELS Useful for', 'els-useful-for') );
	}

	function form($instance) {
		require_once dirname(__FILE__).'/../lib.php';
		require_once dirname(__FILE__).'/ajax-admin.php';

		$title = isset( $instance['title'] ) ? $instance['title'] : '';
		$tags = isset( $instance['tags'] ) ? $instance['tags'] : array();
		
		?>
<p>
<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Widget Title', 'wp_widget_plugin'); ?></label>
<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
</p>
<script>
jQuery(document).ready(function($) 
{
	function els_useful_def_del(e)
    {
        e.preventDefault();
		var b = $(e.target);
		b.parent().detach();
    }
	
	function els_useful_def_add(e)
    {
        e.preventDefault();
		var b = $(e.target);
		var n= $('#<?php echo $this->get_field_id('new-tag'); ?>');
		var tag=n.val();
		if (tag == '') return;
		
		var l = $('#<?php echo $this->get_field_id('list-tags'); ?>');
		var btn = $("<span class='remove-field button button-primary button-large els_useful_def_del_btn' style='float:right'>Del</span>");
		var c = $('#<?php echo $this->get_field_id('list-tags'); ?> p');
		var nb = $("<p><input type='text' name='<?php echo $this->get_field_name( 'tags' );?>["+c.length+"]' value='"+tag+"'>");
		nb.append(btn);
		l.append(nb);
		btn.click(els_useful_def_del);
    }
	$('.els_useful_def_add_btn').click(els_useful_def_add);
	$('.els_useful_def_del_btn').click(els_useful_def_del);

});
</script>
		<?php
		echo "<div id='{$this->get_field_id( 'list-tags' )}'>";
		$field_name = $this->get_field_name( 'tags' );
		$i = 0;
		foreach($tags as $f) {
			echo "<p><input type='text' name='{$field_name}[$i]' value='$f'> <span class='remove-field button button-primary button-large els_useful_def_del_btn' style='float:right'>Del</span></p>";
			$i++;
		}
		echo '</div>';
		
		echo "<p>Add:<br/><input id='{$this->get_field_id('new-tag')}' name='{$this->get_field_name('new-tag')}' class='' type='text' value=''> <span class='remove-field button button-primary button-large els_useful_def_add_btn' style='float:right'>Add</span></p>";
		
	}
	
	function update($new_instance, $old_instance) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['tags'] = array();
		
		if ( isset( $new_instance['tags'] ) )
        {
            foreach ( $new_instance['tags'] as $tag )
            {
                if ( '' !== trim( $tag ) )
                    $instance['tags'][] = $tag;
            }
        }
		
		return $instance;
	}
		
	function widget($args, $instance) {
		extract( $args );
		require_once dirname(__FILE__).'/../lib.php';
		
		$post = get_post();
		$post_id = $post->ID;
		$user_id = get_current_user_id();

		// these are the widget options
		$title = apply_filters('widget_title', $instance['title']);
		
		wp_enqueue_style('els-useful-for', plugins_url() . '/els-useful-for/els-useful-for.css');

		echo $before_widget;

		echo '<div class="widget-text wp_widget_plugin_box">';

		// Check if title is set
		if ( $title ) {
			echo $before_title . $title . $after_title;
		}
		else {
			echo $before_title . 'Useful for:' . $after_title;
		}
		if ($user_id) {
			$tags = ELS_Useful_For_Lib::get_tags($post_id);
			$user_tags = ELS_Useful_For_Lib::get_user_tags($post_id, $user_id);
			$def_tags = isset( $instance['tags'] ) ? $instance['tags'] : array();
?>
<script>
jQuery(document).ready(function($) 
{
	var tags = [<?php 
		echo '"' . join('","',$def_tags) . '"';
		if (count($def_tags) > 0 && count($tags) > 0) echo ',';
		foreach($tags as $t) echo "'{$t->tag}',"; 
	?>];
	function els_useful_for_btn_click(e)
    {
        e.preventDefault();
		var b = $(e.target);
		b.unbind('click');
		var useful=b.data("useful");
		var tag=b.data("tag");

        var data = {
            action: 'els_useful_for',
			tag: tag,
			useful:useful,
			post_id: <?php echo $post_id; ?>,
            nonce: '<?php echo wp_create_nonce( "els_useful_for_nonce" );?>'
        };

        $.post( '<?php echo admin_url( 'admin-ajax.php' ); ?>', data, function( response ) 
        {
			b.bind('click', els_useful_for_btn_click);
			b.data('useful', response.useful);
			//console.log(response);
			if (response.useful==1) {
				b.html('<?php _e('Yes','els-useful-for');?>');
			}
			else {
				b.html('+1');
			}
        });
    }
	function els_useful_for_btn_add_click(e)
    {
        e.preventDefault();
		var b = $(e.target);
		var n= $('#new-tag');
		var tag=n.val();
		if (tag == '') return;
		var isPre = false;
		for(var i = 0 , l = tags.length ; i <l ; i++) {
			if (tags[i].toLowerCase() == tag.toLowerCase()) {
				isPre = true;
				break;
			}
		}
		if (isPre) {
			alert('"' + tag + '" already present.');
			return;
		}
		n.attr("readonly", "readonly");
		b.unbind('click');
        var data = {
            action: 'els_useful_for_add',
			tag: tag,
			post_id: <?php echo $post_id; ?>,
            nonce: '<?php echo wp_create_nonce( "els_useful_for_add_nonce" );?>'
        };

        $.post( '<?php echo admin_url( 'admin-ajax.php' ); ?>', data, function( response ) 
        {
			//console.log(response);
			b.bind('click', els_useful_for_btn_add_click);
			n.removeAttr('readonly');
			if (response.useful==1) {
				var l = $('#useful-list');
				var nb = $("<button class='els-useful-item els-useful-item-enabled' data-tag='tag' data-useful='1'><?php _e('Yes','els-useful-for');?></button>"+tag+"<br/>");
				l.append(nb);
				nb.click(els_useful_for_btn_click);
			}
        });
    }
	$('.els-useful-item').click(els_useful_for_btn_click);
	$('.els-useful-btn-add').click(els_useful_for_btn_add_click);

});
</script>
<?php 
		} // if user logged
		echo '<div id="useful-list">';
		foreach($def_tags as $tag) {
			$u = false;
			foreach($user_tags as $ut) {
				if ($ut->tag == tag && $ut->used == 1) {
					$u=true;
					break;
				}
			}
			$b_t = $u ? __('Yes', 'els-useful-for') : '+1';
			$b_d = $user_id ? '':'disabled="disabled"';
			$b_c = $user_id ? 'els-useful-item-enabled':'els-useful-item-disabled';
			$b_u = $u? '1':'0';
			echo "<button $b_d class='els-useful-item $b_c' data-tag='{$tag}' data-useful='$b_u'>$b_t</button>{$tag}<br/>";
		}

		foreach($tags as $tag) {
			$is_def_tag = false;
			foreach($def_tags as $t) {
				if (strtolower($t) == strtolower($tag->tag)) {
					$is_def_tag = true;
					break;
				}
			}
			if ($is_def_tag) continue;
			
			$u = false;
			foreach($user_tags as $ut) {
				if ($ut->tag == $tag->tag && $ut->used == 1) {
					$u=true;
					break;
				}
			}
			$b_t = $u ? __('Yes', 'els-useful-for') : '+1';
			$b_d = $user_id ? '':'disabled="disabled"';
			$b_c = $user_id ? 'els-useful-item-enabled':'els-useful-item-disabled';
			$b_u = $u? '1':'0';
			echo "<button $b_d class='els-useful-item $b_c' data-tag='{$tag->tag}' data-useful='$b_u'>$b_t</button>{$tag->tag}<br/>";
		}
		echo '</div>';
		if ($user_id) {
?>
<div style="margin-top:1em"><?php _e('Other:', 'els-useful-for'); ?></div>
<input id="new-tag" type="text" value="" class="els-useful-newitem"><button class="els-useful-btn-add"><?php _e('Add', 'els-useful-for'); ?></button>
<?php
		}
		echo '</div>';
		echo $after_widget;
	}

}

add_action('widgets_init', create_function('', 'return register_widget("ELS_Useful_For_Widget");'));
require_once dirname(__FILE__).'/ajax-frontend.php';
 
