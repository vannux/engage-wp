<?php
/*
Plugin Name: ELS Users Profile Promote Button
Plugin URI: 
Description: Shows the button <i>Promote myself</i> in the user-profile page.
Version: 0.1.0
Author: Mauro Vannucci
Author URI: 
Copyright 2014  Mauro Vannucci (email : vannux@gmail.com)
Permission to add/modify/copy are subject to the author.
*/

define('ELS_PROMOTE_ADAPTER', 'ELS_PROMOTE_ADAPTER');

class ELS_user_profile_promote_button
{
	public function __construct() {
		$action = ( IS_PROFILE_PAGE ? 'show' : 'edit' ) . '_user_profile';
		add_action( $action, array ( $this, 'show_user_profile' ));
		
		wp_enqueue_script( 'jquery', plugins_url( '/jquery-2.0.2.min.js', __FILE__ ), null);
		wp_enqueue_style( 'jquery-ui', plugins_url( '/jq/jquery-ui.min.css', __FILE__ ) );
		wp_enqueue_style( 'jquery-ui-s', plugins_url( '/jq/jquery-ui.structure.min.css', __FILE__ ) );
		wp_enqueue_style( 'jquery-ui-t', plugins_url( '/jq/jquery-ui.theme.min.css', __FILE__ ) );
		wp_enqueue_script( 'jquery-ui', plugins_url( '/jq/jquery-ui.min.js', __FILE__ ), array('jquery'));
		wp_enqueue_script( 'ELS_user_profile_promote_button', plugins_url( '/mod.js', __FILE__ ), array('jquery', 'jquery-ui'), null, true );

		wp_localize_script( 'ELS_user_profile_promote_button', 'MyAjax', array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'nonce'   => wp_create_nonce( 'ELS_user_profile_promote_button' ),
			)
		);

	}
	
	function show_user_profile($user) {
		// 0 = not asked
		// 1 = asked
		// 2 = confirmed
		$opt = get_user_option( ELS_PROMOTE_ADAPTER, $user->id );
	?>
	<h2><?php _e('Adapter', 'user_profile_promote_button'); ?></h2>
	
	<table class="form-table">
		<tr>
			<th>
				<label for="promotemyself"><?php _e('Promote Myself', 'user_profile_promote_button'); ?></label>
			</th>
			<td>
			<?php if (!$opt || $opt == '0') { ?>
				<span id="ajax-error-box" class="error hidden"><br/></span>
				<span id="ajax-msg-box">
					<input type="button" name="promotemyself" id="btnPromotemyself" href="" class="button button-primary" value="<?php _e('Promote Myself', 'user_profile_promote_button'); ?>"/>
					&nbsp;<span id="showspinner" class="spinner" style="float:none"></span>
					<br/><p style="margin-top:0.2em" class=""><?php _e('Click here to promote yourself as <i>Adapter</i>', 'user_profile_promote_button'); ?></p>
				</span>
			<?php } else if ($opt == '1') { 
				_e('You already asked to become <i>Adapter</i> please wait for confirmation.', 'user_profile_promote_button');
			} else {
				_e('You are an <i>Adapter</i>.', 'user_profile_promote_button');
			}
			?>
			</td>
		</tr>
	</table>
	<?php
	}
	
}

function ELS_user_profile_promote_button_start() {
	new ELS_user_profile_promote_button();
}

function ELS_user_profile_promote_button_ajax() {
	$nonce = $_POST['nonce'];
	if ( ! wp_verify_nonce( $nonce, 'ELS_user_profile_promote_button') )
		die ( 'Busted!');

	$response = array();
	header( 'Content-Type: application/json' );
	
	$user_id = wp_get_current_user()->ID;
	
	//$response['error'] = __('You have not the rights to query data.','user_profile_promote_button');
	//$protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
	//header($protocol . ' 403 Forbidden', true, 403);
	//echo json_encode($response);
	//exit;
	
	$res = update_user_option( $user_id, ELS_PROMOTE_ADAPTER, 1, true );
	
	$response['msg'] = __('Request submitted.', 'user_profile_promote_button');
	echo json_encode($response);
	exit;
}

add_action( 'wp_ajax_promote_myself', 'ELS_user_profile_promote_button_ajax' );
add_action( 'personal_options', 'ELS_user_profile_promote_button_start' );

