(function () {
	var $ = jQuery;

	$("#btnPromotemyself").on("click", function(ev) {
		ev.preventDefault();
		$("#btnPromotemyself").attr("disabled", "disabled");
		$("#showspinner").css("visibility", "visible");
		$("#showspinner").show();
		$.ajax({
            url: MyAjax.ajaxurl,
            type: "POST",
            data: {
                action: 'promote_myself',
                nonce: MyAjax.nonce,
            },
            success: function (res) {
				$("#showspinner").hide();
				$("#btnPromotemyself").hide();
                $('#ajax-error-box').hide();
				$('#ajax-msg-box').html(res.msg);
				console.log(res);
			},
			error: function (jqXHR, textStatus, errorThrown) {
                console.log("ajax error:");
                console.log(jqXHR);
				$("#showspinner").hide();
				$("#btnPromotemyself").removeAttr("disabled");
                var msg = jqXHR.responseText;
                try {
                    var j = JSON.parse(jqXHR.responseText);
                    if (j.error) msg = j.error;
                }
                catch (ex) { }
				$('#ajax-error-box').html(msg);
                $('#ajax-error-box').show();
            }
        }); //*/ ajax
		
	}); // on click

	
	
}());