<?php
/*
Plugin Name: ELS Users Profile Statistics
Plugin URI: 
Description: Shows Users Statistics in the user-profile page
Version: 0.1.0
Author: Mauro Vannucci
Author URI: 
Copyright 2014  Mauro Vannucci (email : vannux@gmail.com)
Permission to add/modify/copy are subject to the author.
*/


class ELS_user_profile_stats
{
	public function __construct() {
		
		$action = ( IS_PROFILE_PAGE ? 'show' : 'edit' ) . '_user_profile';
		add_action( $action, array ( $this, 'show_user_profile' ), 9 );
		
		//add_action( 'show_user_profile', array( $this, 'show_user_profile' ) );
		//add_action( 'edit_user_profile', array( $this, 'show_user_profile' ) );
	}
	
	function show_user_profile($user) {
		
		global $wpdb;
		//echo "SELECT count(ads.pid) downloads FROM {$wpdb->prefix}posts p
		//	left join {$wpdb->prefix}ahm_download_stats ads on ads.pid = p.id
		//	where post_type='wpdmpro' group by p.id	order by p.id";

		// for all blogs
		$blogs = $wpdb->get_col("select blog_id FROM $wpdb->blogs", 0);
		$nr_downloads = 0;
		$nr_comments = 0;
		$nr_comments_reply = 0;
		foreach($blogs as $blogid) {
			$blog_prefix = $blogid == 1 ? $wpdb->base_prefix : $wpdb->base_prefix . $blogid;
			
			$nr_downloads += $wpdb->get_var("SELECT count(*) FROM {$blog_prefix}_posts p left join {$blog_prefix}_ahm_download_stats ads on ads.pid = p.id where p.post_type='wpdmpro'
							and ads.uid = {$user->id}");
			
			$nr_comments += $wpdb->get_var("SELECT count(*) FROM {$blog_prefix}_comments where user_id = {$user->id}");
			
			$nr_comments_reply += $wpdb->get_var("SELECT count(*) FROM {$blog_prefix}_comments where user_id = {$user->id} and comment_parent > 0");

		}
	?>
	<h2><?php _e('User Statistics', 'els_user_profile_stats'); ?></h2>
	
	<table class="form-table">
		<tr>
			<th>
				<label for="downloads"><?php _e('Downloads', 'els_user_profile_stats'); ?></label>
			</th>
			<td>
				<?php echo $nr_downloads; ?>
				<!--span class="description"><?php _e('downloads description', 'your_textdomain'); ?></span-->
			</td>
		</tr>
		<tr>
			<th>
				<label for="comments"><?php _e('Comments', 'els_user_profile_stats'); ?></label>
			</th>
			<td>
				<?php echo $nr_comments; ?>
			</td>
		</tr>
		<tr>
			<th>
				<label for="blogreplays"><?php _e('Blog Replays', 'els_user_profile_stats'); ?></label>
			</th>
			<td>
				<?php echo $nr_comments_reply; ?>
			</td>
		</tr>
	</table>
	<?php
	}
}
function ELS_user_profile_stats_start() {
	new ELS_user_profile_stats();
}

add_action( 'personal_options', 'ELS_user_profile_stats_start' );

