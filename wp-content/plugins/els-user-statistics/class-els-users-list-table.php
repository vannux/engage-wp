<?php 

require_once( dirname( __FILE__ ) . '/class-wp-list-table.php' ); 
require_once realpath(__DIR__.'/../../..').'/wp-includes/user.php';
 
class Els_Users_List_Table extends WP_List_Table_Els
{
	function __construct( ) {
		parent::__construct( array(
            'singular'  => 'movie',     //singular name of the listed records
            'plural'    => 'movies',    //plural name of the listed records
            'ajax'      => false        //does this table support ajax?
        ) );
	}
    
    
	
	function column_default($item, $column_name){
        switch($column_name){
            case 'ID':
            case 'name':
            case 'school':
            case 'downloads':
            case 'comments':
                return $item[$column_name];
            default:
                return print_r($item,true); //Show the whole array for troubleshooting purposes
        }
    }
	
	function column_title($item){
        
        //Build row actions
        //$actions = array(
        //    'edit'      => sprintf('<a href="?page=%s&action=%s&movie=%s">Edit</a>',$_REQUEST['page'],'edit',$item['ID']),
        //    'delete'    => sprintf('<a href="?page=%s&action=%s&movie=%s">Delete</a>',$_REQUEST['page'],'delete',$item['ID']),
        //);
        
        //Return the title contents
        //return sprintf('%1$s <span style="color:silver">(id:%2$s)</span>%3$s',
        //    /*$1%s*/ $item['title'],
        //    /*$2%s*/ $item['ID'],
        //    /*$3%s*/ $this->row_actions($actions)
        //);
        return sprintf('%1$s <span style="color:silver">(id:%2$s)</span>%3$s',
            $item['title'],
            $item['ID']
        );
    }
	
	function column_cb($item){
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            /*$1%s*/ $this->_args['singular'],  //Let's simply repurpose the table's singular label ("movie")
            /*$2%s*/ $item['ID']                //The value of the checkbox should be the record's id
        );
    }
    
	
	function get_columns(){
        $columns = array(
            'ID' => 'ID',
            'name' => 'Name',
            'school' => 'School',
            'downloads' => 'Nr. downloads',
            'comments' => 'Nr. reviews'
        );
        return $columns;
    }
	
	function get_sortable_columns() {
        $sortable_columns = array(
            'name' => array('name',false),     //true means it's already sorted
            'school' => array('school',false),
            'downloads' => array('downloads',false),
            'comments' => array('comments',false)
        );
        return $sortable_columns;
    }
	
	//function get_bulk_actions() {
    //    $actions = array(
    //        'delete'    => 'Delete'
    //    );
    //    return $actions;
    //}
	//
	//function process_bulk_action() {
    //    
    //    //Detect when a bulk action is being triggered...
    //    if( 'delete'===$this->current_action() ) {
    //        wp_die('Items deleted (or they would be if we had items to delete)!');
    //    }
    //    
    //}
	
	function extra_tablenav( $which ) {
		//$pp =  (!empty($_REQUEST['items_per_page'])) ? $_REQUEST['items_per_page'] : 50;
		//echo 'Items per page:
		//<select name="items_per_page" id="items_per_page" onchange="alert(\'items_per_page: \' + document.getElementById(\'items_per_page\').value); document.getElementsByTagName(\'form\')[0].submit();">
		//	<option '.($pp<=20?'selected':'').'>20</option>
		//	<option '.($pp>20 && $pp<100?'selected':'').'>50</option>
		//	<option '.($pp>=100?'selected':'').'>100</option>
		//</select>';
	}
	
	function prepare_items() {

        /**
         * First, lets decide how many records per page to show
         */
		//var_dump($_REQUEST);
		//echo ( 'items_per_page: ' . $_REQUEST['items_per_page']);
        $per_page = (!empty($_REQUEST['items_per_page'])) ? $_REQUEST['items_per_page'] : 20;
		/**
         * REQUIRED for pagination. Let's figure out what page the user is currently 
         * looking at. We'll need this later, so you should always include it in 
         * your own package classes.
         */
		 $current_page = $this->get_pagenum();
        
        /**
         * REQUIRED. Now we need to define our column headers. This includes a complete
         * array of columns to be displayed (slugs & titles), a list of columns
         * to keep hidden, and a list of columns that are sortable. Each of these
         * can be defined in another method (as we've done here) before being
         * used to build the value for our _column_headers property.
         */
        $columns = $this->get_columns();
        $hidden = array('ID');
        $sortable = $this->get_sortable_columns();
        
       /**
         * REQUIRED. Finally, we build an array to be used by the class for column 
         * headers. The $this->_column_headers property takes an array which contains
         * 3 other arrays. One for all columns, one for hidden columns, and one
         * for sortable columns.
         */
        $this->_column_headers = array($columns, $hidden, $sortable);
        
        
        /**
         * Optional. You can handle your bulk actions however you see fit. In this
         * case, we'll handle them within our package just to keep things clean.
         */
        //$this->process_bulk_action();
        
        
        $data = array();
		
		$orderby = (!empty($_REQUEST['orderby'])) ? $_REQUEST['orderby'] : 'name';
		if ($orderby == 'school' || $orderby == 'downloads' || $orderby == 'comments') $this->orderby = $orderby;

		$order = (!empty($_REQUEST['order'])) ? $_REQUEST['order'] : 'asc';
		add_action( 'pre_user_query', array( $this, 'extended_user_search' ) );
		// http://codex.wordpress.org/Class_Reference/wpdb
        $user_query = new WP_User_Query( array( 
			'fields' => array( 'id', 'display_name' ), 
			'orderby'=> $orderby, 
			'order' => $order,
			'offset' => ($current_page-1)*$per_page,
			'number' => $per_page,
			'count_total' => true
		));
		
		//var_dump($user_query);

		$users = $user_query->get_results();
		//var_dump($users);
		
		/**
         * REQUIRED for pagination. Let's check how many items are in our data array. 
         * In real-world use, this would be the total number of items in your database, 
         * without filtering. We'll need this later, so you should always include it 
         * in your own package classes.
         */
        $total_items = $user_query->get_total();
		
			
		foreach ( $users as $user ) {
			array_push($data, array(
				'ID'     => $user->id,
				'name'     => $user->display_name,
				'school'    => $user->school,
				'downloads'  => $user->downloads,
				'comments'  => $user->comments
			));
		}
		
		
		
		//esci_cimy_uef_data` 
		//user_id, field_id, value
		//
		//esci_cimy_uef_fields
		//id, name = SCHOOL/INSTITUTION, label = ...

        /**
         * REQUIRED. Now we can add our *sorted* data to the items property, where 
         * it can be used by the rest of the class.
         */
        $this->items = $data;
        
        
        /**
         * REQUIRED. We also have to register our pagination options & calculations.
         */
        $this->set_pagination_args( array(
            'total_items' => $total_items,                  //WE have to calculate the total number of items
            'per_page'    => $per_page,                     //WE have to determine how many items to show on a page
            'total_pages' => ceil($total_items/$per_page)   //WE have to calculate the total number of pages
        ) );
    }
	
	
	function extended_user_search( $user_query ){
		global $wpdb;
		//var_dump($wpdb);
		//var_dump($user_query);

		// field id:
		$field_school = $wpdb->get_var( "SELECT id FROM {$wpdb->base_prefix}cimy_uef_fields where name = 'SCHOOL/INSTITUTION'" );

		$user_query->query_fields .= ',cud.value as school,count(distinct ahm_ds.id) as downloads, count(distinct comm.comment_ID) as comments ';
		$user_query->query_from .= " left join {$wpdb->prefix}ahm_download_stats ahm_ds ON ahm_ds.uid = {$wpdb->base_prefix}users.ID".
                                   " left join {$wpdb->prefix}comments comm ON comm.user_id = {$wpdb->base_prefix}users.ID".
								   " left join {$wpdb->base_prefix}cimy_uef_data cud ON (cud.user_id = {$wpdb->base_prefix}users.ID AND cud.field_id = $field_school)";
		if (isset($this->orderby)) {
			$user_query->query_orderby = " GROUP BY {$wpdb->base_prefix}users.id ORDER BY {$this->orderby} {$user_query->query_vars['order']}";
		}
		else {
			$user_query->query_orderby = " GROUP BY {$wpdb->base_prefix}users.id " . $user_query->query_orderby;
		}

		//var_dump($user_query);

	}
		
}
