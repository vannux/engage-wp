<?php
// GET = the client wait for update
// POST = node add/delete/updated


defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

add_action( 'wp_ajax_nopriv_get_metrics', 'get_metrics' );
add_action( 'wp_ajax_get_metrics', 'get_metrics' );

function get_metrics() {
	global $wpdb;
	$nonce = $_POST['nonce'];
	if ( ! wp_verify_nonce( $nonce, 'els-users-stats-ajax') )
		die ( 'Busted!');
	
	$response = array();
	header( "Content-Type: application/json" );
	
	if ( !is_admin() ) {
		$response['error'] = 'You have not the rights to query data.';
		$protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
		header($protocol . ' 403 Forbidden', true, 403);
		echo json_encode($response);
		exit;
	}

	
	$blog_id = get_current_blog_id();
	$startdate = $_POST['startdate'];
	$enddate = $_POST['enddate'];
	$category = $_POST['postcategory'];

	// DEBUG
	//$response['blog_id'] = $blog_id;
	//$response['wpdb_prefix'] = $wpdb->prefix;
	//$response['startdate'] = $startdate;
	//$response['enddate'] = $enddate;

	if (!$startdate || !$enddate) {
		$response['error'] = 'Invalid startdate or endate.';
		$protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
		header($protocol . ' 403 Forbidden', true, 403);
		echo json_encode($response);
		exit;
	}
	
	if ($startdate > $enddate) {
		$response['error'] = 'Startdate > endate.';
		$protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
		header($protocol . ' 403 Forbidden', true, 403);
		echo json_encode($response);
		exit;
	}
	
	if (!$category) {
		$response['error'] = 'You must specify postcategory.';
		$protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
		header($protocol . ' 403 Forbidden', true, 403);
		echo json_encode($response);
		exit;
	}
	
	$response['tot_dwl'] = $wpdb->get_var( "SELECT count(*) FROM {$wpdb->prefix}ahm_download_stats where timestamp BETWEEN $startdate AND $enddate" );
	
	$response['tot_usr'] = $wpdb->get_var( 
		"SELECT count(*) FROM {$wpdb->base_prefix}users u left join {$wpdb->base_prefix}usermeta um ON um.user_id = u.ID
	 	 where UNIX_TIMESTAMP(u.user_registered) BETWEEN $startdate AND $enddate AND um.meta_key='{$wpdb->prefix}capabilities'" );
	
	$response['tot_reviews'] = $wpdb->get_var("SELECT count(*) FROM {$wpdb->prefix}comments where UNIX_TIMESTAMP(comment_date) BETWEEN $startdate AND $enddate");

	$response['mat_new'] = $wpdb->get_var("SELECT count(*) FROM {$wpdb->prefix}posts p ".
		" join {$wpdb->prefix}term_relationships tr on tr.object_id = p.id" .
		" where p.post_type='post' and p.post_status = 'publish'" .
		" AND UNIX_TIMESTAMP(p.post_date) BETWEEN $startdate AND $enddate " .
		" AND tr.term_taxonomy_id=$category");
	
	// mat_updated

				
	echo json_encode($response);
	exit;
}
