<?php
/*
Plugin Name: ELS Users Statistics
Plugin URI: 
Description: Users Statistics
Version: 0.01.0
Author: Mauro Vannucci
Author URI: 
Copyright 2014  Mauro Vannucci (email : vannux@gmail.com)
Permission to add/modify/copy are subject to the author.
*/


//require_once( dirname( __FILE__ ) . '/class-els-users-list-table.php' ); 

function print_metrics() {
	global $wpdb;
	
	$blog_id = get_current_blog_id();
	
	wp_enqueue_style( 'els_users_stats_css', plugins_url( '/css/els_users_stats.css', __FILE__ ) );
	wp_enqueue_script( 'jquery', plugins_url( '/jquery-2.0.2.min.js', __FILE__ ), null);
	wp_enqueue_style( 'jquery-ui', plugins_url( '/jq/jquery-ui.min.css', __FILE__ ) );
	wp_enqueue_style( 'jquery-ui-s', plugins_url( '/jq/jquery-ui.structure.min.css', __FILE__ ) );
	wp_enqueue_style( 'jquery-ui-t', plugins_url( '/jq/jquery-ui.theme.min.css', __FILE__ ) );
	wp_enqueue_script( 'jquery-ui', plugins_url( '/jq/jquery-ui.min.js', __FILE__ ), array('jquery'));
	wp_enqueue_script( 'els_users_stats_js', plugins_url( '/mod.js', __FILE__ ), array('jquery', 'jquery-ui'), null, true );

	wp_localize_script( 'els_users_stats_js', 'MyAjax', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		'nonce'   => wp_create_nonce( 'els-users-stats-ajax' ),
		)
	);

	//$result = count_users();
	// materials downloaded (en): esci_6_ahm_download_stats
	//$tot_dwl = $wpdb->get_var( "SELECT count(*) FROM {$wpdb->prefix}ahm_download_stats" );
	
	echo '<div class="wrap"><h2>'. __('Users Statistics', 'engage') . '</h2>';
	
	?>
	
	<br/><h3><?php _e( 'Cumulative', 'engage')?></h3>
	<label for="startdate"><?php _e( 'Start date:', 'engage')?></label><input type="text" data-type="date" id="startdate" name="startdate" value="" />
	&nbsp;<label for="enddate"><?php _e( 'End date:', 'engage')?></label><input type="text" data-type="date" id="enddate" name="enddate" value="" />
	&nbsp;<label for="postcategory"><?php _e( 'Post category:', 'engage')?></label>
	<select id='postcategory'>
	<?php 
		$categories = get_categories();
		foreach($categories as $cat) {
			echo "<option value='{$cat->term_taxonomy_id}'>{$cat->name}</option>";
		}
	?>
	</select>
	&nbsp;<span id="showspinner" class="spinner"></span><button class="button button-primary" name="show" id="btnShow"><?php _e( 'Show', 'engage')?></button>
	<br/>
	<br/>
	<div id="ajax-error-box" class="error hidden"></div>
	<table class="es-table wp-list-table widefat">
	<thead>
		<tr>
		<th scope="col" class="metric-col">Metric</th>
		<th scope="col" class="value-col">Value</th>
		<th scope="col" class="empty-col">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<tr class="alternate">
			<td class="metric"><?php _e( 'Teachers signed up', 'engage') ?></td>
			<td class="value-col"><span id="tot_usr"></span></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="metric"><?php _e( 'Materials downloaded', 'engage') ?></td>
			<td class="value-col"><span id="tot_dwl"></span></td>
			<td></td>
		</tr>
		<tr class="alternate">
			<td class="metric"><?php _e( 'Teachers reviews', 'engage') ?></td>
			<td class="value-col"><span id="tot_reviews"></td>
			<td></td>
		</tr>
		<tr>
			<td class="metric"><?php _e( 'New posts of type', 'engage') ?></td>
			<td class="value-col"><span id="mat_new"></td>
			<td></td>
		</tr>
		<!--tr class="alternate">
			<td class="metric">< ? php _e( 'Materials updated', 'engage') ? ></td>
			<td class="value-col"><span id="mat_updated"></td>
			<td></td>
		</tr-->
	</tbody>
	</table>
	<br/>
	<h3><?php _e( 'Users') ?></h3>
    <?php 
}

function print_users_stats() {
	
	require_once( dirname( __FILE__ ) . '/class-els-users-list-table.php' ); 

		
	$testListTable = new Els_Users_List_Table();
    //Fetch, prepare, sort, and filter our data...
    $testListTable->prepare_items();
	 ?>

	 <form id="movies-filter" method="get">
            <!-- For plugins, we also need to ensure that the form posts back to our current page -->
            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
            <!-- Now we can render the completed list table -->
            <?php $testListTable->display() ?>
    </form>

    <?php
}

function els_us_admin_column_width() {
    $page = ( isset($_GET['page'] ) ) ? esc_attr( $_GET['page'] ) : false;
    //var_dump($page);
    if( 'ElsUsersStatistics' != $page ) return; 

    echo '<style type="text/css">'.
        '.wp-list-table .column-name { width: 30%; }'.
        '.wp-list-table .column-school { width: 50%; }'.
        '.wp-list-table .column-downloads { width: 12%; text-align: center;}'.
        '.wp-list-table .column-comments { width: 12%; text-align: center;}'.
        '</style>';
}

function els_users_stats() {
	print_metrics();
	print_users_stats();
}

function init_els_users_stats() {
	//                $parent_slug, $page_title,            						$menu_title,                                       $capability,        $menu_slug,          $function
	add_submenu_page( 'users.php', __('Users statistics', 'els-users-statistics'), __('ELS Users statistics', 'els-users-statistics'), 'list_users', 'ElsUsersStatistics', 'els_users_stats' );
}

if ( is_admin() ){ // admin actions
	require_once( dirname( __FILE__ ) . '/els-ajax.php' );

    add_action('admin_head', 'els_us_admin_column_width');
	add_action('admin_menu', 'init_els_users_stats' );
	//add_action('admin_enqueue_scripts', 'els_users_stats_scripts');
}

