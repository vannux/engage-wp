(function () {
	var $ = jQuery;
	//var isDateInputSupported = function(){
	//	var elem = document.createElement('input');
	//	elem.setAttribute('type','date');
	//	elem.value = 'foo';
	//	return (elem.type == 'date' && elem.value != 'foo');
	//}
    //
	//if (!isDateInputSupported()) {
	//	//console.log("Unsupported");
	//	$('input[type=date]').datepicker({
	//		dateFormat: 'dd-mm-yy'
	//	});
	//}
	$('input[data-type=date]').datepicker({
		dateFormat: 'dd-mm-yy'
	});
	
	
	$("#btnShow").on("click", function() {
		var startdate,enddate, timezone;
		//if (isDateInputSupported()) {
		//	startdate = $("#startdate")[0].valueAsNumber;
		//	enddate = $("#enddate")[0].valueAsNumber;
		//	timezone = 0;
		//}
		//else {
			var d = $("#startdate").datepicker("getDate");
			startdate = d.getTime();
			timezone  = d.getTimezoneOffset();
			enddate   = $("#enddate").datepicker("getDate").getTime();
		//}
		
		if (isNaN(startdate)) {
			$("#startdate").addClass("mandatory");
			return;
		}
		
		$("#startdate").removeClass("mandatory");
		if (isNaN(enddate)) {
			$("#enddate").addClass("mandatory");
			return;
		}
		
		if (startdate > enddate) {
			$("#startdate").addClass("mandatory");
			$("#enddate").addClass("mandatory");
			return;
		}
		
		startdate = Math.floor(startdate/1000);
		enddate   = Math.floor(enddate/1000)
		startdate -= timezone * 60;
		enddate   -= timezone * 60;
		
		//console.log("postcategory  " + $("#postcategory").val());
		//console.log("startdate  " + startdate);
		//console.log("enddate  " + enddate);
		
		$("#enddate").removeClass("mandatory");
		$("#showspinner").show();
		$("#btnShow").attr("disabled", "disabled");
		
		$.ajax({
            url: MyAjax.ajaxurl,
            type: "POST",
            data: {
                action: 'get_metrics',
                nonce: MyAjax.nonce,
				startdate : startdate,
				enddate: enddate,
				postcategory: $("#postcategory").val()
            },
            success: function (res) {
				$("#showspinner").hide();
				$("#btnShow").removeAttr("disabled");
                $('#ajax-error-box').hide();
				//console.log("Success!");
				//console.log(res);
				$("#tot_usr").html(res.tot_usr);
				$("#tot_dwl").html(res.tot_dwl);
				$("#tot_reviews").html(res.tot_reviews);
				$("#mat_new").html(res.mat_new);
				//$("#mat_updated").html(res.mat_updated);
			},
			error: function (jqXHR, textStatus, errorThrown) {
                console.log("ajax error:");
                console.log(jqXHR);
				$("#showspinner").hide();
				$("#btnShow").removeAttr("disabled");
                var msg = jqXHR.responseText;
                try {
                    var j = JSON.parse(jqXHR.responseText);
                    if (j.error) msg = j.error;
                }
                catch (ex) { }
				$('#ajax-error-box').html(msg);
                $('#ajax-error-box').show();
            }
        }); //*/ ajax
		
	}); // on click

	
	
}());