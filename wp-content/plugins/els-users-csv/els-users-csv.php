<?php
/*
Plugin Name: ELS export users to CSV
Description: Export Users data and metadata to a csv file.
Version: 1.0.0
Author: Mauro Vannucci
Author URI: 
Copyright 2014  Mauro Vannucci
Permission to add/modify/copy are subject to the author.
*/

class ELS_Users_CSV {

	public function __construct() {
		add_action( 'admin_menu', array( $this, 'add_admin_pages' ) );
		add_action( 'init', array( $this, 'init' ) );
	}

	public function add_admin_pages() {
		add_users_page( 'ELS export to CSV', 'ELS export to CSV', 'list_users', 'els-users-csv', array( $this, 'render_html' ) );
	}

	public function init() {
		if ( isset( $_POST['_wpnonce-els-users-csv'] ) ) {
			check_admin_referer( 'els-users-csv-export', '_wpnonce-els-users-csv' );
			$action = $_POST['_wp_http_action'];
			if ( isset( $action ) ) {
				$this->consume_action($action);
			}
		}
	}
	
	private function consume_action($action) {
		if ( $action == 'users_to_csv' ) {
			$this->generate_users_csv();
		}
		else if ( $action == 'downloads_csv' ) {
			$this->generate_downloads_csv();
		}
		else if ( $action == 'list_all_posts' ) {
			$this->generate_list_all_posts();
		}
		else if ( $action == 'post_materials' ) {
			$this->generate_post_materials();
		}
		else if ( $action == 'post_comments' ) {
			$this->generate_post_comments();
		}
    else if ( $action == 'x_per_user' ) {
			$this->generate_x_per_user();
    }
	}

	/**
	 * All users with cimy info
	 *
	*/
	public function generate_users_csv() {
		
		if (!isset($_POST['site'])) {
			$this->error = 'Site not specified';
			return;
		}
		
		if (!($blogdetails = get_blog_details($_POST['site']))) {
			$this->error = 'Invalid blog id: ' . $_POST['site'];
			return;
		}
		
		/*
		SELECT 
		u.ID, u.user_login, u.user_nicename, u.user_email, u.user_registered, u.display_name, 
		um1.meta_value as first_name, um2.meta_value as last_name,
		c0.value as 'SCHOOL/INSTITUTION',c1.value as 'CITY',c2.value as 'POSTCODE',c3.value as 'JOB',c4.value as 'COUNTRY' ,
		IFNULL(ahm_ds.tot, 0) as downloads, 
		IFNULL(comm.tot,0) as comments
		FROM esci_users u left join esci_cimy_uef_data c0 on c0.FIELD_ID = 1 and c0.USER_ID = u.ID 
		left join esci_cimy_uef_data c1 on c1.FIELD_ID = 2 and c1.USER_ID = u.ID 
		left join esci_cimy_uef_data c2 on c2.FIELD_ID = 3 and c2.USER_ID = u.ID 
		left join esci_cimy_uef_data c3 on c3.FIELD_ID = 4 and c3.USER_ID = u.ID 
		left join esci_cimy_uef_data c4 on c4.FIELD_ID = 5 and c4.USER_ID = u.ID 
		left join 
		(
		select uid, count(id) as tot from esci_6_ahm_download_stats group by uid
		) as ahm_ds ON ahm_ds.uid = u.ID 
		left join 
		(
		select user_id, count(comment_ID) as tot from esci_6_comments group by user_id
		) as comm ON comm.user_id = u.ID 
		left join esci_usermeta um ON um.user_id = u.ID 
		left join esci_usermeta um1 ON um1.user_id = u.ID and um1.meta_key='first_name' 
		left join esci_usermeta um2 ON um2.user_id = u.ID and um2.meta_key='last_name'  
		WHERE um.meta_key='esci_6_capabilities'
		GROUP BY u.ID ORDER BY u.ID ASC
		
		
		SELECT u.ID, u.user_login, u.user_nicename, u.user_email, u.user_registered, u.display_name,
		max(case when c_name = 'city' then c_val end) city,
		max(case when c_name = 'POSTCODE' then c_val end) POSTCODE,
		max(case when c_name = 'JOB' then c_val end) JOB,
		max(case when c_name = 'COUNTRY' then c_val end) COUNTRY,
		max(case when c_name = 'SCHOOL/INSTITUTION' then c_val end) 'SCHOOL/INSTITUTION',
		IFNULL(ahm_ds.tot, 0) as downloads, 
		IFNULL(comm.tot,0) as comments
		from esci_users u 
		left join
		(
			select c.USER_ID as user_id, f.NAME as c_name, c.value as c_val
			from esci_cimy_uef_data c
			join esci_cimy_uef_fields f on f.ID = c.FIELD_ID
		) as cimy on cimy.user_id = u.id
		left join esci_usermeta um ON um.user_id = u.ID 
		left join 
		(
		select uid, count(id) as tot from esci_6_ahm_download_stats group by uid
		) as ahm_ds ON ahm_ds.uid = u.ID 
		left join 
		(
		select user_id, count(comment_ID) as tot from esci_6_comments group by user_id
		) as comm ON comm.user_id = u.ID 
		WHERE um.meta_key='esci_6_capabilities'
		group by u.id
		ORDER BY u.id ASC

		*/
		
		global $wpdb;
		$prefix = $wpdb->base_prefix . $_POST['site'] . '_';
		//id, name, label, 
		$cimy_fields = $wpdb->get_results( "SELECT * FROM {$wpdb->base_prefix}cimy_uef_fields");

		$join = '';
		$query_fields = 'u.ID id, u.user_nicename username, u.user_email, u.user_registered, u.display_name, 
		um1.meta_value as first_name, um2.meta_value as last_name';
		$cimy_fields_length = count($cimy_fields);
		$cimy_names = array();
		for($i=0 ; $i < $cimy_fields_length ; $i++) {
			$query_fields .= ",c$i.value as '".strtolower($cimy_fields[$i]->NAME)."'";
			$join .= " left join {$wpdb->base_prefix}cimy_uef_data c$i on c$i.FIELD_ID = {$cimy_fields[$i]->ID} and c$i.USER_ID = u.ID";
		}
		
		$query_fields .= ' ,IFNULL(ahm_ds.tot, 0) as downloads, IFNULL(comm.tot,0) as comments ';
		
		$query_from =  	" FROM {$wpdb->base_prefix}users u" . $join .
						" left join (select uid, count(id) as tot from {$prefix}ahm_download_stats group by uid) as ahm_ds ON ahm_ds.uid = u.ID ".
						" left join (select user_id, count(comment_ID) as tot from {$prefix}comments group by user_id) as comm ON comm.user_id = u.ID " .
						" left join {$wpdb->base_prefix}usermeta um ON um.user_id = u.ID" .
						" left join {$wpdb->base_prefix}usermeta um1 ON um1.user_id = u.ID and um1.meta_key='first_name'" .
						" left join {$wpdb->base_prefix}usermeta um2 ON um2.user_id = u.ID and um2.meta_key='last_name'";

		$query_orderby = " GROUP BY u.ID ORDER BY u.ID ASC";
		
		$where = " WHERE um.meta_key='{$prefix}capabilities'";
		
		$els_user_role = $_POST['els_user_role'];
		//echo '<h1>User role selected: '.$els_user_role.'</h1>';
		//$where .= " AND um.meta_value LIKE '%adapter%'";
		$where .= " AND um.meta_value LIKE '%$els_user_role%'";
	
		//echo "<pre>SELECT $query_fields $query_from $where $query_orderby</pre>";
		//exit;
		$users = $wpdb->get_results("SELECT $query_fields $query_from $where $query_orderby");
		//echo "<p>$query_fields</p>";
		//echo "<p>$query_from</p>";
		//echo "<p>$where</p>";
		//echo "<p>$query_orderby</p>";
		
				//query for adapters only
				/*93606
				1003271
				esci_6_capabilities
				a:2:{s:10:"subscriber";b:1;s:7:"adapter";b:1;
				vendoc SELECT * FROM `esci_usermeta` WHERE user_id = '1003271'
				SELECT * FROM `esci_usermeta` WHERE `meta_value` LIKE '%adapt%'
				*/
		

		//result from UK
		//u.ID id, u.user_login, u.user_nicename, u.user_email, u.user_registered, u.display_name, um1.meta_value as first_name, um2.meta_value as last_name, c0.value as 'school/institution',c1.value as 'city',c2.value as 'postcode',c3.value as 'job',c4.value as 'country',c5.value as 'about-my-self',c6.value as 'photo',c7.value as 'educational-interest',c8.value as 'publish-permission' ,IFNULL(ahm_ds.tot, 0) as downloads, IFNULL(comm.tot,0) as comments

		//FROM esci_users u left join esci_cimy_uef_data c0 on c0.FIELD_ID = 1 and c0.USER_ID = u.ID left join esci_cimy_uef_data c1 on c1.FIELD_ID = 2 and c1.USER_ID = u.ID left join esci_cimy_uef_data c2 on c2.FIELD_ID = 3 and c2.USER_ID = u.ID left join esci_cimy_uef_data c3 on c3.FIELD_ID = 4 and c3.USER_ID = u.ID left join esci_cimy_uef_data c4 on c4.FIELD_ID = 5 and c4.USER_ID = u.ID left join esci_cimy_uef_data c5 on c5.FIELD_ID = 6 and c5.USER_ID = u.ID left join esci_cimy_uef_data c6 on c6.FIELD_ID = 8 and c6.USER_ID = u.ID left join esci_cimy_uef_data c7 on c7.FIELD_ID = 9 and c7.USER_ID = u.ID left join esci_cimy_uef_data c8 on c8.FIELD_ID = 10 and c8.USER_ID = u.ID left join (select uid, count(id) as tot from esci_6_ahm_download_stats group by uid) as ahm_ds ON ahm_ds.uid = u.ID left join (select user_id, count(comment_ID) as tot from esci_6_comments group by user_id) as comm ON comm.user_id = u.ID left join esci_usermeta um ON um.user_id = u.ID left join esci_usermeta um1 ON um1.user_id = u.ID and um1.meta_key='first_name' left join esci_usermeta um2 ON um2.user_id = u.ID and um2.meta_key='last_name'

		//WHERE um.meta_key='esci_6_capabilities'

		//GROUP BY u.ID ORDER BY u.ID ASC
		
		//SELECT u.ID id, u.user_login, u.user_nicename, u.user_email, u.user_registered, u.display_name, um1.meta_value as first_name, um2.meta_value as last_name,c0.value as 'school/institution', c1.value as 'city',c2.value as 'postcode',c3.value as 'job',c4.value as 'country',c5.value as 'about-my-self',c6.value as 'photo',c7.value as 'educational-interest',c8.value as 'publish-permission', IFNULL(ahm_ds.tot, 0) as downloads, IFNULL(comm.tot,0) as comments FROM esci_users u left join esci_cimy_uef_data c0 on c0.FIELD_ID = 1 and c0.USER_ID = u.ID left join esci_cimy_uef_data c1 on c1.FIELD_ID = 2 and c1.USER_ID = u.ID left join esci_cimy_uef_data c2 on c2.FIELD_ID = 3 and c2.USER_ID = u.ID left join esci_cimy_uef_data c3 on c3.FIELD_ID = 4 and c3.USER_ID = u.ID left join esci_cimy_uef_data c4 on c4.FIELD_ID = 5 and c4.USER_ID = u.ID left join esci_cimy_uef_data c5 on c5.FIELD_ID = 6 and c5.USER_ID = u.ID left join esci_cimy_uef_data c6 on c6.FIELD_ID = 8 and c6.USER_ID = u.ID left join esci_cimy_uef_data c7 on c7.FIELD_ID = 9 and c7.USER_ID = u.ID left join esci_cimy_uef_data c8 on c8.FIELD_ID = 10 and c8.USER_ID = u.ID left join (select uid, count(id) as tot from esci_6_ahm_download_stats group by uid) as ahm_ds ON ahm_ds.uid = u.ID left join (select user_id, count(comment_ID) as tot from esci_6_comments group by user_id) as comm ON comm.user_id = u.ID left join esci_usermeta um ON um.user_id = u.ID left join esci_usermeta um1 ON um1.user_id = u.ID and um1.meta_key='first_name' left join esci_usermeta um2 ON um2.user_id = u.ID and um2.meta_key='last_name' WHERE um.meta_key='esci_6_capabilities' GROUP BY u.ID ORDER BY u.ID ASC
		
		//from UK Only adapt
		//SELECT u.ID id, u.user_login, u.user_nicename, u.user_email, u.user_registered, u.display_name, um1.meta_value as first_name, um2.meta_value as last_name,c0.value as 'school/institution', c1.value as 'city',c2.value as 'postcode',c3.value as 'job',c4.value as 'country',c5.value as 'about-my-self',c6.value as 'photo',c7.value as 'educational-interest',c8.value as 'publish-permission', IFNULL(ahm_ds.tot, 0) as downloads, IFNULL(comm.tot,0) as comments FROM esci_users u left join esci_cimy_uef_data c0 on c0.FIELD_ID = 1 and c0.USER_ID = u.ID left join esci_cimy_uef_data c1 on c1.FIELD_ID = 2 and c1.USER_ID = u.ID left join esci_cimy_uef_data c2 on c2.FIELD_ID = 3 and c2.USER_ID = u.ID left join esci_cimy_uef_data c3 on c3.FIELD_ID = 4 and c3.USER_ID = u.ID left join esci_cimy_uef_data c4 on c4.FIELD_ID = 5 and c4.USER_ID = u.ID left join esci_cimy_uef_data c5 on c5.FIELD_ID = 6 and c5.USER_ID = u.ID left join esci_cimy_uef_data c6 on c6.FIELD_ID = 8 and c6.USER_ID = u.ID left join esci_cimy_uef_data c7 on c7.FIELD_ID = 9 and c7.USER_ID = u.ID left join esci_cimy_uef_data c8 on c8.FIELD_ID = 10 and c8.USER_ID = u.ID left join (select uid, count(id) as tot from esci_6_ahm_download_stats group by uid) as ahm_ds ON ahm_ds.uid = u.ID left join (select user_id, count(comment_ID) as tot from esci_6_comments group by user_id) as comm ON comm.user_id = u.ID left join esci_usermeta um ON um.user_id = u.ID left join esci_usermeta um1 ON um1.user_id = u.ID and um1.meta_key='first_name' left join esci_usermeta um2 ON um2.user_id = u.ID and um2.meta_key='last_name' WHERE um.meta_key='esci_6_capabilities' AND um.meta_value LIKE '%adapt%' GROUP BY u.ID ORDER BY u.ID ASC
		
		if ( !$users ) {
			$this->error = "No users found.";
			return;
		}

		//var_dump($users[0]);
		$sitename = sanitize_key( get_bloginfo( 'name' ) ) .'.'.substr($blogdetails->path, 1, -1).'.';
		$filename = $sitename . 'users.' . date( 'Y-m-d-H-i-s' ) . '.csv';
		$this->export_csv($filename, $users); //REMOVE COMMENT!
		exit;
	}
	
	private function parseDate($post_date) {
		$d = date_parse($post_date);
		if (count($d['errors']) > 0) {
			$this->error = 'Invalid StartDate: ' . $d['errors'][0];
			return null;
		}
		$mydate = mktime(
			$d['hour'], 
			$d['minute'], 
			$d['second'], 
			$d['month'], 
			$d['day'], 
			$d['year']
		);
		return $mydate;
	}
	
	/**
	 * For each "File" (material) returns
	 * Id, Material title, NrDownloads
	*/
	public function generate_downloads_csv() {
		$startdate = $this->parseDate($_POST['startdate']);
		if ($startdate == null) return;
		
		$enddate = $this->parseDate($_POST['enddate']);
		if ($enddate == null) return;

		if ($startdate >= $enddate) {
			$this->error = 'Error StartDate >= EndDate';
			return;
		}
		
		global $wpdb;
		
		$items = $wpdb->get_results("SELECT p.id 'Id', p.post_title 'Material', p.post_modified 'Modified', count(ads.pid) 'Downloads' FROM {$wpdb->prefix}posts p
			left join {$wpdb->prefix}ahm_download_stats ads on ads.pid = p.id and ads.timestamp BETWEEN $startdate AND $enddate
			where post_type='wpdmpro' group by p.id	order by p.id");

		if ( !$items ) {
			$this->error = "No data found.<br/>$q";
			return;
		}
	    
		$sitename = sanitize_key( get_bloginfo( 'name' ) ) .'.'.substr($blogdetails->path, 1, -1).'.';
		//$filename = $sitename . 'downloads.' . date( 'Y-m-d-H-i-s' ) . '.csv';
		// .downloads.<start>_<end>
		$filename = $sitename . 'downloads.' . date( 'Y-m-d', $startdate ) . '_' . date( 'Y-m-d', $enddate ) . '.csv';
	    
		$this->export_csv($filename, $items);
		exit;
	}

	/**
	 * posts_list: lists all posts
	 * - for each post: 
	 * - id, 
	 * - title, 
	 * - type, 
	 * - total number of downloads = sum of number of downloads for each file into the post
	 * - 3 average ratings
	 * - number of likes to the post
	 */
	public function generate_list_all_posts() {
		
		//$startdate = $this->parseDate($_POST['startdate']);
		//if ($startdate == null) return;
		//
		//$enddate = $this->parseDate($_POST['enddate']);
		//if ($enddate == null) return;
    //
		//if ($startdate >= $enddate) {
		//	$this->error = 'Error StartDate >= EndDate';
		//	return;
		//}
		
		global $wpdb;
		
		$csvItems = array();		
				
		$args=array(
			'category_name ' => 'resources',
			'post_type' => 'post',
			'posts_per_page' => -1,
			'post_status' => 'publish',
			'caller_get_posts'=> 1
		);
		$query = new WP_Query($args);
		
		while ( $query->have_posts() ) {
			$query->next_post();
			$categories = get_the_category($query->post->ID);
			$type = 'Adopt';
			$isResource = false;
			foreach($categories as $cat) {
				if ($cat->slug == 'adapt') {
					$type = 'Adapt';
				}
				else if ($cat->slug == 'transform') {
					$type = 'Transform';
				}
				else if ($cat->slug == 'resources') {
					$isResource = true;
				}
			}
			if (!$isResource) continue;
			
			$row = array(
				'Id' => $query->post->ID, 
				'Title' => $query->post->post_title,
				'Type' => $type,
				'Downloads' => get_nr_downloads_post($query->post)
			);
			
			$table_name = $wpdb->prefix . 'els_useful_for';
			$all_tags = $wpdb->get_results("select tag, count(id) tot, avg(used) average_rating from $table_name where post_id={$query->post->ID} or post_id=-1 group by tag order by tag");
			foreach($all_tags as $t) {
				$row[$t->tag] = $t->average_rating;
			}
			
			$row['likes'] = get_post_meta($query->post->ID, '_liked', true);
			$csvItems[] = $row;
		}
		wp_reset_query();
		wp_reset_postdata();


		if ( !$csvItems ) {
			$this->error = "No data found.<br/>$q";
			return;
		}
	    
		$sitename = sanitize_key( get_bloginfo( 'name' ) ) . '.';
		$filename = $sitename . 'list_all_posts.csv';
		//$filename = $sitename . 'list_all_posts.' . date( 'Y-m-d', $startdate ) . '_' . date( 'Y-m-d', $enddate ) . '.csv';
	    
		$this->export_csv($filename, $csvItems);
		exit;
	}
	
	/**
	 * lists files in post and related downloads
 	 */
	public function generate_post_materials() {
		//$post_id = $_POST['post_id'];
		
		$csvItems = array();
			
		$args=array(
				'category_name ' => 'resources',
				'posts_per_page' => -1,
				'post_status' => 'publish',
				'caller_get_posts'=> 1,
				'post_type' => 'post',
		);
		$query = new WP_Query($args);
		while ( $query->have_posts() ) {
			$query->next_post();
			$categories = get_the_category($query->post->ID);
			$isResource = false;
			foreach($categories as $cat) {
				if ($cat->slug == 'resources') {
					$isResource = true;
					break;
				}
			}
			if (!$isResource) continue;
		
			$post = $query->post;
				
			$res = array();
			
			$m = array();
			$i = preg_match_all('|\[wpdm_els_adapt id\s*=\s*["\']?(\d+)["\']?[^]]*\]|u', $post->post_content, $m, PREG_OFFSET_CAPTURE);
			if (count($m) > 1 )  $res = $m[1];
			$i = preg_match_all('|\[wpdm_package\s+id\s*=\s*["\']?(\d+)["\']?[^]]*\]|u', $post->post_content, $m, PREG_OFFSET_CAPTURE);
			if (count($m) > 1 ) $res = array_merge ($res, $m[1]);
			
			if (count($res) > 1 ) {
				foreach($res as $wpdm) {
					$wpdmid = $wpdm[0];
					$pack = wpdm_get_package($wpdmid);

          $r = get_post_meta($wpdmid, '__wpdm_download_count', true);
					if(!$r) $r=0;
					$csvItem = array();
					$csvItem['Post id'] = $post->ID;
					$csvItem['Post title'] = $post->post_title;
					$csvItem['Material id'] = $pack['ID'];
					$csvItem['Material name'] = $pack['post_title'];
					$csvItem['Downloads'] = $r?$r:0;
					$csvItems[] = $csvItem;
				}
			}
	
		} // all posts
		
		if ( !$csvItems ) {
			$this->error = "No data found.<br/>$q";
			return;
		}
	    
		$sitename = sanitize_key( get_bloginfo( 'name' ) ) . '.';
		$filename = $sitename . "post_materials.csv";
	    
		$this->export_csv($filename, $csvItems);
		exit;
	}
	
	/**
	 * sheet with list of comments: 
	 * comment id, comment title, comment text, number of likers to the comment; if the comment is in reply to another one, id of the parent comment
	 */
	private function generate_post_comments() {
		$post_id = $_POST['post_id'];
		$csvItems = array();
		
		$sitename = sanitize_key( get_bloginfo( 'name' ) ) . '.';
		
		if ($post_id != null) {
			if (isset($_POST['post_ratings'])) {
				$csvItems = $this->csv_generate_post_ratings($post_id);
				$filename = $sitename . "post_{$post_id}_ratings.csv";
			}
			else if(isset($_POST['post_likers'])) {
				$csvItems = $this->csv_generate_post_likers($post_id);
				$filename = $sitename . "post_{$post_id}_likers.csv";
			}
			else if (isset($_POST['posts_users_downloads'])) {
				$csvItems = $this->generate_post_users_downloads($post_id);
				$filename = $sitename . "post_{$post_id}_users_downloads.csv";
			}
			else {
				$comments = get_comments(array('post_id' =>$post_id, 'orderby'=>'comment_ID', 'order'=>'ASC'));			
				if (isset($_POST['post_comments'])) {
					$filename = $sitename . "post_{$post_id}_comments.csv";
					foreach($comments as $c) {
						$likes = get_comment_meta($c->comment_ID, '_commentliked', true);
						$item=array(
							'Comment id' => $c->comment_ID,
							'Comment author' => $c->comment_author,
							'Comment date' => $c->comment_date,
							'Comment title' => get_comment_meta( $c->comment_ID, 'review_title', true ),
							'Comment parent' => $c->comment_parent,
							'Likes' => $likes?$likes:0,
							'Comment text' => $c->comment_content,
						);
						$csvItems[] = $item;
					}
				}
				else if (isset($_POST['comment_likers'])) {
					$filename = $sitename . "post_{$post_id}_comments_likers.csv";
					global $wpdb;
					foreach($comments as $c) {
						//  list of people who liked the comment, for all the comments related to the post
			
						$csvItems = array_merge($csvItems, $wpdb->get_results("SELECT c.comment_ID 'Comment ID', u.ID 'User ID', u.user_nicename 'Name' FROM {$wpdb->prefix}ulike_comments c
							left join {$wpdb->users} u on u.ID=user_id
							where comment_id={$c->comment_ID} order by comment_id", ARRAY_A ));				
					}
				}
			}
		}
		
		if ( !$csvItems ) {
			$this->error = "No data found.";
			return;
		}
	    
		$this->export_csv($filename, $csvItems);
		exit;
	}
	
	private function csv_generate_post_ratings($post_id) {
		global $wpdb;
		$csvItems = array();
		$table_name = $wpdb->prefix . 'els_useful_for';

		$csvItems[0] = array(
			'Post id' => $post_id,
			'User id' => -1,
			'User Name' => '',
		);
		$tags = $wpdb->get_results( "select r.* from $table_name r where r.post_id=-1 order by tag");
		foreach($tags as $row) {
			$csvItems[0][$row->tag] = 0;
		}
		
		$res = $wpdb->get_results( "select r.*, u.user_nicename from $table_name r left join {$wpdb->users} u on u.ID = r.user_id where r.post_id=$post_id ");
		foreach($res as $row) {
			if (!isset($csvItems[$row->user_id])) {
				$csvItems[$row->user_id] = array(
					'Post id' => $post_id,
					'User id' => $row->user_id,
					'User Name' => $row->user_nicename,
				);
			}
			$csvItems[$row->user_id][$row->tag] = $row->used;
		}
		return $csvItems;
	}
	
	private function csv_generate_post_likers($post_id) {
		global $wpdb;
		$csvItems = array();
		$res = $wpdb->get_results( "select r.*, u.ID, u.user_nicename from {$wpdb->prefix}ulike r left join {$wpdb->users} u on u.ID = r.user_id where r.post_id=$post_id ");
		foreach($res as $r) {
			$csvItems[] = array (
			'User id' => $r->user_id,
			'User name' => $r->user_nicename,
			'Date' => $r->date_time,
			'Status' => $r->status
			);
		}
		return $csvItems;
	}
	
	private function generate_post_users_downloads($post_id) {
		$csvItems = array();
			
		$post = get_post($post_id);
		if ($post == null) {
			exit;
		}
				
		$m1 = array();
		preg_match_all('|\[wpdm_els_adapt id\s*=\s*["\']?(\d+)["\']?[^]]*\]|u', $post->post_content, $m1, PREG_OFFSET_CAPTURE);
		$m2 = array();
		preg_match_all('|\[wpdm_package\s+id\s*=\s*["\']?(\d+)["\']?[^]]*\]|u', $post->post_content, $m2, PREG_OFFSET_CAPTURE);
		$m = array_merge($m1[1], $m2[1]);
		
		if (count($m) > 1 ) {
			
			global $wpdb;
			
			$table_name = $wpdb->prefix . 'ahm_download_stats';
			
			foreach($m as $wpdm) {
				$wpdmid = $wpdm[0];
				
				$r = $wpdb->get_results( "select p.post_title, pm.meta_value files, u.ID, u.user_login, u.user_nicename, w.*, count(u.ID) downloads from $table_name w 
				left join {$wpdb->users} u on u.ID = w.uid
				left join {$wpdb->posts} p on p.ID = w.pid
				left join {$wpdb->postmeta} pm on pm.post_id = w.pid and pm.meta_key = '__wpdm_files'
				where w.pid = $wpdmid group by u.ID");
				
				foreach($r as $row) {
					
					if ($row->uid == 0) continue;

					$files = unserialize($row->files);

					$csvItem = array();
					$csvItem['Timestamp'] = $row->year.'-'.$row->month.'-'.$row->day;
					$csvItem['Post id'] = $post_id;
					$csvItem['Post title'] = $post->post_title;
					$csvItem['Material id'] = $row->pid;
					$csvItem['Material Title'] = $row->post_title;
					$csvItem['Material File'] = join(', ', array_map('basename', $files));
					$csvItem['User id'] = $row->uid;
					//$csvItem['User login'] = $row->user_login;
					$csvItem['Username'] = $row->user_nicename;
					$csvItem['Nr downloads'] = $row->downloads;
					$csvItems[] = $csvItem;
				}
			}
		}
		
		return $csvItems;
	}
	
  /*
      posts_per_user: la lista dei post creati dall’utente o che gli sono piaciuti.
   comments_per_user: la lista dei commenti creati dall’utente o che gli sono piaciuti.
  downloads_per_user: la lista dei download dell'utente
  */
  private function generate_x_per_user() {
    $user_id = $_POST['user'];
    if ($user_id != null) {
			if (isset($_POST['posts_per_user'])) {
				$csvItems = $this->generate_posts_per_user($user_id);
				$filename = $sitename . "posts_per_user_{$user_id}.csv";
			}
			else if (isset($_POST['comments_per_user'])) {
				$csvItems = $this->generate_comments_per_user($user_id);
				$filename = $sitename . "comments_per_user_{$user_id}.csv";
			}
      
    }
    
    if ( !$csvItems ) {
			$this->error = "No data found.";
			return;
		}
	    
		$this->export_csv($filename, $csvItems);
		exit;
  }

  /*
  posts_per_user: la lista dei post creati dall’utente o che gli sono piaciuti.
  */
  private function generate_posts_per_user($user_id) {
    $csvItems = array();
    global $wpdb;
    $ulike = $wpdb->prefix . 'ulike';
    //$r = $wpdb->get_results( "select p1.ID p1_id, p1.post_title p1_title, p2.ID p2_id, p2.post_title p2_title, u.user_nicename, u2.user_nicename author from {$wpdb->users} u 
    //    left join  {$ulike} l on l.user_id = u.ID and l.status = 'like'
		//		left join {$wpdb->posts} p1 on p1.post_author = u.ID and p1.post_status='publish' and p1.post_type='post'
		//		left join {$wpdb->posts} p2 on p2.ID = l.post_id and p2.post_status='publish' and p2.post_type='post'
    //    left join {$wpdb->users} u2 on u2.ID = p2.post_author
		//		where u.ID = $user_id ");
            
    $r = $wpdb->get_results( "select p.ID post_id, p.post_title, u.user_nicename from {$wpdb->users} u 
				left join {$wpdb->posts} p on p.post_author = u.ID and p.post_status='publish' and p.post_type='post'
				where u.ID = $user_id ");
            
    foreach($r as $row) {
      $csvItem = array();
      $csvItem['username'] = $row->user_nicename;
      $csvItem['Post id'] = $row->post_id;
      $csvItem['Post title'] = $row->post_title;
      $csvItem['Action'] = 'C';
      $csvItem['Author'] = $row->user_nicename;
      $csvItem['Link'] = get_permalink($row->post_id);
      $csvItems[] = $csvItem;
    }

    $r = $wpdb->get_results( "select p.ID post_id, p.post_title, u.user_nicename from {$ulike} l 
				join {$wpdb->posts} p on p.ID = l.post_id and p.post_status='publish' and p.post_type='post'
        join {$wpdb->users} u on u.ID = p.post_author
				where l.status = 'like' and l.user_id = $user_id ");
            
    foreach($r as $row) {
      $csvItem = array();
      $csvItem['username'] = $row->user_nicename;
      $csvItem['Post id'] = $row->post_id;
      $csvItem['Post title'] = $row->post_title;
      $csvItem['Action'] = 'L';
      $csvItem['Author'] = $row->user_nicename;
      $csvItem['Link'] = get_permalink($row->post_id);
      $csvItems[] = $csvItem;
    }
    
    return $csvItems;
  }
  /*
  Comments_per_user: la lista dei commenti creati dall’utente o che gli sono piaciuti.
  */
  private function generate_comments_per_user($user_id) {
    $csvItems = array();
    global $wpdb;
    $ulike = $wpdb->prefix . 'ulike';
    
    /*
    select c.comment_author, u.user_nicename, c.comment_ID, cm.meta_value comment_title, c.comment_content, p.ID post_id, p.post_title
    from esci_6_comments c
    left join esci_6_posts p on p.ID = c.comment_post_ID
    left join esci_users u on u.ID = c.user_id
    left join esci_6_commentmeta cm on cm.comment_id = c.comment_ID and cm.meta_key = 'review_title'
    where c.comment_approved = 1 and c.user_id = 1001133
    */
    
    $r = $wpdb->get_results( "select c.comment_author, u.user_nicename, u.ID user_id, c.comment_ID, cm.meta_value comment_title, c.comment_content, p.ID post_id, p.post_title
      from {$wpdb->comments} c
      left join {$wpdb->posts} p on p.ID = c.comment_post_ID
      left join {$wpdb->users} u on u.ID = c.user_id
      left join {$wpdb->commentmeta} cm on cm.comment_id = c.comment_ID and cm.meta_key = 'review_title'
      where c.comment_approved = 1 and c.user_id = $user_id");
        
    foreach($r as $row) {
      $csvItem = array();
      $csvItem['user_id'] = $row->user_id;
      $csvItem['username'] = $row->comment_author;// . ' - ' . $row->user_nicename;
      $csvItem['Comment id'] = $row->comment_ID;
      $csvItem['Comment title'] = $row->comment_title;
      $csvItem['Action'] = 'C';
      $csvItem['Author'] = $row->user_nicename;
      $csvItem['Post id'] = $row->post_id;
      $csvItem['Post title'] = $row->post_title;
      $csvItem['Link'] = get_comment_link($row->comment_ID);
      $csvItem['Comment text'] = $row->comment_content;
      $csvItems[] = $csvItem;
    }

    $r = $wpdb->get_results( "select c.comment_author, u.user_nicename, u.ID user_id, c.comment_ID, cm.meta_value comment_title, c.comment_content, p.ID post_id, p.post_title
      from {$wpdb->prefix}ulike_comments l
      left join {$wpdb->comments} c on c.comment_ID = l.comment_id
      left join {$wpdb->posts} p on p.ID = c.comment_post_ID
      left join {$wpdb->users} u on u.ID = c.user_id
      left join {$wpdb->commentmeta} cm on cm.comment_id = c.comment_ID and cm.meta_key = 'review_title'
      where l.status = 'like' and l.user_id = $user_id");

    foreach($r as $row) {
      $csvItem = array();
      $csvItem['user_id'] = $row->user_id;
      $csvItem['username'] = $row->comment_author;// . ' - ' . $row->user_nicename;
      $csvItem['Comment id'] = $row->comment_ID;
      $csvItem['Comment title'] = $row->comment_title;
      $csvItem['Action'] = 'L';
      $csvItem['Author'] = $row->user_nicename;
      $csvItem['Post id'] = $row->post_id;
      $csvItem['Post title'] = $row->post_title;
      $csvItem['Link'] = get_comment_link($row->comment_ID);
      $csvItem['Comment text'] = $row->comment_content;
      $csvItems[] = $csvItem;
    }
    
    return $csvItems;
  }
  
	private function export_csv($filename, $items) {
		//ob_clean();
		header( 'Content-Description: File Transfer' );
		header( 'Content-Disposition: attachment; filename=' . $filename );
		header( 'Content-Type: text/csv; charset=' . get_option( 'blog_charset' ), true );
		
		$output = fopen('php://output', 'w');
		fwrite($output, "\xEF\xBB\xBF");
		$headers = array();
		foreach ( $items[0] as $key => $field ) {
			$headers[] = $key;
		}
		fputcsv($output, $headers);


		foreach ( $items as $i ) {
			$data = array();
			foreach ( $i as $k=>$v ) {
				$data[] = $v;
			}
			fputcsv($output, $data);

		}
	}
	
	public function render_html() {
		if ( ! current_user_can( 'list_users' ) )
			wp_die( __( 'You do not have sufficient permissions to access this page.', 'export-users-to-csv' ) );
		
		if (isset($this->error))
			echo "<div class='updated'><p><strong>{$this->error}</strong></p></div>";
		
		$this->scripts();
		$this->users_to_csv();
		$this->downloads_csv();
		$this->list_all_posts();
		$this->post_materials();
		$this->post_comments();
    $this->x_per_user();
	}
	
	private function scripts() {
		wp_enqueue_script( 'jquery', plugins_url( '/jquery-2.0.2.min.js', __FILE__ ), null);
		wp_enqueue_style( 'jquery-ui', plugins_url( '/jq/jquery-ui.min.css', __FILE__ ) );
		wp_enqueue_style( 'jquery-ui-s', plugins_url( '/jq/jquery-ui.structure.min.css', __FILE__ ) );
		wp_enqueue_style( 'jquery-ui-t', plugins_url( '/jq/jquery-ui.theme.min.css', __FILE__ ) );
		wp_enqueue_script( 'jquery-ui', plugins_url( '/jq/jquery-ui.min.js', __FILE__ ), array('jquery'));
		wp_enqueue_script( 'els_activate_html5date', plugins_url( '/mod.js', __FILE__ ), array('jquery', 'jquery-ui'), null, true );		
	}
	
	private function users_to_csv() {
		?>
		<h2>Export users to a CSV file</h2>
		<form method="post" action="" enctype="multipart/form-data">
			<?php wp_nonce_field( 'els-users-csv-export', '_wpnonce-els-users-csv' ); ?>
			<table class="form-table">
				<tr class="form-field">				
					<td>
						<label for="els_sites">Site</label>
						<select name="site" id="els_sites">
							<!--option value='all'>All</option-->
						<?php 
						$sites = wp_get_sites(array('deleted' => false, 'spam' => false, 'archived'=> false, 'public' => true));
						foreach ($sites as $site) {
							if ($site['path'] != '/') {
							echo "<option value='{$site['blog_id']}'>".substr($site['path'], 1,-1).'</option>';
							}
						} 
						?>
					</td>
					<td>
						<label for="els_roles">Role</label>
						<select name="els_user_role"">
						<?php wp_dropdown_roles( 'subscriber' ); //get all users role ?>
						</select>
					</td>
				</tr>
			</table>
			<p class="submit">
				<input type="hidden" name="_wp_http_referer" value="<?php echo $_SERVER['REQUEST_URI'] ?>" />
				<input type="hidden" name="_wp_http_action" value="users_to_csv" />
				<input type="submit" class="button-primary" value="<?php _e( 'Export', 'export-users-to-csv' ); ?>" />
			</p>
		</form>
		<?php
	}
	
	private function downloads_csv() {
		?>
		<br/><h2>Export downloads to a CSV file</h2>
	  <i>For each "File" (material) returns: Id, Material title, NrDownloads</i>
		<form method="post" action="" enctype="multipart/form-data">
			<?php wp_nonce_field( 'els-users-csv-export', '_wpnonce-els-users-csv' ); ?>
			<label for="startdate"><?php _e( 'Start date:', 'engage')?></label><input type="mydate" id="startdate" name="startdate" value="" />
			&nbsp;<label for="enddate"><?php _e( 'End date:', 'engage')?></label><input type="mydate" id="enddate" name="enddate" value="" />
			<p class="submit">
				<input type="hidden" name="_wp_http_referer" value="<?php echo $_SERVER['REQUEST_URI'] ?>" />
				<input type="hidden" name="_wp_http_action" value="downloads_csv" />
				
				<input type="submit" class="button-primary" value="<?php _e( 'Export', 'export-users-to-csv' ); ?>" />
			</p>
		</form>
		<?php
	}
	
	/*
	posts_list: lists all posts
	- for each post: id, title, type, total number of downloads = sum of number of downloads for each file into the post
	- 3 average ratings
	- number of likes to the post
	*/
	private function list_all_posts() {
		?>
		<br/><h2>All posts</h2>
		<i>	For each post: 
		id, title, type, total number of downloads = sum of number of downloads for each file into the post,
		3 average ratings, number of likes to the post
		</i><br/>
		<form method="post" action="" enctype="multipart/form-data">
			<?php wp_nonce_field( 'els-users-csv-export', '_wpnonce-els-users-csv' ); /* ?>
			<label for="startdate"><?php _e( 'Start date:', 'engage')?></label><input type="mydate" id="startdate_lap" name="startdate" value="" />
			&nbsp;<label for="enddate"><?php _e( 'End date:', 'engage')?></label><input type="mydate" id="enddate_lap" name="enddate" value="" />
			*/?><p class="submit">
				<input type="hidden" name="_wp_http_referer" value="<?php echo $_SERVER['REQUEST_URI'] ?>" />
				<input type="hidden" name="_wp_http_action" value="list_all_posts" />
				
				<input type="submit" class="button-primary" value="<?php _e( 'Export', 'export-users-to-csv' ); ?>" />
			</p>
		</form>
		<?php
	}

	/*
	lists files in post and related downloads
	*/
	private function post_materials() {
		?>
		<br/><h2>Lists files in post and related downloads</h2>
		<i>	For each material: 
		id, title, total number of downloads
		</i><br/>
		<form method="post" action="" enctype="multipart/form-data">
			<?php wp_nonce_field( 'els-users-csv-export', '_wpnonce-els-users-csv' ); /* ?>
			<label for="startdate"><?php _e( 'Start date:', 'engage')?></label><input type="mydate" id="startdate_lap" name="startdate" value="" />
			&nbsp;<label for="enddate"><?php _e( 'End date:', 'engage')?></label><input type="mydate" id="enddate_lap" name="enddate" value="" />
			<label for="post_id"><?php _e( 'Post:', 'engage')?></label>
			<select name="post_id">
			<?php 
					$args=array(
						'category_name ' => 'resources',
						'posts_per_page' => -1,
						'post_status' => 'publish',
						'caller_get_posts'=> 1,
						'post_type' => 'post',
					);
					$query = new WP_Query($args);
					while ( $query->have_posts() ) {
						$query->next_post();
						$categories = get_the_category($query->post->ID);
						$isResource = false;
						foreach($categories as $cat) {
							if ($cat->slug == 'resources') {
								$isResource = true;
								break;
							}
						}
						if (!$isResource) continue;
						echo "<option value='{$query->post->ID}'>{$query->post->post_title}</option>";
					}
			?>
			</select>
			*/?>

			<p class="submit">
				<input type="hidden" name="_wp_http_referer" value="<?php echo $_SERVER['REQUEST_URI'] ?>" />
				<input type="hidden" name="_wp_http_action" value="post_materials" />
				
				<input type="submit" class="button-primary" value="<?php _e( 'Export', 'export-users-to-csv' ); ?>" />
			</p>
		</form>
		<?php
	}
	
	/*
	post_comments: data about comments for a given post
	*/
	private function post_comments() {
		?>
		<br/><h2>Activity on posts:</h2>
		<i>	Data about comments and downloads for a given post</i><br/>


		<form method="post" action="" enctype="multipart/form-data">
			<?php wp_nonce_field( 'els-users-csv-export', '_wpnonce-els-users-csv' ); /* ?>
			<label for="startdate"><?php _e( 'Start date:', 'engage')?></label><input type="mydate" id="startdate_lap" name="startdate" value="" />
			&nbsp;<label for="enddate"><?php _e( 'End date:', 'engage')?></label><input type="mydate" id="enddate_lap" name="enddate" value="" />
			*/?>
			<label for="post_id"><?php _e( 'Post:', 'engage')?></label>
			<select name="post_id">
			<?php 
					$args=array(
						'category_name ' => 'resources',
						'posts_per_page' => -1,
						'post_status' => 'publish',
						'caller_get_posts'=> 1,
						'post_type' => 'post',
					);
					$query = new WP_Query($args);
					while ( $query->have_posts() ) {
						$query->next_post();
						$categories = get_the_category($query->post->ID);
						$isResource = false;
						foreach($categories as $cat) {
							if ($cat->slug == 'resources') {
								$isResource = true;
								break;
							}
						}
						if (!$isResource) continue;
						echo "<option value='{$query->post->ID}'>{$query->post->post_title}</option>";
					}
			?>
			</select>

			<p class="submit">
				<input type="hidden" name="_wp_http_referer" value="<?php echo $_SERVER['REQUEST_URI'] ?>" />
				<input type="hidden" name="_wp_http_action" value="post_comments" />
				
				<input name="post_comments" type="submit" class="button-primary" value="<?php _e( 'Comments', 'export-users-to-csv' ); ?>" />
				<input name="comment_likers" type="submit" class="button-primary" value="<?php _e( 'Comments Likers', 'export-users-to-csv' ); ?>" />
				<input name="post_ratings" type="submit" class="button-primary" value="<?php _e( 'Post User Ratings', 'export-users-to-csv' ); ?>" />
				<input name="post_likers" type="submit" class="button-primary" value="<?php _e( 'Post User Likers', 'export-users-to-csv' ); ?>" />
				<input name="posts_users_downloads" type="submit" class="button-primary" value="<?php _e( 'Post User Downloads', 'export-users-to-csv' ); ?>" />
			</p>
		</form>
		<?php
  }
  
	/*
      posts_per_user: la lista dei post creati dall’utente o che gli sono piaciuti.
   comments_per_user: la lista dei commenti creati dall’utente o che gli sono piaciuti.
  downloads_per_user: la lista dei download dell'utente
	*/
	private function x_per_user() {
		?>
		<br/><h2>Data per user:</h2>
		<ul>
    <li><i>Posts per user:</i> list of posts created or liked by the user.</li>
    <li><i>Comments per user:</i> list of comments created or liked by the user.</li>
    <li><i>Downloads per user:</i> list of materials downloaded by the user.</li>
    </ul>
		<br/>
		<form method="post" action="" enctype="multipart/form-data">
			<?php wp_nonce_field( 'els-users-csv-export', '_wpnonce-els-users-csv' ); /* ?>
			<label for="startdate"><?php _e( 'Start date:', 'engage')?></label><input type="mydate" id="startdate_lap" name="startdate" value="" />
			&nbsp;<label for="enddate"><?php _e( 'End date:', 'engage')?></label><input type="mydate" id="enddate_lap" name="enddate" value="" />
			*/?>
			<label for="user_id"><?php _e( 'User:', 'engage')?></label>
			<?php 
        $args = array();
        if ( isset($_POST['user']) ) $args['selected'] = $_POST['user'];
        wp_dropdown_users(  $args );
			?>
     
			<p class="submit">
				<input type="hidden" name="_wp_http_referer" value="<?php echo $_SERVER['REQUEST_URI'] ?>" />
				<input type="hidden" name="_wp_http_action" value="x_per_user" />
  
				<input name="posts_per_user" type="submit" class="button-primary" value="<?php _e( 'Posts per user', 'export-users-to-csv' ); ?>" />
				<input name="comments_per_user" type="submit" class="button-primary" value="<?php _e( 'Comments per user', 'export-users-to-csv' ); ?>" />
				<!--<input name="downloads_per_user" type="submit" class="button-primary" value="<?php _e( 'Downloads per user', 'export-users-to-csv' ); ?>" />-->
			</p>
		</form>
    <?php
  }
	
}

new ELS_Users_CSV();
