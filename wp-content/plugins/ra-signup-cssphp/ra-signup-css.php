<?php
/*
Plugin Name: Signup Page CSS
Plugin URI: http://wpmututorials.com
Description: This plugin adds the styles/codes needed to have signup page display correctly or at least better on any theme that the main blog has selected.. 
Version: 0.1
Author: Ron & Andrea Rennick
Author URI: http://ronandandrea.com/
*/

/* 
	Copyright:	(C) 2009 Ron Rennick, All rights reserved. 
	Released under GPL.
*/

function ra_signup_css() { ?>
<style type="text/css">
.widecolumn .entry p { font-size: 1.05em;	}
.widecolumn {	line-height: 1.6em;	}
.widecolumn {	padding: 10px 0 20px 0;	margin: 5px 0 0 150px;	width: 450px;	}
.widecolumn .post {	margin: 0;	}

#content {background: #f1f1f1 !important;}

#header { display: none; }
.mu_register { width: 350px; margin:0 auto; background: ;}
.mu_register h2 {
font-size: 1.2em;
background-color: #eb6937;
color: white;
padding: 7px;
}
.mu_register form { margin-top: 20px;
margin-left: 0;
padding: 26px 24px 46px;
font-weight: 400;
font-size: 0.7em;
color: #777;
overflow: hidden;
background: #fff;
-webkit-box-shadow: 0 1px 3px rgba(0,0,0,.13);
box-shadow: 0 1px 3px rgba(0,0,0,.13);
}
.mu_register form input {
  border: solid 1px #E6DB55;
  background-color: #FFFFE0;
}
.mu_register label {
  font-size: 1.2em;
  margin: 0px;
}
.alo_easymail_reg_optin label {
  display: inline;
  margin-left: 7px;
}

/* defaults from signup page. feel free to override. It's commented out for now.
		.mu_register { width: 90%; margin:0 auto; }
		.mu_register form { margin-top: 2em; }
		.mu_register .error { font-weight:700; padding:10px; color:#333333; background:#FFEBE8; border:1px solid #CC0000; }
		.mu_register #submit,
			.mu_register #blog_title,
			.mu_register #user_email, 
			.mu_register #blogname,
			.mu_register #user_name { width:100%; font-size: 24px; margin:5px 0; }	
		.mu_register .prefix_address,
			.mu_register .suffix_address {font-size: 18px;display:inline; }			
		.mu_register label { font-weight:700; font-size:15px; display:block; margin:10px 0; }
		.mu_register label.checkbox { display:inline; }
		.mu_register .mu_alert { font-weight:700; padding:10px; color:#333333; background:#ffffe0; border:1px solid #e6db55; }
 */
</style>
<?php 
} 
function ra_add_signup_css () { add_action('wp_head','ra_signup_css', 99); }
add_action('signup_header','ra_add_signup_css');
?>