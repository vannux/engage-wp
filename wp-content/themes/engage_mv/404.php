<?php get_header(); ?>
<!-- content / social -->
<div id="content">
	<div class="container">
    	<div class="row">
        	<div class="col_12">
                <section id="content" role="main">
                	<article id="post-0" class="post not-found">
                        <header class="header">
                            <h1><?php _e( 'Not Found', 'engage' ); ?></h1>
                        </header>
                        <section class="entry-content">
                            <p><?php _e( 'Nothing found for the requested page. Try a search instead?', 'engage' ); ?></p>
                            <?php get_search_form(); ?>
                        </section>
                	</article>
                </section>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<!-- content ends -->
<?php #get_sidebar(); ?>
<?php get_footer(); ?>