<?php get_header(); ?>
<!-- content / social -->
<div id="content">
	<div class="container">
    	<div class="row">
        	<div class="col_12">
            	<section id="content" role="main">
                    <header class="header">
                    	<h1><?php
                    	if ( is_day() ) { printf( __( 'Daily Archives: %s', 'engage' ), get_the_time( get_option( 'date_format' ) ) ); }
                    	elseif ( is_month() ) { printf( __( 'Monthly Archives: %s', 'engage' ), get_the_time( 'F Y' ) ); }
                    	elseif ( is_year() ) { printf( __( 'Yearly Archives: %s', 'engage' ), get_the_time( 'Y' ) ); }
                    	else { _e( 'Archives', 'engage' ); }
                    	?></h1>
                    </header>
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    	<?php get_template_part( 'entry' ); ?>
                    <?php endwhile; endif; ?>
                    <?php get_template_part( 'nav', 'below' ); ?>
                </section>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<!-- content ends -->
<?php #get_sidebar(); ?>
<?php get_footer(); ?>
