<?php get_header(); ?>
<!-- content / social -->
<div id="content">
	<div class="container">
    	<div class="row">
        	<div class="col_12">
                <section id="content" role="main">
                    <header class="header">
                        <?php the_post(); ?>
                        <h1 class="author"><?php _e( 'Author Archives', 'engage' ); ?>: <?php the_author_link(); ?></h1>
                        <?php if ( '' != get_the_author_meta( 'user_description' ) ) echo apply_filters( 'archive_meta', '<div class="archive-meta">' . get_the_author_meta( 'user_description' ) . '</div>' ); ?>
                        <?php rewind_posts(); ?>
                    </header>
                    <?php while ( have_posts() ) : the_post(); ?>
                        <?php get_template_part( 'entry' ); ?>
                    <?php endwhile; ?>
                    <?php get_template_part( 'nav', 'below' ); ?>
                </section>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<!-- content ends -->
<?php #get_sidebar(); ?>
<?php get_footer(); ?>