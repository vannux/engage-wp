<?php get_header(); ?>
<!-- content / social -->
<div id="content">
	<div class="container">
    	<div class="row">
        	<div class="col_12">
                <section id="content" role="main">
                <header class="header">
                <h1><?php _e( 'Category Archives:', 'engage' ); ?> <?php single_cat_title(); ?></h1>
                <?php if ( '' != category_description() ) echo apply_filters( 'archive_meta', '<div class="archive-meta">' . category_description() . '</div>' ); ?>
                </header>
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <?php get_template_part( 'entry' ); ?>
                <?php endwhile; endif; ?>
                <?php get_template_part( 'nav', 'below' ); ?>
                </section>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<!-- content ends -->
<?php #get_sidebar(); ?>
<?php get_footer(); ?>