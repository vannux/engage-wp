<!-- footer -->
<footer id="footer" role="contentinfo">
	<div class="container">
    	<div class="row">
        	<div class="col_6">
            	<a href="http://cordis.europa.eu/projects/rcn/111469_en.html" title="<?php _e( 'European Unions Seventh Framework Programme', 'engage' ); ?>" target="_blank">
				  <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/flag_black_white.png" class="alignleft" alt="<?php _e( 'European Unions Seventh Framework Programme', 'engage' ); ?>" /></a>
                <p><?php _e( 'This project has received funding from the European Union\'s Seventh Framework Programme for research, technological development and demonstration under grant agreement no.', 'engage' ); ?> <strong>612269</strong></p>
            </div>
            <div class="col_6 textright last">
            	<a href="https://www.facebook.com/engagingscience" title="Engaging Science on Facebook" target="_blank">
				  <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/facebook.png" width="32" alt="Facebook" /></a> 
				<a href="<?php _e('https://twitter.com/engage_science', 'engage'); ?>" title="Engaging Science on Twitter" target="_blank">
				  <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/twitter.png" width="32" alt="Twitter" /></a> 
				<a href="http://www.pinterest.com/engagingscience/" title="Engaging Science on Pinterest" target="_blank">
				  <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/pinterest.png" width="32" alt="Pinterest" /></a> 
				<a href="https://www.youtube.com/channel/UCQ6sDUmGJ0-LnLBJtd3k-hQ/playlists" title="<?php _e( 'Engaging Science on YouTube', 'engage' ); ?>" target="_blank">
				  <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/youtube.png" width="32" alt="YouTube" /></a>
                <div class="clear"></div>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/CC-BY-SA.png" height="50" alt="<?php _e( 'Creative Commons license', 'engage' ); ?>" />
            </div>
            <div class="clear"></div>
        </div>
    </div>
</footer>
<!-- footer ends -->
<!-- javascript includes -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/modernizr.custom.js"></script>    
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/jquery.bxslider.js"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/classie.js"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/engage.scripts.js"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/slider.script.js"></script>
<!-- javascript includes end -->
<?php wp_footer(); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-45929135-3', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>