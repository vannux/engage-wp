<?php

/*/ NO ACTIVATION LINK REQUIRED
function els_disable_activation( $user, $user_email, $key, $meta = '' ) {
	// Activate the user
	wpmu_activate_signup( $key );

	// Return false so no email sent
	return false;
}
add_filter( 'wpmu_signup_user_notification', 'els_disable_activation', 10, 4 );
//Disable new blog notification email for multisite
add_filter( 'wpmu_signup_blog_notification', '__return_false' );
// disable sending activation emails for multisite
add_filter( 'wpmu_signup_user_notification', '__return_false', 1, 4 );
*/

//add_filter('wp_mail_from', 'new_mail_from');
//add_filter('wp_mail_from_name', 'new_mail_from_name');
//function new_mail_from($old) { return 'engageadmin@upd8.org.uk'; }
//function new_mail_from_name($old) { return 'EngagingScience'; }



/**
* vannux: filter for category template
*/
add_filter('single_template', 'els_single_template');
function els_single_template($single) {
	global $wp_query, $post;
	// /var/www/html/wp_t1/wp-content/themes/engage_mv
	foreach((array)get_the_category() as $cat) {
		if(file_exists(TEMPLATEPATH . '/single-cat-' . $cat->slug . '.php'))
			return TEMPLATEPATH . '/single-cat-' . $cat->slug . '.php';

		elseif(file_exists(TEMPLATEPATH . '/single-cat-' . $cat->term_id . '.php'))
			return TEMPLATEPATH . '/single-cat-' . $cat->term_id . '.php';
	}
	return $single;
}

// Log login errors to Apache error log
add_action('wp_login_failed', 'log_wp_login_fail'); // hook failed login
function log_wp_login_fail($username){
	error_log("WP login failed for username: $username");
}

add_action('wp_login', 'els_wp_login'); // hook failed login
function els_wp_login($user_login, $user){
	$_SESSION['els_logged']='true';
}

/**
 * ELS: login redirect
 */
function els_login_redirect($redirect_to, $requested_redirect_to, $user)
{
    if (!is_wp_error($user) || $requested_redirect_to == null) return site_url();
	return $redirect_to;
}
add_filter('login_redirect', 'els_login_redirect', 10, 3);

/**
 * ELS: Change signup location
 * in order to have it localised
 */
add_filter('wp_signup_location', 'els_signup_location');
function els_signup_location() {
	return get_bloginfo('template_directory').'/wp-signup.php';
}

/**
 * Change wp-activate page
 * func from wp-include/ms-functions.php
 * original = wpmu_signup_user_notification
 * removed
 * if ( !apply_filters('wpmu_signup_user_notification', $user, $user_email, $key, $meta) )
        return false;
 * changed
 * site_url( "wp-activate.php/?key=$key" ) to site_url( "activate/?key=$key" )
 */
add_filter('wpmu_signup_user_notification', 'els_signup_user_notification', 10, 4);
function els_signup_user_notification( $user, $user_email, $key, $meta = array() ) {
	/**
	 * Filter whether to bypass the email notification for new user sign-up.
	 *
	 * @since MU
	 *
	 * @param string $user       User login name.
	 * @param string $user_email User email address.
	 * @param string $key        Activation key created in wpmu_signup_user().
	 * @param array  $meta       Signup meta data.
	 */
	//if ( ! apply_filters( 'wpmu_signup_user_notification', $user, $user_email, $key, $meta ) )
	//	return false;

	// Send email with activation link.
	$admin_email = get_site_option( 'admin_email' );
	if ( $admin_email == '' )
		$admin_email = 'support@' . $_SERVER['SERVER_NAME'];
	$from_name = get_site_option( 'site_name' ) == '' ? 'WordPress' : esc_html( get_site_option( 'site_name' ) );
	$message_headers = "From: \"{$from_name}\" <{$admin_email}>\n" . "Content-Type: text/plain; charset=\"" . get_option('blog_charset') . "\"\n";
	$message = sprintf(
		/**
		 * Filter the content of the notification email for new user sign-up.
		 *
		 * Content should be formatted for transmission via wp_mail().
		 *
		 * @since MU
		 *
		 * @param string $content    Content of the notification email.
		 * @param string $user       User login name.
		 * @param string $user_email User email address.
		 * @param string $key        Activation key created in wpmu_signup_user().
		 * @param array  $meta       Signup meta data.
		 */
		apply_filters( 'wpmu_signup_user_notification_email',
			__( "To activate your user, please click the following link:\n\n%s\n\nAfter you activate, you will receive *another email* with your login." ),
			$user, $user_email, $key, $meta
		),
		//site_url( "wp-activate.php?key=$key" )
		get_bloginfo('template_directory') . "/wp-activate.php?key=$key"
	);
	// TODO: Don't hard code activation link.
	$subject = sprintf(
		/**
		 * Filter the subject of the notification email of new user signup.
		 *
		 * @since MU
		 *
		 * @param string $subject    Subject of the notification email.
		 * @param string $user       User login name.
		 * @param string $user_email User email address.
		 * @param string $key        Activation key created in wpmu_signup_user().
		 * @param array  $meta       Signup meta data.
		 */
		apply_filters( 'wpmu_signup_user_notification_subject',
			__( '[%1$s] Activate %2$s' ),
			$user, $user_email, $key, $meta
		),
		$from_name,
		$user
	);
	wp_mail( $user_email, wp_specialchars_decode( $subject ), $message, $message_headers );
	return false;
}

/**
 * ELS: login logo
 */
function els_login_logo() { ?>
<style type="text/css">
	.login h1 a {
		background-image:url(<?php echo get_bloginfo('template_directory') ?>/images/engage-logo.png) !important;
		width: 185px !important;
		background-size: 185px 80px !important;
		background-repeat: no-repeat;
	}
</style>
<?php }
add_action('login_head', 'els_login_logo');

add_action( 'personal_options', array ( 'T5_Hide_Profile_Bio_Box', 'start' ) );
/**
 * Captures the part with the biobox in an output buffer and removes it.
 *
 * @author Thomas Scholz, <info@toscho.de>
 *
 */
class T5_Hide_Profile_Bio_Box
{
    /**
     * Called on 'personal_options'.
     *
     * @return void
     */
    public static function start()
    {
        $action = ( IS_PROFILE_PAGE ? 'show' : 'edit' ) . '_user_profile';
        add_action( $action, array ( __CLASS__, 'stop' ) );
        ob_start();
    }

    /**
     * Strips the bio box from the buffered content.
     *
     * @return void
     */
    public static function stop()
    {
        $html = ob_get_contents();
        ob_end_clean();

        // remove the headline
        $headline = __( IS_PROFILE_PAGE ? 'About Yourself' : 'About the user' );
        $html = str_replace( '<h3>' . $headline . '</h3>', '', $html );

        // remove the table row
        $html = preg_replace( '~<tr class="user-description-wrap">\s*<th><label for="description".*</tr>~imsUu', '', $html );
        print $html;
    }
}


function engage_login() { ?>
<style>
.create-account a {
	display: block;
	background: #eb6937;
	margin: 20px 0 10px 0;
	padding: 5px 8px;
	color: #fff;
	font-size: 16px;
	font-size: 1.2em;
	font-weight: bold;
	text-decoration: none;
}
</style>
<div class="create-account">
	<?php 
	$registration_url = sprintf( '<a href="%s">%s</a>', esc_url( wp_registration_url() ), __( 'Get your Engaging Science account in seconds' ) );
	echo apply_filters( 'Get your Engaging Science account in seconds', $registration_url ); 
	?>
</div>
<?php }
add_action( 'login_message', 'engage_login' );

add_action( 'after_setup_theme', 'engage_setup' );
add_theme_support('html5', array(
		'search-form'
) );
function engage_setup() {
	load_theme_textdomain ( 'engage', get_template_directory () . '/languages' );
	add_theme_support ( 'automatic-feed-links' );
	add_theme_support ( 'post-thumbnails' );
	global $content_width;
	if (! isset ( $content_width ))
		$content_width = 640;
	register_nav_menus ( array (
			'main-menu' => __ ( 'Main Menu', 'engage' ),
			'top-menu' => __ ( 'Top Menu', 'engage' ) )
	);
}

// Shorten next / previous titles
function shrink_previous_post_link($format, $link){
    $in_same_cat = false;
    $excluded_categories = '4';
    $previous = true;
    $link='&laquo; %title';
    $format='%link';


    if ( $previous && is_attachment() )
        $post = & get_post($GLOBALS['post']->post_parent);
    else
        $post = get_adjacent_post($in_same_cat, $excluded_categories, $previous);

    if ( !$post )
        return;

    $title = $post->post_title;

    if ( empty($post->post_title) )
        $title = $previous ? __('Previous Post') : __('Next Post');

    $rel = $previous ? 'prev' : 'next';

    //Save the original title
    $original_title = $title;

    //create short title, if needed
    if (strlen($title)>40){
        $first_part = substr($title, 0, 23);
        $last_part = substr($title, -17);
        $title = $first_part."...".$last_part;
    }

    $string = '<a href="'.get_permalink($post).'" rel="'.$rel.'" title="'.$original_title.'">';
    $link = str_replace('%title', $title, $link);
    $link = $string . $link . '</a>';

    $format = str_replace('%link', $link, $format);

    echo $format;
}

function shrink_next_post_link($format, $link){
    $in_same_cat = false;
    $excluded_categories = '4';
    $previous = false;
    $link='%title &raquo;';
    $format='%link';

    if ( $previous && is_attachment() )
        $post = & get_post($GLOBALS['post']->post_parent);
    else
        $post = get_adjacent_post($in_same_cat, $excluded_categories, $previous);

    if ( !$post )
        return;

    $title = $post->post_title;

    if ( empty($post->post_title) )
        $title = $previous ? __('Previous Post') : __('Next Post');

    $rel = $previous ? 'prev' : 'next';

    //Save the original title
    $original_title = $title;

    //create short title, if needed
    if (strlen($title)>40){
        $first_part = substr($title, 0, 23);
        $last_part = substr($title, -17);
        $title = $first_part."...".$last_part;
    }

    $string = '<a href="'.get_permalink($post).'" rel="'.$rel.'" title="'.$original_title.'">';
    $link = str_replace('%title', $title, $link);
    $link = $string . $link . '</a>';

    $format = str_replace('%link', $link, $format);

    echo $format;
}

add_filter('next_post_link', 'shrink_next_post_link',10,2);
add_filter('previous_post_link', 'shrink_previous_post_link',10,2);

add_action( 'wp_enqueue_scripts', 'engage_load_scripts' );
function engage_load_scripts() {
	wp_enqueue_script ( 'jquery' );
}


add_action ( 'comment_form_before', 'engage_enqueue_comment_reply_script' );
function engage_enqueue_comment_reply_script() {
	if (get_option ( 'thread_comments' )) {
		wp_enqueue_script ( 'comment-reply' );
	}
}


add_filter ( 'the_title', 'engage_title' );
function engage_title($title) {
	if ($title == '') {
		return '&rarr;';
	} else {
		return $title;
	}
}


add_filter ( 'wp_title', 'engage_filter_wp_title' );
function engage_filter_wp_title($title) {
	return $title . esc_attr ( get_bloginfo ( 'name' ) );
}


add_action ( 'widgets_init', 'engage_widgets_init' );
function engage_widgets_init() {
	register_sidebar ( array (
			'name' => __ ( 'Sidebar Widget Area', 'engage' ),
			'id' => 'primary-widget-area',
			'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
			'after_widget' => "</div>",
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>'
	) );

	register_sidebar ( array (
		'name' =>  __ ( 'Post-material Widgets', 'engage' ),
		'id' => 'post-material-area',
		'before_widget' => "",
		'after_widget' => ""
	) );

	//register_sidebar ( array (
	//		'name' => 'Twitter Area',
	//		'id' => 'tweetbox-area',
	//		'before_widget' => "<li>",
	//		'after_widget' => "</li>"
	//// 'before_title' => '<h3 class="widget-title">',
	//// 'after_title' => '</h3>',
	//	) );*/
}

function engage_custom_pings($comment) {
	$GLOBALS ['comment'] = $comment;
	?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
<?php
}

add_filter ( 'get_comments_number', 'engage_comments_number' );
function engage_comments_number($count) {
	if (! is_admin ()) {
		global $id;
		$comments_by_type = &separate_comments ( get_comments ( 'status=approve&post_id=' . $id ) );
		return count ( $comments_by_type ['comment'] );
	} else {
		return $count;
	}
}

// ADAPT

function isAdapt($postId) {
	$cats = get_the_category($postId);
	$isAdapt = false;
	foreach($cats as $cat) {
		if ($cat->slug == 'adapt') {
			$isAdapt = true;
			break;
		}
	}
	return $isAdapt;
}

function get_nr_downloads_user($user = null) {
	if ($user == null) $user = wp_get_current_user();
	if (!$user) return null;
	global $wpdb;
	$blog_prefix = $wpdb->base_prefix . get_current_blog_id();
	return $wpdb->get_var("
				SELECT count(*) FROM {$blog_prefix}_posts p
				left join {$blog_prefix}_ahm_download_stats ads on ads.pid = p.id
				where p.post_type='wpdmpro'
				and ads.uid = {$user->ID}");
}

function get_nr_downloads_post($post = null) {
	$totDownloads = 0;
	if ($post == null) $post = get_post();
	if ($post != null) {
		$i = preg_match_all('|\[wpdm_els_adapt id\s*=\s*\'(\d+)\'\]|u', $post->post_content, $m, PREG_OFFSET_CAPTURE);
		//print_r($m);
		if (count($m) > 1 ) {
			foreach($m[1] as $wpdm) {
				$wpdmid = $wpdm[0];
				$r = get_post_meta($wpdmid, '__wpdm_download_count', true);
				$totDownloads += $r;
			}
		}
		$m = array();
		$i = preg_match_all('|\[wpdm_package\s+id\s*=\s*\'(\d+)\'\]|u', $post->post_content, $m, PREG_OFFSET_CAPTURE);
		//print_r($m);
		if (count($m) > 1 ) {
				//print_r($m[1]);

			foreach($m[1] as $wpdm) {
				$wpdmid = $wpdm[0];
				$r = get_post_meta($wpdmid, '__wpdm_download_count', true);
				$totDownloads += $r;
			}
		}
	}
	return $totDownloads;
}

function get_last_activity($user = null) {
	if ($user == null) $user = wp_get_current_user();
	if (!$user) return null;
	global $wpdb;
	$blogid = get_current_blog_id();
	$blog_prefix = $wpdb->base_prefix . $blogid;
	$download_id = $wpdb->get_var("
				SELECT ads.pid FROM {$blog_prefix}_ahm_download_stats ads
				where ads.uid = {$user->ID} order by timestamp DESC");
	if (!$download_id) {
		return null;
	}
	
	$k = "last_activity_{$user->ID}_{$blogid}_{$download_id}";
	if (isset($_SESSION[$k])) {
		$postid = $_SESSION[$k];
	}
	else {
		$postid = $wpdb->get_var("
				SELECT * FROM {$blog_prefix}_posts p
				where p.post_content like '%wpdm_package_id=_{$download_id}_%' AND p.post_status='publish' and p.post_type='post'");
		$_SESSION[$k] = $postid;
	}
	return get_post($postid);
}


function get_nr_comments($user = null) {
	if ($user == null) $user = wp_get_current_user();
	if (!$user) return null;
	global $wpdb;
	$blog_prefix = $wpdb->base_prefix . get_current_blog_id();
	return $wpdb->get_var("SELECT count(*) FROM {$blog_prefix}_comments where user_id = {$user->ID}");
}

function get_nr_replies($user = null) {
	if ($user == null) $user = wp_get_current_user();
	if (!$user) return null;
	global $wpdb;
	$blog_prefix = $wpdb->base_prefix . get_current_blog_id();
	return $wpdb->get_var("SELECT count(*) FROM {$blog_prefix}_comments where user_id = {$user->ID} and comment_parent > 0");
}



// grap news excerpt
function get_sub_excerpt() {
	$excerpt = get_the_content ();
	$excerpt = preg_replace ( " (\[.*?\])", '', $excerpt );
	$excerpt = strip_shortcodes ( $excerpt );
	$excerpt = strip_tags ( $excerpt );
	$excerpt = substr ( $excerpt, 0, 200 );
	$excerpt = substr ( $excerpt, 0, strripos ( $excerpt, " " ) );
	$excerpt = trim ( preg_replace ( '/\s+/', ' ', $excerpt ) );
	$excerpt = $excerpt . '...';
	return $excerpt;
}
function get_front_excerpt() {
	$excerpt = get_the_content ();
	$excerpt = preg_replace ( " (\[.*?\])", '', $excerpt );
	$excerpt = strip_shortcodes ( $excerpt );
	$excerpt = strip_tags ( $excerpt );
	$excerpt = substr ( $excerpt, 0, 300 );
	$excerpt = substr ( $excerpt, 0, strripos ( $excerpt, " " ) );
	$excerpt = trim ( preg_replace ( '/\s+/', ' ', $excerpt ) );
	$excerpt = $excerpt . '...';
	return $excerpt;
}
function get_resource_excerpt() {
	$excerpt = get_the_content ();
	$excerpt = preg_replace ( " (\[.*?\])", '', $excerpt );
	$excerpt = strip_shortcodes ( $excerpt );
	$excerpt = strip_tags ( $excerpt );
	$excerpt = substr ( $excerpt, 0, 140 );
	$excerpt = substr ( $excerpt, 0, strripos ( $excerpt, " " ) );
	$excerpt = trim ( preg_replace ( '/\s+/', ' ', $excerpt ) );
	$excerpt = $excerpt . '...';
	return $excerpt;
}
function catch_that_image() {
	global $post, $posts;
	$first_img = '';
	ob_start ();
	ob_end_clean ();
	$output = preg_match_all ( '/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches );
	$first_img = $matches [1] [0];

	if (empty ( $first_img )) {
		$first_img = get_template_directory_uri() . '/images/news.png';
	}
	return $first_img;
}
// remove width and height from inserted images - responsive
add_filter ( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter ( 'image_send_to_editor', 'remove_width_attribute', 10 );
function remove_width_attribute($html) {
	$html = preg_replace ( '/(width|height)="\d*"\s/', "", $html );
	return $html;
}

// exclude resources from main blog roll
// function exclude_category($query) {
// if($query->is_home || $query->is_archive){
// $query->set('cat', '-4');
// $query->set('cat', '-xx -xx -xx -xx');
// }
// return $query;
// }
// add_filter('pre_get_posts', 'exclude_category');

// count comments
//function comments_shortcode($atts) {
//	extract ( shortcode_atts ( array (
//			'id' => ''
//	), $atts ) );
//	$num = 0;
//	$post_id = $id;
//	$queried_post = get_post ( $post_id );
//	$cc = $queried_post->comment_count;
//	if ($cc == $num || $cc > 1) :
//		$cc = $cc . ' Comments';
//	 else :
//		$cc = $cc . ' Comment';
//	endif;
//	$permalink = get_permalink ( $post_id );
//	return '<a href="#comments">' . $cc . '</a>';
//}
//add_shortcode ( 'comments', 'comments_shortcode' );

add_filter ( 'comment_form_defaults', 'comment_reform' );
function comment_reform($arg) {
	$arg ['title_reply'] = __ ( 'Write a comment', 'engage' );
	$arg ['comment_field'] = '<p class="margin_top_15"><label for="review-title">' . __( 'Title') .'</label></p>
		<p><input id="review_title" name="review_title" style="width:98%" type="text" /></p>
		<p class="comment-form-comment"><label for="review">'.__('Comment', 'engage').'</label></p>
		<p><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></p>';
	$arg ['label_submit'] = __ ( 'Submit comment', 'engage');
	return $arg;
}

add_filter ( 'preprocess_comment', 'verify_comment_meta_data' );
function verify_comment_meta_data($commentdata) {
	$action = isset($_POST['action']) ? $_POST['action'] : '';
	$title = isset($_POST ['review_title']) ? trim($_POST ['review_title']) : '';
	if ( !$title != '' && $action != 'replyto-comment' )
		wp_die ( __ ( 'Error: please fill the required field (Review Title).', 'engage' ) );
	return $commentdata;
}

add_action ( 'comment_post', 'save_comment_meta_data' );
function save_comment_meta_data($comment_id) {
	add_comment_meta ( $comment_id, 'review_title', $_POST ['review_title'] );
}

add_filter ( 'get_comment_text', 'attach_review_to_author' );
function attach_review_to_author($comment_text) {
	$review = get_comment_meta ( get_comment_ID (), 'review_title', true );
	if ($review)
		$comment_text = "<h3>$review</h3> $comment_text";
	return $comment_text;
}
