<?php
  $headerBG = array('headerpurple', 'headeryellow', 'headergreen', 'headerblue', 'headerorange' ); // array of styles
  $i = rand(0, count($headerBG)-1); // generate random number size of the array
  $headerColour = "$headerBG[$i]"; // set variable equal to which random filename was chosen
?>
<!doctype html>  
<html lang="en" class="no-js">
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
  	<title><?php wp_title(' | ', true, 'right'); ?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  	<link rel="icon" href="<?php echo esc_url(home_url('/')); ?>favicon.ico" />
  	<link rel="shortcut icon" href="<?php echo esc_url(home_url('/')); ?>favicon.ico" type="image/x-icon" />
    <!-- The Columnal Grid (1140px wide base, load first), Type and image presets, and mobile stylesheet -->
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/columnal.css" type="text/css" media="screen" />
	<!-- Customizations here --> 
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/custom.css" type="text/css" media="screen" />
    <link href='http://fonts.googleapis.com/css?family=Lato:400,700,900' rel='stylesheet' type='text/css'> 
    <!-- javascript includes -->
    <!--[if lt IE 9]><script src="<?php bloginfo('template_directory'); ?>/js/html5shiv.js"></script><![endif]-->
    <!-- javascript includes end -->
    <?php wp_head(); ?>
</head>
<body id="engaging-science" class="mobile-menu-push">
<!-- hidden nav -->
<nav class="mobile-menu mobile-menu-vertical mobile-menu-left mobile-menu-open" id="mobile-menu-s1">
	<h3><button id="closeLeft"></button></h3>
    <?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
</nav>
<!-- hidden nav ends -->
<!-- navigation -->
<div id="navigation">
    <div class="container">
        <div class="row">          
        	<div class="col_4">            	
                <div class="mobileright"><button id="showLeftMobile" class="mobile"></button></div>
            	<a href="<?php echo esc_url(home_url('/')); ?>" title="Engaging Science"><img src="<?php bloginfo('template_directory'); ?>/images/engage-logo.png" alt="Engaging Science Logo" /></a>
            </div>
            <div class="col_8 last">
                <!-- search -->
                <nav>
                    <ul id="top">
                    	<?php
                        global $user_ID;
                        if($user_ID) {
                            if(current_user_can('level_0')):
                                global $current_user; 
								get_currentuserinfo();
                                echo '<li><a href="' . get_edit_user_link() . '" class="logio" title="' . __( 'Edit your profile', 'engage') . '">'
								. __( 'Logged in as', 'engage' ) . ' ' . (isset($current_user->user_firstname) ? $current_user->user_firstname : $current_user->user_login) . 
								'</a></li>';
                            endif;
						}
                        ?>
                    </ul>
                </nav>
				<?php if (!is_main_site()) { ?>
					<div class="searchsocial">
						<?php get_search_form(); ?>                
					</div>
					<!-- search ends -->
					<button id="showLeftPad" class="pad"></button>
					<nav>
						<ul>
							<li>
							<?php						
							if(is_user_logged_in()):
								echo '<a href="'.wp_logout_url( get_permalink() ).'" title="Logout">'.__('Log out').'</a>';
							else:
								echo '<a href="'.wp_login_url( get_permalink() ).'" title="Login">'.__('Log in').'</a>';
							endif;
							?></li>
						</ul>
						<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
					</nav>
				<?php } ?>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<!-- navigation ends -->
<?php if (is_front_page()): ?>
<!-- logo / banner -->
<header id="header" class="<?php echo $headerColour; ?>">
	<div class="container">
    	<div class="row">
        	<div class="col_12 textcenter">
				<?php 
				$lang = substr(get_blog_details()->path, -4); // <path>/en/
				$dir = get_template_directory() . '/images/banners';
				
				$count = 0;
				if ($handle = opendir($dir . $lang)) {
					while (false !== ($entry = readdir($handle))) {
						$a4 = substr($entry, -4);
						if ($a4 == '.png' || $a4 == '.jpg') $count++;
					}
					closedir($handle);
				}
				
				if ($count == 0 ) $lang = '/';
				
			 ?>
            	<ul class="bxslider grey">
					<?php
					// Show static image if 'it'
					//echo 'xxx'.$lang.'yyy';
					if ($lang == '/it/') {
						
					}
					if ($handle = opendir($dir . $lang)) {
						while (false !== ($entry = readdir($handle))) {
							$a4 = substr($entry, -4);
							//$a5 = substr($entry, -5);
							if ($a4 == '.png' || $a4 == '.jpg') {
								echo '<li style="display:none"><div class="col_12">
										<img src="'. get_bloginfo('template_directory') . '/images/banners' . $lang . $entry . '" alt="Engaging Science" />
									  </div><div class="clear"></div></li>';
							}
						}
						closedir($handle);
					}?>
                </ul>
		
            </div>
            <div class="clear"></div>
        </div>
    </div>
</header>
<!-- logo / banner ends -->
<?php else: ?>
<!-- logo / banner -->
<header id="header" class="<?php echo $headerColour; ?>">
	<div class="container">
    	<div class="row">
        	<div class="col_12 textcenter">&nbsp;</div>
            <div class="clear"></div>
        </div>
    </div>
</header>
<!-- logo / banner ends -->
<?php endif; ?>