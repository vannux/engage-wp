<?php get_header(); ?>
<!-- content / social -->
<div id="content">
	<div class="container">
  	<div class="row">
    	<div class="col_9">
        <section role="main">
                <?php
									$args = array('cat' => '-4,-26');
                	$front_news = new WP_Query($args);
								?>
                <?php if ( $front_news->have_posts() ) : while ( $front_news->have_posts() ) : $front_news->the_post(); ?>
                <?php get_template_part( 'entry' ); ?>
                <?php comments_template(); ?>
                <div class="clear"></div>
                <?php endwhile; endif; ?>
                <?php get_template_part( 'nav', 'below' ); ?>
        </section>
      </div>
			<div class="col_3">
				<?php get_sidebar(); ?>
			</div>
      <div class="clear"></div>
    </div>
  </div>
</div>
<!-- content ends -->

<?php get_footer(); ?>
