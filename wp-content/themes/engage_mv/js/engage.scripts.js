// mobile push menu
var menuLeft = document.getElementById( 'mobile-menu-s1' ),
	showLeftMobile = document.getElementById( 'showLeftMobile' ),
	showLeftPad = document.getElementById( 'showLeftPad' ),
	closeLeft = document.getElementById( 'closeLeft' ),
	body = document.body;

closeLeft.onclick = function() {
	classie.toggle( this, 'active' );
	classie.toggle( menuLeft, 'mobile-menu-open' );
	disableOther( 'closeLeft' );
};
showLeftMobile.onclick = function() {
	classie.toggle( this, 'active' );
	classie.toggle( menuLeft, 'mobile-menu-open' );
	disableOther( 'showLeftMobile' );
};
showLeftPad.onclick = function() {
	classie.toggle( this, 'active' );
	classie.toggle( menuLeft, 'mobile-menu-open' );
	disableOther( 'showLeftPad' );
};

function disableOther( button ) {
	if( button !== 'showLeftMobile' ) {
		classie.toggle( showLeft, 'disabled' );
	}
	if( button !== 'showLeftPad' ) {
		classie.toggle( showLeft, 'disabled' );
	}
	if( button !== 'closeLeft' ) {
		classie.toggle( showLeft, 'disabled' );
	}
}

// offset anchor
$(function(){
    $("[href^='#']").not("[href~='#']").click(function(evt){
        evt.preventDefault();
        var obj = $(this),
        getHref = obj.attr("href").split("#")[1],
        offsetSize = 135;
        $(window).scrollTop($("[name*='"+getHref+"']").offset().top - offsetSize);
    });
});