$().ready(function(){
	var f=function(e) {

		function getR(r, totVotes, percent) {
			var t='<div class="els-rating-rate-item-points-pu">';
			for(var i=1;i<6;i++) {
				t+='<div class="els-rating-rate-item-point els-rating-rate-item-point_'+(i<=r?'e':'d')+'"></div>';
			}
			t+= '<div class="els-rating-rate-item-points-bar1"><div class="els-rating-rate-item-points-bar2" style="width:'+percent+'%"></div></div>';
			t+= '<div class="els-rating-rate-item-points-votes"><div style="width: 42px;float: left;">'+totVotes+'</div><div style="margin-left:5px;float: left;">' + (totVotes==1?Locale['vote']:Locale['votes']) + '</div></div>';
			t+='</div>';
			return t;
		};
		var tag=e.target.getAttribute("data-tag");
		var j=Rating[tag];
		var tot = 0;
		for(var i=1;i<6;i++) if (j[i]!=undefined) tot+=parseInt(j[i].votes);
		var t='<h3>'+tag+'</h3><p>'+j.descr+'</p>';
		for(var i=1;i<6;i++) {
			if (j[i]!=undefined)
				t+=getR(i, j[i].votes, Math.round(100*j[i].votes/tot));
			else 
				t+=getR(i, 0, 0);
		}
		$('#els-rating-votes-pu_content').html(t);
		$('#els-rating-votes-pu').bPopup();
	};
	
	
	
	$(".els-rating-votes").click(f);
	$(".els-img-info-click").click(f);
 });
 
