// preload images
$.fn.preload = function() {
    this.each(function(){
        $('<img/>')[0].src = this;
    });
}
// Usage:
//$(['http://edv-project.net/wp-content/themes/edv/images/banner/EDV-party-leaders-banner.png','http://edv-project.net/wp-content/themes/edv/images/banner/EDV-polling-banner.png','http://edv-project.net/wp-content/themes/edv/images/banner/EDV-no10-banner.png']).preload();

// Image slide variables
$(document).ready(function(){
	$('.bxslider').bxSlider({
		mode: 'fade',
		auto: true,
		adaptiveHeight: true,
		controls: false,
		pager: true,
		pause: 10000
	});
});