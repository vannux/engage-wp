��    +      t  ;   �      �     �     �     �     �     �  
   �                    /  	   B     L     ^  5   z  +   �     �  	   �     �  	     	          ;   *     f     r     �     �     �     �     �  5   �                  �   .     �  	   �  
   �     �               +     7  0  @     q	     �	     �	     �	     �	     �	  	   �	  
   �	     �	     �	     	
     #
     ;
  J   X
  -   �
     �
  
   �
     �
     �
            l   *     �     �     �     �     �     �       \   "          �  
   �  �   �  	   t  	   ~  
   �     �     �     �     �     �                               #          
                                             %      "   '         )      	                    $         !                      *             +   (         &           %s older Archives Author Archives Categories: Category Archives: Click here Comment Comments Creative Commons license Daily Archives: %s Edit This Edit your profile Engaging Science on YouTube Error: please fill the required field (Review Title). European Unions Seventh Framework Programme Logged in as Main Menu Monthly Archives: %s Next Post Not Found Nothing Found Nothing found for the requested page. Try a search instead? Posts by %s Previous Post Return to %s Review Title Reviews Search Results for: %s Sidebar Widget Area Sorry, nothing matched your search. Please try again. Submit comment Submit review Tag Archives: This project has received funding from the European Union's Seventh Framework Programme for research, technological development and demonstration under grant agreement no. Top Menu Trackback Trackbacks Write a comment Write a review Yearly Archives: %s Your Review newer %s Project-Id-Version: Engaging Science Reviewed v
Report-Msgid-Bugs-To: 
POT-Creation-Date: 
PO-Revision-Date: 2015-06-12 12:06+0100
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.7.1
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Textdomain-Support: yes
Language: de_DE
X-Poedit-SearchPath-0: .
 piu' vecchio di %s Archiv Autoren Archiv Kategorien: Kategorien-Archiv: Klicken Sie hier Kommentar Kommentare Creative Commons license Tages-Archiv: %s Bitte bearbeiten Sie dies Bearbeitung des Profils Engaging Science auf YouTube Fehler: Bitte füllen Sie das erforderliche Feld aus (Titel der Bewertung) Siebtes Rahmenprogram der Europäischen Union Angemeldet als Hauptmenü Monats-Archiv: %s Nächster Posten Kein Ergebnis Kein Ergebnis Für Ihre Anfrage wurde leider kein Ergebnis gefunden. Wollen Sie es mit einem andern Suchbegriff probieren? Veröffentlichungen von %s Vorheriger Posten Kehren Sie zurück zu %s Titel der Bewertung Bewertungen Suchergebnis: %s Sidebar Widget Area Leider haben wir kein Ergebnis für Ihre Suche gefunden. Bitte versuchen Sie es noch einmal. Kommentar abschicken Bewertung abschicken Merkliste: Dieses Projekt erhielt Fördermittel im Rahmen des Siebten Rahmenprogrammes der Europäischen Union für Forschung, technologischer Entwicklung und Durchführung mit der Rahmenvertragsnummer. Top Menü Trackback Trackbacks Schreiben Sie eine kommentar Schreiben Sie eine Bewertung Jahres-Archiv: %s Ihre Bewertung neuer %s 