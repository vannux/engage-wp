��    +      t  ;   �      �     �     �     �     �     �  
   �                    /  	   B     L     ^  5   z  +   �     �  	   �     �  	     	          ;   *     f     r     �     �     �     �     �  5   �                  �   .     �  	   �  
   �     �               +     7  0  @     q	     �	     �	     �	     �	     �	  
   �	     �	     �	     
     
     $
     4
  H   P
  -   �
     �
     �
     �
     �
            R   8     �     �     �     �     �      �     �  V        f     x     �  �   �     `  	   o  
   y     �     �     �     �     �                               #          
                                             %      "   '         )      	                    $         !                      *             +   (         &           %s older Archives Author Archives Categories: Category Archives: Click here Comment Comments Creative Commons license Daily Archives: %s Edit This Edit your profile Engaging Science on YouTube Error: please fill the required field (Review Title). European Unions Seventh Framework Programme Logged in as Main Menu Monthly Archives: %s Next Post Not Found Nothing Found Nothing found for the requested page. Try a search instead? Posts by %s Previous Post Return to %s Review Title Reviews Search Results for: %s Sidebar Widget Area Sorry, nothing matched your search. Please try again. Submit comment Submit review Tag Archives: This project has received funding from the European Union's Seventh Framework Programme for research, technological development and demonstration under grant agreement no. Top Menu Trackback Trackbacks Write a comment Write a review Yearly Archives: %s Your Review newer %s Project-Id-Version: Engaging Science Reviewed v
Report-Msgid-Bugs-To: 
POT-Creation-Date: 
PO-Revision-Date: 2015-06-12 12:06+0100
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.7.1
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Textdomain-Support: yes
Language: es_ES
X-Poedit-SearchPath-0: .
 piu' vecchio di %s Archivo Archivo de autores  Categorías: Categoría: archivos: Haz clic aquí Comentario Comentarios licencia Creative Commons Archivos diarios: %s Editar esto Edita tu perfil Engaging Science en YouTube Error: por favor, completa el campo obligatorio (Título del comentario) Séptimo Programa Marco de la Unión Europea  Has entrado como Menú principal Archivos mensuales: %s Siguiente entrada No encontrado No se ha encontrado nada No se ha encontrado nada en la página solicitada. Quieres realizar una búsqueda? Entradas de %s Entrada anterior Volver a %s Título del comentario Comentarios Resultados de búsqueda para: %s Sidebar Widget Area Lo sentimos, no se ha encontrado nada con estos criterios. Por favor, prueba otra vez. Enviar comentario Enviar comentario Archivos de etiquetas: Este proyecto ha recibido financiación del Séptimo Programa Marco de la Unión Europea para la investigación, desarrollo tecnológico y demostración con el acuerdo de subvención número Menú superior Trackback Trackbacks Publica un comentario Publica un comentario Archivos anuales: %s Tu comentario  %s más nuevos 