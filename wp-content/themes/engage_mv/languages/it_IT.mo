��    ,      |  ;   �      �     �     �     �     �     �  
   
               &     ?  	   R     \     n  5   �  +   �     �     �  	   �       	     	   &     0  ;   >     z     �     �     �     �     �     �  5   �          &     4  �   B     �  	   �  
                  +     ?     K  0  T     �	     �	     �	  
   �	     �	  	   �	     �	     �	     �	     
     
     '
     8
  6   T
  +   �
     �
     �
     �
     �
     �
     �
       C        [     l     |     �  	   �     �     �  ?   �          +     <  �   I     �  	     
             ,     B     V     e     !                         $          
                                             &      #   (         *      	                    %         "                       +            ,   )         '           %s older Archives Author Archives Categories: Category Archives: Click here Comment Comments Creative Commons license Daily Archives: %s Edit This Edit your profile Engaging Science on YouTube Error: please fill the required field (Review Title). European Unions Seventh Framework Programme Job Logged in as Main Menu Monthly Archives: %s Next Post Not Found Nothing Found Nothing found for the requested page. Try a search instead? Posts by %s Previous Post Return to %s Review Title Reviews Search Results for: %s Sidebar Widget Area Sorry, nothing matched your search. Please try again. Submit comment Submit review Tag Archives: This project has received funding from the European Union's Seventh Framework Programme for research, technological development and demonstration under grant agreement no. Top Menu Trackback Trackbacks Write a comment Write a review Yearly Archives: %s Your Review newer %s Project-Id-Version: Engaging Science Reviewed v
Report-Msgid-Bugs-To: 
POT-Creation-Date: 
PO-Revision-Date: 2015-06-12 12:07+0100
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.7.1
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Textdomain-Support: yes
Language: it_IT
X-Poedit-SearchPath-0: .
 %s piu' vecchio Archivi Archivi dell'autore Categorie: Archivio Categorie: Premi qui Commento Commenti Creative Commons license Archivio giornaliero: %s Modifica Modifica profilo Engaging Science su YouTube Errore: inserire il campo richiesto (Invia recensione) European Unions Seventh Framework Programme Lavoro Ciao Menu principale Archivi Mensili: %s Prossimo post Non Trovato Non ho trovato niente Nessun risultato per la pagina richiesta. Vuoi provare una ricerca? Pubblicato da %s Post precedente Ritorna a %s Titolo della recensione Revisione Risultati ricerca per: %s Sidebar Widget Area Spiacente, non trovato niente con la tua ricerca. Prova ancora. Invia il commento Invia recensione Archivi Tag: This project has received funding from the European Union's Seventh Framework Programme for research, technological development and demonstration under grant agreement no. Menu superiore Trackback Trackbacks Scrivi un commento Scrivi una recensione Archivi Annuali: %s Tua Recensione piu' nuovo di %s 