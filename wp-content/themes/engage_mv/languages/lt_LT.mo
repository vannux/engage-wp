��    +      t  ;   �      �     �     �     �     �     �  
   �                    /  	   B     L     ^  5   z  +   �     �  	   �     �  	     	          ;   *     f     r     �     �     �     �     �  5   �                  �   .     �  	   �  
   �     �               +     7  0  @     q	     }	     �	     �	     �	     �	  
   �	  
   �	     �	     
  	   
     !
     9
  C   U
  )   �
     �
     �
     �
     �
            ?   !     a     r     �     �  
   �     �     �  C   �     &     ;     P  �   _     	          !     '     @     Y     l     �                               #          
                                             %      "   '         )      	                    $         !                      *             +   (         &           %s older Archives Author Archives Categories: Category Archives: Click here Comment Comments Creative Commons license Daily Archives: %s Edit This Edit your profile Engaging Science on YouTube Error: please fill the required field (Review Title). European Unions Seventh Framework Programme Logged in as Main Menu Monthly Archives: %s Next Post Not Found Nothing Found Nothing found for the requested page. Try a search instead? Posts by %s Previous Post Return to %s Review Title Reviews Search Results for: %s Sidebar Widget Area Sorry, nothing matched your search. Please try again. Submit comment Submit review Tag Archives: This project has received funding from the European Union's Seventh Framework Programme for research, technological development and demonstration under grant agreement no. Top Menu Trackback Trackbacks Write a comment Write a review Yearly Archives: %s Your Review newer %s Project-Id-Version: Engaging Science Reviewed v
Report-Msgid-Bugs-To: 
POT-Creation-Date: 
PO-Revision-Date: 2015-06-12 12:07+0100
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.7.1
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Textdomain-Support: yes
Language: lt_LT
X-Poedit-SearchPath-0: .
 %s senesnis Archyvai Autorių archyvas Kategorijos: Archyvai pagal kategorijas: Spauskite čia Komentuoti Komentarai Autorinių teisių licenzija Dienos archyvas: %s Redaguoti Redaguoti savo profilį Engaging Science su YouTube Klaida: užpildykite privalomus laukelius (Atsiliepimo pavadinimas) Europos Sąjungos 7-oji Bendroji Programa Prisijungęs kaip Pagrindinis meniu Mėnesio archyvas: %s Kita žinutė Nerasta Nieko nerasta Nieko nerasta pagal pateiktą užklausą. Bandysite dar kartą? Žinutės nuo %s Ankstesnė žinutė Grįžti į %s Atsiliepimo pavadinimas Apžvalgos Paieškos rezultatai: %s Įrankių laukas Atsiprašome, nieko nerasta pagal užklausą. Bandykite dar kartą. Priimti atsiliepimą Priimti atsiliepimą Seni archyvai: Šis projektas finansuojamas Europos Sąjungos 7-osios Bendrosios programos lėšomis, skirtomis mokslui, technologijų vystymui ir viešinimui. Dotacijos sutarties  Nr. Viršutinis meniu Atgal Atgal Parašykite atsiliepimą Parašykite atsiliepimą Metų archyvas: %s Jūsų atsiliepimas naujesnis %s 