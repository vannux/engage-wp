��    *      l  ;   �      �     �     �     �     �  
   �     �     �     �       	   )     3     E  5   a  +   �     �  	   �     �  	   �  	   �       ;        M     Y     g     t     �     �     �  5   �     �     �       �        �  	   �  
   �     �     �     �            0  '     X	     a	  	   p	     z	  	   �	  	   �	     �	     �	     �	     �	     �	     �	  >   
     L
     e
  	   v
     �
  
   �
     �
     �
  7   �
     �
     �
               0     <     T  M   k     �     �  
   �  n   �     Z  	   c  
   m     x     �     �     �     �                              "          	                                             $      !   &         (                   )       #                         
                     *   '         %           %s older Author Archives Categories: Category Archives: Click here Comment Comments Creative Commons license Daily Archives: %s Edit This Edit your profile Engaging Science on YouTube Error: please fill the required field (Review Title). European Unions Seventh Framework Programme Logged in as Main Menu Monthly Archives: %s Next Post Not Found Nothing Found Nothing found for the requested page. Try a search instead? Posts by %s Previous Post Return to %s Review Title Reviews Search Results for: %s Sidebar Widget Area Sorry, nothing matched your search. Please try again. Submit comment Submit review Tag Archives: This project has received funding from the European Union's Seventh Framework Programme for research, technological development and demonstration under grant agreement no. Top Menu Trackback Trackbacks Write a comment Write a review Yearly Archives: %s Your Review newer %s Project-Id-Version: Engaging Science Reviewed v
Report-Msgid-Bugs-To: 
POT-Creation-Date: 
PO-Revision-Date: 2015-06-12 12:07+0100
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.7.1
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Textdomain-Support: yes
Language: nb_NO
X-Poedit-SearchPath-0: .
 %s eldre Forfatterarkiv Kategori: Kategoriarkiv: Klikk her Kommentar Kommentarer Creative Commons lisens Dagsarkiv: %s Rediger her Rediger profilen din Engaging Science på YouTube Feil: Fyll ut det obligatoriske feltet (Tittel på anmeldelse) EUs Syvende rammeprogram Du er logget inn Hovedmeny Månedsarkiv: %s Neste post Ikke funnet Ingenting funnet Vi fant ikke siden du så etter. Vil du prøve et søk? Poster pr %s Forrige post Returner til%s Tittel på anmeldelse Anmeldelser Søkeresultater for: %s Sidebar Widget område Vi fant dessverre ingen ting som passer med søket ditt. Vil du prøve igjen? Legg inn kommenter Legg inn anmeldelse Tag-arkiv: Prosjektet er støttet av EUs Syvende rammeprogram for forskning og teknologisk utvikling, prosjektkontrakt nr Toppmeny Trackback Trackbacks Skriv en kommentar Skriv en anmeldelse Årsarkiv: %s Din anmeldelse nyere %s 