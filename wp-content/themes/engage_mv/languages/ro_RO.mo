��    +      t  ;   �      �     �     �     �     �     �  
   �                    /  	   B     L     ^  5   z  +   �     �  	   �     �  	     	          ;   *     f     r     �     �     �     �     �  5   �                  �   .     �  	   �  
   �     �               +     7  0  @     q	     ~	     �	  
   �	     �	  	   �	  
   �	  
   �	     �	     �	     
     
     &
  Q   F
  +   �
     �
     �
     �
     �
            T   6     �     �  "   �     �     �  )   �       �   )     �     �     �  �   �     �  	   �  
   �     �     �     �     �  
   �                               #          
                                             %      "   '         )      	                    $         !                      *             +   (         &           %s older Archives Author Archives Categories: Category Archives: Click here Comment Comments Creative Commons license Daily Archives: %s Edit This Edit your profile Engaging Science on YouTube Error: please fill the required field (Review Title). European Unions Seventh Framework Programme Logged in as Main Menu Monthly Archives: %s Next Post Not Found Nothing Found Nothing found for the requested page. Try a search instead? Posts by %s Previous Post Return to %s Review Title Reviews Search Results for: %s Sidebar Widget Area Sorry, nothing matched your search. Please try again. Submit comment Submit review Tag Archives: This project has received funding from the European Union's Seventh Framework Programme for research, technological development and demonstration under grant agreement no. Top Menu Trackback Trackbacks Write a comment Write a review Yearly Archives: %s Your Review newer %s Project-Id-Version: Engaging Science Reviewed v
Report-Msgid-Bugs-To: 
POT-Creation-Date: 
PO-Revision-Date: 2015-06-12 12:07+0100
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.7.1
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Textdomain-Support: yes
Language: ro_RO
X-Poedit-SearchPath-0: .
 %s mai vechi Arhive Arhivele autorului Categorii: Categorie arhive: Clic aici Comentariu Comentarii Creative Commons license Arhive zilnice: %s Modific&#259; Modific&#259; profilul Engaging Science &#238; YouTube Eroare: v&#259; rug&#259;m s&#259; completa&#355;i c&#226;mpul (Titlul recenziei) European Unions Seventh Framework Programme Utilizator curent: Meniul principal Arhive lunare: %s Urm&#259;toarea postare Neg&#259;sit Nu am g&#259;sit nimic Nu am g&#259;it pagina cerut&#259;. &#206;ncerca&#355;i s&#259; o c&#259;uta&#355;i? Publicate de %s Precedenta postare &#206;ntoarce&#355;i-v&#359; la %s Titlul recenziei Recenzii Rezultatele c&#259;ut&#259;rii pentru: %s Sidebar Widget Area &#206;mi pare r&#259;u, dar nu am g&#259;it nimic din ce ai c&#259;utat. V&#259; rug&#259;m s&#259; &#238;ncerci &#238;nc&#259; odat&#259;. Trimite comentariu Trimite recenzia Etichete arhive: Acest proiect a primit finan&#355;are de la Programul FP7 al Uniunii Europene pentru cercetare, dezvoltare tehnologic&#259; &#351;i demonstrare prin contractul cu nr. Meniul superior Trackback Trackbacks Scrie un comentariu Scrie o recenzie Arhive anuale: %s Recenzia dvs. mai nou %s 