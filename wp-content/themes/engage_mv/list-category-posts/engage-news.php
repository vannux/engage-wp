<?php

$o = '';
$o .= '<div class="clear"></div>';

$count = 0;
foreach ($this->catlist->get_categories_posts() as $single){
	//$lcp_display_output .= '<div class="col_3"><div class="box">';
	$post_type_obj = get_post_type_object( $single->post_type );
	
	$o .= '<article id="post-'.$single->ID . '" class="' . join( ' ', get_post_class( '', $single->ID ) ) .'">';
 	$o .= '<header>';
	if ( false && is_singular($single->post_type) ) { 
		$o .= '<h1 class="entry-title">' . $this->get_post_title($single) . '</h1>';
	} 
	else { 
		$o .= '<h3 class="entry-title">'; 
		$o .= '<a href="'. get_permalink($single->ID) .'" title="'.get_the_title( $single ).'" rel="bookmark">' . $this->get_post_title($single).'</a>';
		$o .= '</h3>'; 
	}
	//$url = get_edit_post_link( $single->ID );
	//$link = __('Edit This', 'engage');
	//$link = '<a class="post-edit-link" href="' . $url . '">' . $link . '</a>';
 	//$o .= apply_filters( 'edit_post_link', $link, $single->ID );
	//
	//$author = get_the_author_meta ('', $single->post_autor);
	//print_r($author);
 	//$link = sprintf(
 	//		'<a href="%1$s" title="%2$s" rel="author">%3$s</a>',
 	//		esc_url( get_author_posts_url( $single->ID, $authordata->user_nicename ) ),
 	//		esc_attr( sprintf( __( 'Posts by %s', 'engage'), get_the_author() ) ),
 	//		get_the_author()
 	//);
	
 	//$o .= '<section class="entry-meta">';
 	//$o .= '<span class="author vcard">'. apply_filters( 'the_author_posts_link', $link ).'</span>';
 	//$o .= '<span class="meta-sep"> | </span>';
 	//$o .= '<span class="entry-date">'.apply_filters( 'the_time', get_the_time( get_option('date_format'), $single ), 'date_format' ) .'</span>';
 	//$o .= '</section>';
 	//
	$o .= '</header>';
	
	$o .= '<section class="entry-content">';
	$o .= $this->get_excerpt($single, 'p', 'margin_top_15');
	$o .= '</section>';
	
	
	$o .= '</article>';
		
}
$this->lcp_output = $o;	
