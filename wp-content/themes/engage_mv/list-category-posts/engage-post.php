<?php

$lcp_display_output = '';
$lcp_display_output .= '<div class="clear"></div>';

$count = 0;
foreach ($this->catlist->get_categories_posts() as $single){
	$count = ++$count % 3;
	$cats = get_the_category($single->ID);
	/*$isAdapt = false;
	foreach($cats as $cat) {
		if ($cat->slug == 'adapt') {
			$isAdapt = true;
			break;
		}
	}*/
	$isAdapt = isAdapt($single->ID);
	if ($count==1) $lcp_display_output .= '<div class="clear"/>';
	$lcp_display_output .= '<div class="col_3' . ($count==0 ? ' last' : '') .'"><div class="box">';
	$lcp_display_output .= '<div class="show imgcap maxheight"><a href="'.get_permalink($single->ID).'">';

	if (has_post_thumbnail($single->ID)) {
		$lcp_display_output .=  get_the_post_thumbnail($single->ID, null, array('class' => "fullwidth" ));
	}
	else {
		$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $single->post_content, $matches);
		$first_img = $matches[1][0];
		if (empty($first_img)) {
			//$first_img = bloginfo('template_directory').'/images/news.png';
			$first_img = get_template_directory_uri().'/images/news.png';
		}
		$lcp_display_output .= "<img src='$first_img' class = 'fullwidth'>";
	}

	if ($isAdapt) {
		$current_site = substr(get_bloginfo('wpurl'), -2);
		$lcp_display_output .= '<style>.adapt-overlay{	background: url("'.get_template_directory_uri().'/images/adapt-overlay-'.$current_site.'.png");width: 100%;height: 100%;position: absolute;overflow: hidden;top: 0;left: 0;}</style>';
		$lcp_display_output .= '<div class="adapt-overlay"></div>';
	}
	$lcp_display_output .= '<div class="mask"><span class="more">'. __('Click here', 'engage') . '</span></div></a></div>';
	$lcp_display_output .= '<h3'. ($isAdapt?' class="adapt-title"':'') .'>'.$this->get_post_title($single).'</h3>';
	$lcp_display_output .= '<span class="tag-links'. ($isAdapt?' adapt-tag-links':'') .'">' . get_the_tag_list('', ' ', '', $single->ID) . '</span>';
	$lcp_display_output .= $this->get_excerpt($single, 'p', 'margin_top_15');
	$lcp_display_output .= '<div class="clear"></div></div></div>';
	
}
$this->lcp_output = $lcp_display_output;	
