<?php 
/* Template Name: No title, No widgets */
get_header(); ?>
<!-- content / social -->
<div class="container main-content">
	<div class="row">
		<div class="col_12">
			<section id="content" role="main">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<section class="entry-content">
							<?php the_content(); ?>
						</section>
					</article>
				<?php endwhile; endif; ?>
			</section>
		</div>
	</div>
	<div class="clear"></div>
</div>
<?php get_footer(); ?>
