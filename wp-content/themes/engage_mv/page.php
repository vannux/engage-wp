<?php get_header(); ?>
<!-- content / social -->
<div class="container main-content">
	<div class="row">
		<div class="col_9">
			<section id="content" role="main">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<header class="header" id="page-<?php the_ID(); ?>">
							<?php if (!is_front_page()) { ?>
								<h1><?php the_title(); ?></h1>
							<?php } else if (is_main_site()) { ?>
								<h1 class="entry-title"><?php the_title(); ?></h1>
							<?php } ?>
						</header>
						<section class="entry-content">
							<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
							<?php the_content(); ?>
							<div class="entry-links"><?php wp_link_pages(); ?></div>
						</section>
					</article>
				<?php #if ( ! post_password_required() ) comments_template( '', true ); ?>
				<?php endwhile; endif; ?>
				<?php #edit_post_link(); ?>

			</section>
		</div>
		<div class="col_3 border-left last">
			<?php get_sidebar(); ?>
		</div>
	</div>
	<div class="clear"></div>
</div>
<!-- content ends -->

<?php 
if ($_SESSION['els_logged']=='true') {
	unset($_SESSION['els_logged']);
	$pages = get_posts( array( 'name' => 'welcome-popup', 'post_type' => 'page' ) );
	if (count($pages) > 0) {
		$page = $pages[0];
		wp_enqueue_script( 'bpopup', get_template_directory_uri().'/js/bpopup.js', ['jquery'], null, true );
		wp_enqueue_script( 'popup_login', get_template_directory_uri().'/js/login_popup.js', ['bpopup'], null, true );
		?>
		<style>
		#login_popup {
			display:none;
			width: 661px;
			height: 941px;
			background-image: url("<?php echo get_template_directory_uri(); ?>/images/dialogue_box_b.png");
			background-repeat: no-repeat;
			}
			#login_popup_content {
				position:relative;
				left: 117px;
				top: 173px;
				width: 453px;
				height: 380px;
				overflow: auto;
			}
			#login_popup_links {
				position: absolute;
				left: 117px;
				top: 564px;
				width: 453px;
				height: 48px;
				text-align: right;
				overflow: hidden;
				font-weight: bold;
			}
				</style>
		<input type="hidden" id="cacheTest"></input>
		<div id="login_popup" hidden="hidden">
			<div id="login_popup_content">
				<?php
				 $content = get_post_field('post_content', $page->ID);
				 $content = apply_filters('the_content', $content);
				 echo $content;
				?>
			</div>
			<div id="login_popup_links">
				<?php echo '<a href="'.admin_url( 'profile.php' ).'">'. __('Admin area', '') . '</a>'; ?>
			</div>
		</div>
<?php } } ?>
<?php get_footer(); ?>
