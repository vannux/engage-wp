<?php $isAdapt = isAdapt(get_the_ID()) ?>
<footer class="entry-footer">
<span class="tag-links<?php if ($isAdapt) echo ' adapt-tag-links'; ?>"><?php the_tags('', ' ', ''); ?></span>
<?php if ( comments_open() ) { ?>
<span class='comments-link' style="margin-left:1em">
	<a href='<?php comments_link();?>'><?php  echo __( 'Comments') . ' (' . get_comments_number() . ')'; ?></a>
</span>
<?php } ?>
</footer>
<?php if(function_exists('wp_ulike')) wp_ulike('get'); ?>
