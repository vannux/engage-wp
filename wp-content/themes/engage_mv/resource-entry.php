<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<header>

<?php 
if ( is_singular() ) {
	$isAdapt = isAdapt(get_the_ID());
	echo '<h1 class="entry-title'.($isAdapt?' adapt-title':'').'">'; 
	the_title(); 
	?><!--span class='review'><a href='<?php comments_link(); ?>'><?php _e('Write a comment'); ?></a--></span>
	</h1><?php
} 
else { 
	echo '<h2 class="entry-title">'; 
	?><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark"><?php the_title(); ?></a><?php
	echo '</h2>'; 
} 
edit_post_link(); 
?>

<?php if ( !is_search() ) get_template_part( 'entry', 'meta' ); ?>
<?php if ( !is_search() ) get_template_part( 'resource-entry-footer' ); ?>
</header>
<?php get_template_part( 'entry', ( is_archive() || is_search() ? 'summary' : 'content' ) ); ?>
</article>
