<?php 
get_header(); 
do_action('adapt_before_content'); 
wp_enqueue_script( 'bpopup', get_template_directory_uri().'/js/bpopup.js', ['jquery'], null, true );
wp_enqueue_script( 'single-cat-resources', get_template_directory_uri().'/js/single-cat-resources.js', array('bpopup') );
?>
<!-- content / social -->
<div id="content">
	<div class="container">
  	<div class="row">
	  	<div class="col_12 last">
	      <section role="main">

					<?php if ( have_posts() ) {
						while ( have_posts() ) {
							the_post();
							?>
							<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
								<header>

								<?php
								if ( is_singular() ) {
									$isAdapt = isAdapt(get_the_ID());
									if(false && function_exists('wp_ulike')) {?></div style=""><?php }
									echo '<h1 style="display: initial;" class="entry-title'.($isAdapt?' adapt-title':'').'">';
									the_title();
									}
									echo '</h1>';
									if(function_exists('wp_ulike')) {
										?><span style="margin-left:1em"><?php
										wp_ulike('get');
										?></span><br/><?php
									if(false && function_exists('wp_ulike')) {?></div><?php }
								}
								else {
									echo '<h2 class="entry-title">';
									?><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark"><?php the_title(); ?></a><?php
									if(function_exists('wp_ulike')) wp_ulike('get');
									echo '</h2>';
								}
								edit_post_link();
								?>
								<?php $isAdapt = isAdapt(get_the_ID()) ?>
								<footer class="entry-footer">
								<span class="tag-links<?php if ($isAdapt) echo ' adapt-tag-links'; ?>"><?php the_tags('', ' ', ''); ?></span>
								<?php if ( comments_open() ) { ?>
								<span class='comments-link' style="margin-left:1em">
									<a href='<?php comments_link();?>'><?php  echo __( 'Comments') . ' (' . get_comments_number() . ')'; ?></a>
								</span>
								<span class='review'><a href='<?php comments_link(); ?>'><?php _e('Write a comment'); ?></a></span>
								<?php } 
								
								// STATISTICS
								$totDownloads = get_nr_downloads_post($post);
								$rating = get_comments_number();
								echo '&nbsp;&nbsp;&nbsp;<span class="post_reviews" style="margin-right:10px;">'
									. sprintf( _n( '%s download', '%s downloads', $totDownloads, 'engage' ), $totDownloads )
									. '</span>  ';
								_e('Published', 'engage'); echo ': '; the_time( get_option( 'date_format' ) ); 

								// RATING
								$post = get_post();
								$post_id = $post->ID;

								global $wpdb;
								$table_name = $wpdb->prefix . 'els_useful_for';
								
								$allTags = $wpdb->get_results("select tag, tag_descr from $table_name where post_id=-1 order by tag");
								
								$votes=array();
								foreach($allTags as $t) {
									$votes[$t->tag] = array('descr' => $t->tag_descr);
								}
								
								for ($i=1 ; $i < 6 ; $i++) {
									$dbVotes = $wpdb->get_results("select tag, count(id) votes from $table_name where post_id=$post_id and used=$i group by tag order by tag");
									if ($dbVotes) {
										foreach($dbVotes as $t) {
											if (array_key_exists($t->tag, $votes) ) {
												$votes[$t->tag][$i] = array('votes' => $t->votes);
											}
										}
									}
								}
								
								wp_localize_script( 'single-cat-resources', 'Rating', $votes);
								wp_localize_script( 'single-cat-resources', 'Locale', array('vote'=>__('vote', 'engage'), 'votes'=>__('votes', 'engage')) );

								//ELS_Useful_For_Lib::get_avg_tags($post_id);
								$r4 = $wpdb->get_results("select tag, count(id) tot, avg(used) average_rating from $table_name where post_id=$post_id group by tag order by tag");
								$tags = array();
								foreach($allTags as $t) {
									$arr = array('tag' => $t->tag, 'tag_descr' => $t->tag_descr);
									foreach($r4 as $t2) {
										if ($t->tag == $t2->tag) {
											$arr['tot'] = $t2->tot;
											$arr['average_rating'] = $t2->average_rating;
											break;
										}
									}
									array_push($tags, $arr);
								}
								
								if (count($tags) > 0)
								{  ?> <hr/>
									<div class="els-rating">
									<div class="els-rating-title"><?php _e('Rating summary', 'engage'); ?></div>
									<div class="els-rating-rate">
									<?php foreach ($tags as $value) {
										$avg = isset($value['average_rating']) ? $value['average_rating'] : 0;
										$totVotes = isset($value['tot']) ? $value['tot'] : 0;
										echo '
											<div class="els-rating-rate-item">
												<div class="els-rating-rate-item-title" data-tag="'.$value['tag'].'">'.$value['tag'].'
													<img title="'.$value['tag_descr'].'" class="els-rating-rate-item-info els-img-info-click" data-tag="'.$value['tag'].'" src="'. get_template_directory_uri().'/images/info_small.jpg"></img>
												</div>
												<div class="els-rating-rate-item-points">
													<hr style="margin-bottom:5px"/>
													<div class="els-rating-rate-item-point els-rating-rate-item-point_'.($avg > 0 ? 'e' : 'd').'" data-tagval="1"></div>
													<div class="els-rating-rate-item-point els-rating-rate-item-point_'.($avg > 1 ? 'e' : 'd').'" data-tagval="2"></div>
													<div class="els-rating-rate-item-point els-rating-rate-item-point_'.($avg > 2 ? 'e' : 'd').'" data-tagval="3"></div>
													<div class="els-rating-rate-item-point els-rating-rate-item-point_'.($avg > 3 ? 'e' : 'd').'" data-tagval="4"></div>
													<div class="els-rating-rate-item-point els-rating-rate-item-point_'.($avg > 4 ? 'e' : 'd').'" data-tagval="5"></div>
													<div class="els-rating-votes" data-tag="'.$value['tag'].'">' . ($totVotes == 1 ? sprintf(__('%s vote', 'engage'), $totVotes) : sprintf(__('%s votes', 'engage'), $totVotes)) . '</div>
					
												</div>
											</div>';
									  } ?>
									</div>
									</div>
									<div class="clear"></div>
								<?php } ?>
								</footer>

								<?php do_action('adapt_below_title'); ?>
								<hr/>
								</header>
							<?php get_template_part( 'entry', ( is_archive() || is_search() ? 'summary' : 'content' ) ); ?>
							</article>

							<?php
							if ( ! post_password_required() ) {
								do_action('comment_list_before');
								comments_template( '', true );
							}
						}
					} ?>

	      </section>
	    </div>
  	</div>
  	<div class="clear"></div>
  </div>
</div>

<style>
#els-rating-votes-pu {
	display:none;
	width: 608px;
	height: 670px;
	background-image: url("<?php echo get_template_directory_uri(); ?>/images/dialogue_box_b.png");
	background-repeat: no-repeat;
}
#els-rating-votes-pu_content {
	position:relative;
	left: 117px;
	top: 173px;
	width: 453px;
	height: 380px;
	overflow: auto;
}
#els-rating-votes-pu_content h3 {
	margin:0px;
}
.els-rating-rate-item-points-pu {
	_border: 1px solid red;
	margin-top: 9px;
	float:left;
}
.els-rating-rate-item-points-votes {
	margin-top:-4px;
	float: left;
	text-align: right;
  width: 87px;
}
.els-rating-rate-item-points-bar1{
	border: 1px solid grey;
	background-color: #eee;
	left:0px;
	height:20px;
	width:250px;
	margin:0 5px 0 5px;
	_position:absolute;
	float:left;
}
.els-rating-rate-item-points-bar2{
	background-color: #eb6937;
	position: relative;
	height:20px;
}
</style>
<div id="els-rating-votes-pu" hidden="hidden">
	<div id="els-rating-votes-pu_content">
	</div>
</div>
		
<!-- content ends -->
<?php get_footer(); ?>
