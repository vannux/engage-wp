<?php get_header(); ?>
<!-- content / social -->
<div id="content">
	<div class="container">
  	<div class="row">
			<!--
        	<div class="col_3">
				<?php get_sidebar(); ?>
				</div>
			-->
	  	<div class="col_12 last">
	      <section role="main">
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	          <?php get_template_part( 'resource-entry' ); ?>
	              <?php if ( ! post_password_required() ) comments_template( '', true ); ?>
	            <?php endwhile; endif; ?>
	      </section>
	    </div>
  	</div>
  	<div class="clear"></div>
  </div>
</div>
<!-- content ends -->
<?php get_footer(); ?>
