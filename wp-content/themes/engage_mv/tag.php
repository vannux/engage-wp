<?php get_header(); ?>
<!-- content / social -->
<div id="content">
	<div class="container">
    	<div class="row">
        	<div class="col_12">
                <section id="content" role="main">
                	<header class="header">
                		<h1><?php _e( 'Tag Archives:', 'engage' ); ?><?php single_tag_title(); ?></h1>
                	</header>
                	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                		<?php get_template_part( 'entry' ); ?>
                	<?php endwhile; endif; ?>
                	<?php get_template_part( 'nav', 'below' ); ?>
                </section>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<?php #get_sidebar(); ?>
<?php get_footer(); ?>